<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use App\Order;

class OrderPaid extends Notification
{
    use Queueable;

    protected $order_ref;
    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(Order $order)
    {
        $this->order_ref = $order->order_ref;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $url = url('/order/' . $this->order_ref);
        return (new MailMessage)
            ->subject('Your Order (' . $this->order_ref . ') Paid')
            ->greeting('Hello ' . $notifiable->name . '!')
            ->line('Thanks for order! Your order status is Paid. To view more details, click button below.')
            ->action('Order Status', url($url))
            ->line('Thank you for purchasing from us!');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
