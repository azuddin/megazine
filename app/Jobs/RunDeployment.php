<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use App;
use Illuminate\Support\Facades\Log;
use Symfony\Component\Process\Process;

class RunDeployment implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $branch;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($branch = [])
    {
        $this->branch = $branch;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        if (isset($this->branch['name']) && isset($this->branch['checkout_sha'])) {
            if (App::environment('production') && $this->branch['name'] == 'master') {
                Log::channel('deploy')->info('Starting live deployment... checkout_sha:' . $this->branch['checkout_sha']);
                $process = new Process('cd ' . base_path() . '; ./deploy.production.sh');
                $process->run(function ($type, $buffer) {
                    Log::channel('deploy')->info($buffer);
                });
            } elseif (!App::environment('production') && $this->branch['name'] == 'develop') {
                Log::channel('deploy')->info('Starting staging deployment... checkout_sha:' . $this->branch['checkout_sha']);
                $process = new Process('cd ' . base_path() . '; ./deploy.staging.sh');
                $process->run(function ($type, $buffer) {
                    Log::channel('deploy')->info($buffer);
                });
            }
        }
    }
}
