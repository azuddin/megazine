<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class BundleImage extends Model
{
    use SoftDeletes;

    protected $fillable = ['bundle_id', 'url'];
}
