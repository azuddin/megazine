<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Wishlist extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'product_id',
        'bundle_id',
        'quantity',
    ];

    public function product()
    {
        return $this->hasOne('App\Product');
    }

    public function bundle()
    {
        return $this->hasOne('App\Bundle');
    }
}
