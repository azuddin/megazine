<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Redis;
use Illuminate\Support\Facades\Cache;
use Carbon\Carbon;

class AdminProductCollectionResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        // if (env('CACHE_DRIVER') == 'redis') {
        //     $hit = Redis::get('product:' . $this->product->id . ':' . $this->collection_slug . ':hit');
        // } else {
        $hit = Cache::get('product:' . $this->product->id . ':collection:' . $this->id . ':hit');
        // }

        $collectionImages = ProductImageResource::collection($this->images);

        return [
            'product_id' => $this->product_id,
            'collection_name' => $this->collection_name,
            'collection_slug' => $this->collection_slug,
            'is_hidden' => $this->is_hidden,
            'hit' => $hit, //cannot sort/filter
            'images' => $collectionImages,
            'created_at' => Carbon::parse($this->created_at)->format('d-m-Y'),
            'updated_at' => Carbon::parse($this->updated_at)->format('d-m-Y')
        ];
    }
}
