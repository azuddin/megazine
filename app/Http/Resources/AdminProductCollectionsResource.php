<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use Carbon\Carbon;

class AdminProductCollectionsResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'product_id' => $this->product_id,
            'collection_name' => $this->collection_name,
            'collection_slug' => $this->collection_slug,
            'is_hidden' => $this->is_hidden,
            'created_at' => Carbon::parse($this->created_at)->format('d-m-Y')
        ];
    }
}
