<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\ProductsResource;
use Carbon\Carbon;

class BundleItemResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'product' => new ProductsResource($this->product),
            'price' => $this->price,
            'quantity' => $this->quantity,
            'created_at' => Carbon::parse($this->created_at)->format('d-m-Y')
        ];
    }
}
