<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\BundleItemResource;
use App\Http\Resources\BundleImageResource;
use Carbon\Carbon;

class BundleResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'bundle_name' => $this->bundle_name,
            'bundle_desc' => $this->bundle_desc,
            'images' => BundleImageResource::collection($this->images), //cannot sort/filter
            'items' => BundleItemResource::collection($this->items), //cannot sort/filter
            'price' => $this->price,
            'created_at' => Carbon::parse($this->created_at)->format('d-m-Y')
        ];
    }
}
