<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Redis;
use Illuminate\Support\Facades\Cache;
use Carbon\Carbon;

class ProductCollectionsResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        // if (env('CACHE_DRIVER') == 'redis') {
        //     $hit = Redis::get('product:' . $this->product->id . ':collection:' . $this->id . ':hit');
        // } else {
        $hit = Cache::get('product:' . $this->product->id . ':collection:' . $this->id . ':hit');
        // }

        $collectionImages = ProductImageResource::collection($this->images);

        return [
            'id' => $this->id,
            // 'collection_slug' => $this->collection_slug,
            'collection_name' => $this->collection_name,
            'collection_image_first' => count($collectionImages) > 0 ? $collectionImages[0] : null,
            'collection_image_count' => count($collectionImages),
            'hit' => $hit, //cannot sort/filter
            'created_at' => Carbon::parse($this->created_at)->format('d-m-Y')
        ];
    }
}
