<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use Carbon\Carbon;

class CartResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $name = ($this->bundle !== null ? $this->bundle->bundle_name : $this->product->product_name);
        return [
            'id' => $this->id,
            'name' => $name, //cannot sort/filter
            'product_id' => $this->product_id,
            'bundle_id' => $this->bundle_id,
            'price' => $this->price,
            'quantity' => $this->quantity,
            'created_at' => Carbon::parse($this->created_at)->format('d-m-Y')
        ];
    }
}
