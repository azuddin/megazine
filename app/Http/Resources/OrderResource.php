<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\OrderItemResource;
use Carbon\Carbon;

class OrderResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'order_ref' => $this->order_ref,
            'total' => $this->total,
            'order_status' => $this->order_status,
            'order_items' => OrderItemResource::collection($this->items), //cannot sort/filter
            'created_at' => Carbon::parse($this->created_at)->format('d-m-Y')
        ];
    }
}
