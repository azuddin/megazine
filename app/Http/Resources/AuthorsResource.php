<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use Carbon\Carbon;

class AuthorsResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'author_name' => $this->author_name,
            'alias_name' => $this->alias_name,
            'email' => $this->email,
            'avatar' => $this->avatar,
            'created_at' => Carbon::parse($this->created_at)->format('d-m-Y')
        ];
    }
}
