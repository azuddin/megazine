<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use Carbon\Carbon;

class OrderItemResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'product_id' => $this->product_id,
            'bundle_id' => $this->bundle_id,
            'bought_price' => $this->price,
            'quantity' => $this->quantity,
            'created_at' => Carbon::parse($this->created_at)->format('d-m-Y')
        ];
    }
}
