<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use Carbon\Carbon;

class AdminOrderResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'user_id' => $this->user_id,
            'order_ref' => $this->order_ref,
            'order_status' => $this->order_status,
            'total' => $this->total,
            'payment_method' => $this->payment_method,
            'payment_url' => $this->payment_url,
            'created_at' => Carbon::parse($this->created_at)->format('d-m-Y'),
            'order_items' => OrderItemResource::collection($this->items), //cannot sort/filter
            'created_at' => Carbon::parse($this->created_at)->format('d-m-Y')
        ];
    }
}
