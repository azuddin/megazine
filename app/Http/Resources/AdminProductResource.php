<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;
use Illuminate\Support\Facades\Redis;
use Illuminate\Support\Facades\Cache;
use Carbon\Carbon;

class AdminProductResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        // if (env('CACHE_DRIVER') == 'redis') {
        //     $hit = Redis::get('product:' . $this->id . ':hit');
        // } else {
        $hit = Cache::get('product:' . $this->id . ':hit');
        // }

        $collection = $this->allProductCollections->where('collection_slug', 'cover')->first();

        if ($collection == null) {
            $cover = [];
        } else {
            $cover = AdminProductImageResource::collection($collection->images);
        }

        return [
            'id' => $this->id,
            'product_name' => $this->product_name,
            'product_desc' => $this->product_desc,
            'product_type' => $this->product_type,
            'author_id' => $this->author_id,
            'category_id' => $this->category_id,
            'price' => $this->price,
            'stock' => $this->stock,
            'hit' => $hit, //cannot sort/filter
            'likes' => $this->likes,
            'cover_images' => $cover, //cannot sort/filter
            'collections' => AdminProductCollectionsResource::collection($this->allProductCollections->where('collection_slug', '<>', 'cover')), //cannot sort/filter
            'created_at' => Carbon::parse($this->created_at)->format('d-m-Y')
        ];
    }
}
