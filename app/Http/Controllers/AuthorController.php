<?php

namespace App\Http\Controllers;

use App\Author;
use Illuminate\Http\Request;
use App\Http\Resources\AuthorResource;
use App\Http\Resources\AuthorsResource;
use App\Traits\Pagination;

/**
 * @group Author
 */
class AuthorController extends Controller
{

    /**
     * Get author list
     *
     * Example url: /api/author?pageSize=2&page=0&sorted[0][id]=author_name&sorted[0][desc]=false&filtered[0][id]=author_name&filtered[0][value]=et
     * 
     * @queryParam pageSize List page size. Example: 10
     * @queryParam page Page number. Example: 1
     * @queryParam sorted Array of sorted item. e.g: sort descending by author_name [{"author_name":"true"}]. Example: sorted[0][id]=author_name&sorted[0][desc]=false
     * @queryParam filtered Array of filtered item. e.g: filter record by author_name [{"author_name":"Stan Lee"}]. Example: filtered[0][id]=author_name&filtered[0][value]=Stan Lee
     * 
     * @response {
     * "data": [
            {
                "id": 1,
                "author_name": "CESB",
                "alias_name": null,
                "email": null,
                "avatar": null
            },
            {
                "id": 2,
                "author_name": "epie",
                "alias_name": null,
                "email": null,
                "avatar": null
            },
            {
                "id": 3,
                "author_name": "Halia",
                "alias_name": null,
                "email": null,
                "avatar": null
            }
        ],
        "meta": {
            "total": 3
        }
     * }
     */
    public function index(Request $request)
    {
        $available_key = ['id', 'author_name', 'alias_name', 'email', 'avatar'];
        $paginate = Pagination::paginate($request, new Author, $available_key);

        return AuthorsResource::collection($paginate['data'])->additional(['meta' => ['total' => $paginate['count']]]);
    }

    /**
     * Get author details
     * 
     * @queryParam author required Id of the author. Example: 1
     * 
     * @response {
     * "data": {
            "id": 1,
            "author_name": "CESB",
            "alias_name": null,
            "email": null,
            "avatar": null,
            "created_at": "22-03-2019"
        }
     * }
     */
    public function show(Author $author)
    {
        if ($author == null) return response(['error' => 'Author not found'], 400);

        return new AuthorResource($author);
    }
}
