<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use App\Jobs\RunDeployment;

class DeployController extends Controller
{
    public function gitlab(Request $request)
    {
        //check for x-gitlab-token
        if ($request->header('X-Gitlab-Token') !== ltrim(env('APP_KEY'), "base64:")) return response(['error' => 'Invalid secret token'], 403);

        Log::channel('deploy')->info('GitLab PUSH hooks. ref:' . $request['ref'] . ', checkout_sha:' . $request['checkout_sha']);

        if ($request['ref'] == 'refs/heads/master' || $request['ref'] == 'refs/heads/develop') {
            $branch = [
                'name' => basename($request['ref']),
                'checkout_sha' => $request['checkout_sha']
            ];

            RunDeployment::dispatch($branch);

            Log::channel('deploy')->info('Job has been dispatch');
        }

        $message = 'Deployment has finish';
        Log::channel('deploy')->info($message);
        return response(['message' => $message], 200);
    }
}
