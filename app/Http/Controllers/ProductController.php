<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;
use App\Traits\Pagination;
use App\Http\Resources\ProductResource;
use App\Http\Resources\ProductsResource;
use Illuminate\Support\Facades\Auth;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Support\Facades\Cache;

/**
 * @group Product
 */
class ProductController extends Controller
{

    /**
     * Get product list
     * 
     * Example url: /api/product?pageSize=2&page=0&sorted[0][id]=product_name&sorted[0][desc]=false&filtered[0][id]=product_name&filtered[0][value]=Aut dignissimos
     *
     * @queryParam pageSize List page size. Example: 10
     * @queryParam page Page number. Example: 0
     * @queryParam sorted Array of sorted item. e.g: sort descending by product_name [{"product_name":"true"}]. Example: sorted[0][id]=product_name&sorted[0][desc]=false
     * @queryParam filtered Array of filtered item. e.g: filter record by product_name [{"product_name":"Product A"}]. Example: filtered[0][id]=product_name&filtered[0][value]=Product A
     * 
     * @response {
     * "data": [
            {
                "id": 1,
                "product_name": "Gila-Gila",
                "product_desc": null,
                "product_type": "normal",
                "author_id": 6,
                "category_id": 2,
                "price": "0.00",
                "stock": 1,
                "hit": 7,
                "likes": "0",
                "is_public": 1,
                "cover_images": [
                    {
                        "url": "gg/gg001/gg00101.jpg",
                        "collection_slug": "cover",
                        "order": 1
                    }
                ]
            }
        ],
        "meta": {
            "total": 1
        }
     * }
     */
    public function index(Request $request)
    {
        $available_key = ['id', 'product_name', 'product_desc', 'product_type', 'author_id', 'category_id', 'price', 'stock', 'is_public', 'hit', 'likes'];
        $paginate = Pagination::paginate($request, new Product, $available_key);

        return ProductsResource::collection($paginate['data'])->additional(['meta' => ['total' => $paginate['count']]]);
    }

    /**
     * Get product details
     *
     * @queryParam product_id required Id of the product. Example: 1
     * 
     * @response {
     * "data": {
            "id": 1,
            "product_name": "Gila-Gila",
            "product_desc": null,
            "product_type": "normal",
            "author_id": 6,
            "category_id": 2,
            "price": "0.00",
            "stock": 1,
            "hit": 5,
            "likes": "0",
            "cover_images": [
                {
                    "url": "gg/gg001/gg00101.jpg",
                    "order": 1
                }
            ],
            "collections": [
                {
                    "id": 2,
                    "collection_name": "gila gila 1",
                    "collection_image_first": {
                        "url": "gg/gg001/gg00150.jpg",
                        "order": 1
                    },
                    "collection_image_count": 50,
                    "hit": 4
                },
                {
                    "id": 3,
                    "collection_name": "gila gila 788",
                    "collection_image_first": {
                        "url": "gg/gg788/gg78849.jpg",
                        "order": 1
                    },
                    "collection_image_count": 95,
                    "hit": null
                },
                {
                    "id": 4,
                    "collection_name": "siri istimewa hari raya 789",
                    "collection_image_first": {
                        "url": "gg/gg789/gg78947.jpg",
                        "order": 1
                    },
                    "collection_image_count": 92,
                    "hit": null
                },
                {
                    "id": 5,
                    "collection_name": "gila gila 811",
                    "collection_image_first": {
                        "url": "gg/gg811/gg81145.jpg",
                        "order": 1
                    },
                    "collection_image_count": 87,
                    "hit": null
                },
                {
                    "id": 6,
                    "collection_name": "gila gila 812",
                    "collection_image_first": {
                        "url": "gg/gg812/gg81245.jpg",
                        "order": 1
                    },
                    "collection_image_count": 88,
                    "hit": null
                }
            ]
        }
     * }
     */
    public function show($product_id)
    {
        $productCached = Cache::rememberForever('product:' . $product_id, function () use ($product_id) {
            $product = Product::find($product_id);
            if ($product == null) abort(400, 'Product not found');

            return $product;
        });

        if (!$productCached->is_public && Auth::guard('api')->guest()) throw new AuthenticationException;

        $hit = Cache::get('product:' . $product_id . ':hit') + 1;
        Cache::put('product:' . $product_id . ':hit', $hit);

        return (new ProductResource($productCached));
    }
}
