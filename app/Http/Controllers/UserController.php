<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Http\Resources\UserResource;
use Carbon\Carbon;
use App\User;
use App\Notifications\SignupActivation;
use App\Traits\Upload;
use Illuminate\Support\Facades\Cache;

/**
 * @group Authentication
 *
 * You authenticate to the application by providing your access token in the request header. You can get your access token from login endpoint.
 * Authentication to the API occurs via OAuth2. Provide your access token as the value of Authorization request header.
 * 
 * FUTURE: All API requests must be made over HTTPS. Calls made over plain HTTP will fail.
 */
class UserController extends Controller
{

    /**
     * Login
     *
     * @bodyParam email string required Email of the user account. Example: ahmad@azuddin.com
     * @bodyParam password string required Password of the account. Example: secret
     * 
     * @response {
     * "data": {
            "id": 1,
            "name": "Ahmad Azuddin",
            "email": "ahmad@azuddin.com",
            "phone_no": null,
            "avatar": null
        },
        "token": "eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6IjM2ZDJmNGVlZWM3OGJlYjYwYTQ5NTE4MTZhMzM5MDdkZDJiNTNjNGI0NWE0YTVlZmE4MGFjNWJkZmYzMGRiMjhkNzNlYzBlNWZiNzhkZTU5In0.eyJhdWQiOiIxIiwianRpIjoiMzZkMmY0ZWVlYzc4YmViNjBhNDk1MTgxNmEzMzkwN2RkMmI1M2M0YjQ1YTRhNWVmYTgwYWM1YmRmZjMwZGIyOGQ3M2VjMGU1ZmI3OGRlNTkiLCJpYXQiOjE1NTMzNDI2MDEsIm5iZiI6MTU1MzM0MjYwMSwiZXhwIjoxNTg0OTY1MDAxLCJzdWIiOiIyIiwic2NvcGVzIjpbXX0.tzm5ewp6NEb97_JLFhZSdg3XYVn2jf4bb_Wa_PEA7fKdzbwUb2CxXWsqnwlsHhUZ3h1XZ25DxmT0nuiYngoWc7wmfZlLWZRCmuEtIi2HDASDM6EWb21gyCmj_tWl8cGvUOfk6M4CjpHQ8oZt39CH6tycduX_4dUGKXfO7cM2l_obYPDc4Yiarp1Wy3tNK5jV2pd8AKYhcPXcfGALS3aQ3YPBHEVqppkA1SROtBHEiYAmE5WNZB5viWTbOVu1QCVKxbOLuEe4lD1hW87TitUtcS5xw-aSxFM1cvszWVfysEVi5ykIoZmJVM9G7SoW5a7TldGM7WQ6iVNsko1NRhEjZFYmYpv18qafBrg26MXJwqnjvWdcszSZgyZHBAbd1ornn1f4U23rmjeX6SELh-MjyaK-LRpyimbtfWI_kOM0YJp420cW986etH-90PmiGGc4HdYiKJPii70gaWW-i3sdMj8nHMb8wwnQgHlcggQ6nwhV-0ess3T84hFiox2h8BtmfDYI0KZywMm96sgVH1bhj0ehky2zS8Uqljtot6DMt0-tENX2pPD6wm1c7KHbGhv0BOK0oNEUZsH1DeZxB4tKbH--fAtHUTu5u7KBN_AqZfAEG7j7CkzPb4P6aci5pJSGunOtGS1vi4ISoRVBrdAHxEcC1JLVmVERbpkWIkVuQOo",
        "expiry": "2019-03-24 12:03:21"
     * }
     */
    public function login(Request $request)
    {
        $validated = $this->validate($request, [
            'email' => 'required|exists:users',
            'password' => 'required'
        ]);

        if (Auth::attempt(['email' => $validated['email'], 'password' => $validated['password']])) {
            $user = Auth::user();

            //remove any user token before creating new ones.
            // DB::table('oauth_access_tokens')->where('user_id', $user->id)->where('name', env('APP_NAME'))->update(['revoked' => true]);

            $data = $user->createToken(env('APP_NAME'));
            $token = $data->token;
            $token->expires_at = Carbon::now()->addDays(365);
            $token->save();

            return (new UserResource($user))->additional([
                'token' => $data->accessToken,
                'expiry' => Carbon::parse($token->expires_at)->format('Y-m-d H:i:s')
            ]);
        } else {
            return response([
                'message' => "Unauthorised. The password that you've entered is incorrect."
            ], 401);
        }
    }

    /**
     * Signup
     *
     * @bodyParam email string required Email of the user account. Example: ahmad@azuddin.com
     * @bodyParam name string required Name of the account. Example: azuddin
     * @bodyParam password string required Password of the account. Example: secret
     * @bodyParam password_confirmation string required Password Confirmation of the Password. Example: secret
     * @bodyParam phone_no string Phone number of the user. Example: 0123456789
     * @bodyParam avatar image Image for avatar of the user
     * 
     * @response {
     * "data": {
            "id": 1,
            "name": "azuddin",
            "email": "ahmad@azuddin.com",
            "phone_no": "0123456789",
            "avatar": "http://megazine.test/avatar/user/14-1553342755.png"
        }
     * }
     */
    public function signup(Request $request)
    {
        $validated = $this->validate($request, [
            'name' => 'required',
            'email' => 'required|email|unique:users',
            'phone_no' => 'sometimes|numeric',
            'password' => 'required|confirmed',
            'password_confirmation' => 'present',
            'avatar' => 'sometimes|image'
        ]);

        $validated['password'] = bcrypt($validated['password']);
        $avatar = isset($validated['avatar']) ? $validated['avatar'] : null;
        $validated['avatar'] = "";

        $user = new User;
        $user->fill($validated);
        $user->activation_token = str_random(60);
        $user->save();

        $user->notify(new SignupActivation($user));

        if (!empty($avatar)) {
            $user->avatar = Upload::uploadUserAvatar($user, $avatar);
            $user->save();
        } else {
            $user->avatar = env('APP_URL') . '/storage/avatar-' . rand(1, 9) . '.png';
            $user->save();
        }

        return (new UserResource($user));
    }

    public function verify($activation_token)
    {
        $user = User::where('activation_token', $activation_token)->first();
        if (!$user) {
            return redirect('verification-fail')->with('message', 'Confirmation token is invalid.');
        }
        $user->activation_token = '';
        $user->save();
        $user->markEmailAsVerified();

        return redirect('verification-status');
    }

    /**
     * Resend verification email
     *
     * @bodyParam email string required Email of the user account. Example: ahmad@azuddin.com
     * 
     * @response {
     * "message": "Verification email was sent"
     * }
     */
    public function resend(Request $request)
    {
        $validated = $this->validate($request, [
            'email' => 'required|exists:users'
        ]);

        $user = User::where('email', $validated['email'])->first();

        if ($user->hasVerifiedEmail()) {
            return response(['message' => 'Email has been verified'], 200);
        }

        $user->activation_token = str_random(60);
        $user->save();
        $user->notify(new SignupActivation($user));

        return response(['message' => 'Verification email was sent'], 200);
    }

    /**
     * Get user profile
     *
     * @response {
     * "data": {
            "id": 1,
            "name": "azuddin",
            "email": "ahmad@azuddin.com",
            "phone_no": "0123456789",
            "avatar": "http://megazine.test/avatar/user/14-1553342755.png"
        }
     * }
     */
    public function profile()
    {
        $userCached = Cache::rememberForever('profile:' . Auth::user()->id, function () {
            return Auth::user();
        });

        return (new UserResource($userCached));
    }

    /**
     * Update user profile
     *
     * @bodyParam email string Email of the user account. Example: ahmad@azuddin.com
     * @bodyParam name string Name of the account. Example: azuddin
     * @bodyParam password string Password of the account. Example: secret
     * @bodyParam password_confirmation string Password Confirmation of the Password. Example: secret
     * @bodyParam phone_no string Phone number of the user. Example: 0123456789
     * @bodyParam avatar image Image for avatar of the user
     * 
     * @response {
     * "data": {
            "id": 1,
            "name": "azuddin",
            "email": "ahmad@azuddin.com",
            "phone_no": "0123456789",
            "avatar": "http://megazine.test/avatar/user/14-1553342755.png"
        }
     * }
     */
    public function updateProfile(Request $request)
    {
        $user = User::find(Auth::user()->id);

        $validated = $this->validate($request, [
            'name' => 'sometimes',
            'email' => 'sometimes|email|unique:users,email,' . $user->id,
            'phone_no' => 'sometimes|numeric',
            'password' => 'sometimes|confirmed',
            'password_confirmation' => 'required_with:password',
            'avatar' => 'sometimes|image'
        ]);

        if (isset($validated['name'])) $user->name = $validated['name'];
        if (isset($validated['email'])) $user->email = $validated['email'];
        if (isset($validated['phone_no'])) $user->phone_no = $validated['phone_no'];
        if (isset($validated['password'])) $user->password = bcrypt($validated['password']);
        $user->save();

        if (isset($validated['avatar'])) {
            $user->avatar = Upload::uploadUserAvatar($user, $validated['avatar']);
            $user->save();
        }

        Cache::put('profile:' . $user->id, $user);

        return (new UserResource($user));
    }
}
