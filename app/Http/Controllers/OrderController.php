<?php

namespace App\Http\Controllers;

use App\Order;
use App\User;
use Illuminate\Http\Request;
use App\Traits\Transaction;
use App\Traits\Billplz;
use App\Traits\Pagination;
use App\Http\Resources\OrderResource;
use App\Http\Resources\OrdersResource;
use Illuminate\Support\Facades\Auth;

/**
 * @group Order
 */
class OrderController extends Controller
{

    /**
     * Get order list
     *
     * Example url: /api/order?pageSize=2&page=0&sorted[0][id]=order_ref&sorted[0][desc]=false&filtered[0][id]=order_ref&filtered[0][value]=REF8976189761
     * 
     * @queryParam pageSize List page size. Example: 10
     * @queryParam page Page number. Example: 1
     * @queryParam sorted Array sorted item. e.g: sort descending by order_ref [{"order_ref":"true"}]. Example: sorted[0][id]=order_ref&sorted[0][desc]=false
     * @queryParam filtered Array filtered item. e.g: filter record by order_ref [{"order_ref":"REF8976189761"}]. Example: filtered[0][id]=order_ref&filtered[0][value]=REF8976189761
     * 
     * @response {
     * "data": [
            {
                "order_ref": "REF5722257221",
                "total": "0.00",
                "order_status": "pending",
                "order_items_count": 0
            }
        ],
        "meta": {
            "total": 1
        }
     * }
     */
    public function index(Request $request)
    {
        $available_key = ['order_ref', 'total', 'order_status'];
        $paginate = Pagination::paginate($request, new Order, $available_key);

        return OrdersResource::collection($paginate['data'])->additional(['meta' => ['total' => $paginate['count']]]);
    }

    /**
     * Create an order
     *
     * @response {
     * "message": "Order has been created",
     * "order_ref": "REF7287672873",
     * "redirect_url": "https://billplz-staging.herokuapp.com/bills/0gme8dub"
     * }
     */
    public function create()
    {
        $user = User::find(Auth::user()->id);

        if ($user == null) return response(['error' => 'User not found']);

        $order = Transaction::create_order($user);

        if (isset($order['message'])) return response(['error' => $order['message']]);

        $billplz = Billplz::create_bill($order, route('api.billplz_webhook'), route('order.payment-redirect'));

        if (isset($billplz->error)) {
            return response(['message' => $billplz->error]);
        }

        $order->payment_ref = $billplz->id;
        $order->payment_url = $billplz->url;
        $order->payment_method = 'billplz';
        $order->save();

        return response([
            'message' => 'Order has been created',
            'order_ref' => $order->order_ref,
            'redirect_url' => $billplz->url
        ]);
    }

    /**
     * Get order details
     *
     * @queryParam order required Reference id of the order. Example: REF8976189761
     * 
     * @response {
     * "data": {
            "order_ref": "REF8976189761",
            "total": 177.16,
            "order_status": "paid",
            "order_items": [
                {
                    "product_id": 1,
                    "bundle_id": null,
                    "bought_price": 49.34,
                    "quantity": 2
                },
                {
                    "product_id": 11,
                    "bundle_id": 1,
                    "bought_price": 25.79,
                    "quantity": 1
                }
            ]
        }
     * }
     */
    public function show(Order $order, Request $request)
    {
        if ($order == null) return response(['error' => 'Order not found'], 400);

        if ($request->is('api/*')) return new OrderResource($order);

        return view('order_details')->with('order', $order);
    }

    /**
     * Redirect from payment gateway
     */
    public function redirect(Request $request)
    {
        $payment_success = false;
        $order_ref = '';

        switch (true) {
            case (isset($request->billplz)):
                $billplz = $request->billplz;
                $is_valid = Billplz::validate_xsignature($billplz, true);
                if ($is_valid) {
                    $order = Order::where('payment_ref', $billplz['id'])->first();

                    if ($order !== null && $billplz['paid'] == "true") {
                        $order_ref = $order->order_ref;
                        $payment_success = true;
                        Transaction::handle_paid($order);
                    }
                }
                break;
                //another case if use another payment gateway. eg: ipay88,molpay
            default:
                abort(403, 'Unauthorized action.');
                break;
        }

        return redirect('order/payment-status')->with('status', $payment_success)->with('order_ref', $order_ref);
    }

    /**
     * Webhook for billplz
     */
    public function billplzWebhook(Request $request)
    {
        $requestArray = $request->all();

        $x_signature_is_valid = Billplz::validate_xsignature($requestArray);

        if ($x_signature_is_valid) {

            $order_exist = false;
            $order = Order::where('payment_ref', $requestArray['id'])->first();

            if ($order !== null) {
                if ($requestArray['paid'] == true) {
                    Transaction::handle_paid($order);
                } else {
                    Transaction::handle_fail($order);
                }
                $order_exist = true;
            }

            return response(['x_signature_is_valid' => true, 'order_exist' => $order_exist]);
        }

        return response(['x_signature_is_valid' => false]);
    }
}
