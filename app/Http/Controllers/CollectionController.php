<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;
use App\Http\Resources\ProductImageResource;
use App\Http\Resources\ProductCollectionResource;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Support\Facades\Auth;
use App\ProductCollection;
use Illuminate\Support\Facades\Redis;
use Illuminate\Support\Facades\Cache;

/**
 * @group Product
 */
class CollectionController extends Controller
{
    /**
     * Get product collections
     * 
     * @queryParam product_id required Id of the product. Example: 2
     * @queryParam collection_id required Collection number of the image. Example: 3
     * 
     * @response {
     * "data": {
            "id": 1,
            "product_id": 1,
            "collection_name": "cover",
            "collection_image_first": {
                "url": "gg/gg001/gg00101.jpg",
                "order": 1
            },
            "hit": 31,
            "images": [
                {
                    "url": "gg/gg001/gg00101.jpg",
                    "order": 1
                }
            ]
        }
     * }
     */
    public function index($product_id, $collection_id)
    {
        $productCached = Cache::rememberForever('product:' . $product_id, function () use ($product_id) {
            $product = Product::find($product_id);
            if ($product == null) abort(400, 'Product not found');
            return $product;
        });

        //having product to coupled with collection
        if (!$productCached->is_public && Auth::guard('api')->guest()) throw new AuthenticationException;

        $productCollectionCached = Cache::rememberForever('product:' . $product_id . ':collection:' . $collection_id, function () use ($product_id, $collection_id) {
            $productCollection = ProductCollection::where(['product_id' => $product_id, 'id' => $collection_id])->first();
            if ($productCollection == null) abort(400, 'Collection of the product does not exists');
            return $productCollection;
        });

        $hit = Cache::get('product:' . $product_id . ':collection:' . $collection_id . ':hit') + 1;
        Cache::put('product:' . $product_id . ':collection:' . $collection_id . ':hit', $hit);

        return (new ProductCollectionResource($productCollectionCached));
    }
}
