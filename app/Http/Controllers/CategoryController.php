<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Traits\Pagination;
use App\Http\Resources\CategoriesResource;
use App\Http\Resources\CategoryResource;
use App\Category;

/**
 * @group Category
 */
class CategoryController extends Controller
{

    /**
     * Get category list
     *
     * Example url: /api/category?pageSize=2&page=0&sorted[0][id]=category_name&sorted[0][desc]=false&filtered[0][id]=category_name&filtered[0][value]=et
     * 
     * @queryParam pageSize List page size. Example: 10
     * @queryParam page Page number. Example: 1
     * @queryParam sorted Array of sorted item. e.g: sort descending by category_name [{"category_name":"true"}]. Example: sorted[0][id]=category_name&sorted[0][desc]=false
     * @queryParam filtered Array of filtered item. e.g: filter record by category_name [{"category_name":"Magazine"}]. Example: filtered[0][id]=category_name&filtered[0][value]=Magazine
     * 
     * @response {
     * "data": [
            {
                "id": 1,
                "category_name": "Komik"
            },
            {
                "id": 2,
                "category_name": "Majalah"
            },
            {
                "id": 3,
                "category_name": "Koleksi"
            }
        ],
        "meta": {
            "total": 3
        }
     * }
     */
    public function index(Request $request)
    {
        $available_key = ['id', 'category_name'];
        $paginate = Pagination::paginate($request, new Category, $available_key);

        return CategoriesResource::collection($paginate['data'])->additional(['meta' => ['total' => $paginate['count']]]);
    }

    /**
     * Get category details
     *
     * @queryParam category required Id of the category. Example: 1
     * 
     * @response {
     * "data": {
            "id": 1,
            "category_name": "Komik"
        }
     * }
     */
    public function show(Category $category)
    {
        if ($category == null) return response(['error' => 'Category not found'], 400);

        return new CategoryResource($category);
    }
}
