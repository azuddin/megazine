<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Traits\Pagination;
use App\Http\Resources\AdminOrdersResource;
use App\Http\Resources\AdminOrderResource;
use App\Order;

/**
 * @group (Admin) Order
 */
class OrderController extends Controller
{

    /**
     * Get order list
     *
     * @queryParam pageSize List page size. Example: 10
     * @queryParam page Page number. Example: 0
     * @queryParam sorted Array of sorted item. e.g: sort descending by price [{"price":"true"}]. Example: sorted[0][id]=price&sorted[0][desc]=false
     * @queryParam filtered Array of filtered item. e.g: filter record by price [{"price":"10.23"}]. Example: filtered[0][id]=price&filtered[0][value]=10.23
     * 
     * @response {
     * "data": [
            {
                "id": 1,
                "user_id": 2,
                "order_ref": "REF5722257221",
                "order_status": "pending",
                "total": "0.00",
                "payment_method": null,
                "order_items_count": 0
            }
        ],
        "meta": {
            "total": 1
        }
     * }
     */
    public function index(Request $request)
    {
        $available_key = ['id', 'user_id', 'order_ref', 'order_status', 'total', 'payment_method'];
        $paginate = Pagination::paginate($request, new Order, $available_key);

        return AdminOrdersResource::collection($paginate['data'])->additional(['meta' => ['total' => $paginate['count']]]);
    }

    /**
     * Get order details
     *
     * @queryParam order required Id of the order. Example: 1
     * 
     * @response {
     * "data": {
            "id": 1,
            "user_id": 2,
            "order_ref": "REF5722257221",
            "order_status": "pending",
            "total": "0.00",
            "payment_method": null,
            "payment_url": null,
            "created_at": "23-03-2019",
            "order_items": []
        }
     */
    public function show(Order $order)
    {
        if ($order == null) return response(['error' => 'Order not found'], 400);

        return (new AdminOrderResource($order));
    }
}
