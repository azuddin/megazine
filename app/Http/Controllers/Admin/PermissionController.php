<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\PermissionsResource;
use App\Http\Resources\PermissionResource;
use Spatie\Permission\Models\Permission;
use App\Traits\Pagination;

/**
 * @group (Admin) Permission
 */
class PermissionController extends Controller
{

    /**
     * Get permission list
     *
     * @queryParam pageSize List page size. Example: 10
     * @queryParam page Page number. Example: 0
     * @queryParam sorted Array of sorted item. e.g: sort descending by name [{"name":"true"}]. Example: sorted[0][id]=name&sorted[0][desc]=false
     * @queryParam filtered Array of filtered item. e.g: filter record by name [{"name":"edit product"}]. Example: filtered[0][id]=name&filtered[0][value]=edit product
     * 
     * @response {
     * "data": [
            {
                "id": 1,
                "name": "view_admins"
            },
            {
                "id": 2,
                "name": "manage_admins"
            }
        ],
        "meta": {
            "total": 2
        }
     * }
     */
    public function index(Request $request)
    {
        $available_key = ['id', 'name'];
        $paginate = Pagination::paginate($request, new Permission, $available_key);

        return PermissionsResource::collection($paginate['data'])->additional(['meta' => ['total' => $paginate['count']]]);
    }

    /**
     * Add new permission
     *
     * @bodyParam name string required Name of the permission. Example: edit product
     * 
     * @response {
     * "message": "Permission has been created"
     * }
     */
    public function store(Request $request)
    {
        $validate = $request->validate([
            'name' => 'required|unique:permissions'
        ]);

        Permission::create(['name' => $validate['name']]);

        return response(['message' => 'Permission has been created']);
    }

    /**
     * Get permission details
     *
     * @queryParam permission required Id of the permission. Example: 1
     * 
     * @response {
     * "data": {
            "id": 1,
            "name": "view_admins",
            "created_at": "22-03-2019"
        }
     * }
     */
    public function show(Permission $permission)
    {
        if ($permission == null) return response(['error' => 'Permission not found'], 400);

        return (new PermissionResource($permission));
    }

    /**
     * Update permission detail
     *
     * @queryParam permission required Id of permission. Example: 1
     * @bodyParam name string required Name of the permission. Example: delete product
     * 
     * @response {
     * "message": "Permission has been updated"
     * }
     */
    public function update(Request $request, Permission $permission)
    {
        if ($permission == null) return response(['error' => 'Permission not found'], 400);

        $validate = $request->validate([
            'name' => 'required|unique:permissions'
        ]);

        $permission->name = $validate['name'];
        $permission->save();

        return response(['message' => 'Permission has been updated']);
    }

    /**
     * Remove permission
     *
     * @queryParam permission required Id of permission. Example: 1
     * 
     * @response {
     * "message": "Permission has been deleted"
     * }
     */
    public function destroy(Permission $permission)
    {
        if ($permission == null) return response(['error' => 'Permission not found'], 400);

        $permission->delete();

        return response(['message' => 'Permission has been deleted']);
    }
}
