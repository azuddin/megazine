<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Traits\Pagination;
use App\Author;
use App\Http\Resources\AuthorsResource;
use App\Http\Resources\AuthorResource;
use App\Traits\Upload;
use Illuminate\Support\Facades\Cache;

/**
 * @group (Admin) Author
 */
class AuthorController extends Controller
{

    /**
     * Get author list
     *
     * @queryParam pageSize List page size. Example: 10
     * @queryParam page Page number. Example: 0
     * @queryParam sorted Array of sorted item. e.g: sort descending by author_name [{"author_name":"true"}]. Example: sorted[0][id]=author_name&sorted[0][desc]=false
     * @queryParam filtered Array of filtered item. e.g: filter record by author_name [{"author_name":"Joe Jambul"}]. Example: filtered[0][id]=author_name&filtered[0][value]=Joe Jambul
     * 
     * @response {
     * "data": [
            {
                "id": 1,
                "author_name": "CESB",
                "alias_name": null,
                "email": null,
                "avatar": null
            },
            {
                "id": 2,
                "author_name": "epie",
                "alias_name": null,
                "email": null,
                "avatar": null
            },
            {
                "id": 3,
                "author_name": "Halia",
                "alias_name": null,
                "email": null,
                "avatar": null
            }
        ],
        "meta": {
            "total": 3
        }
     * }
     */
    public function index(Request $request)
    {
        $available_key = ['id', 'author_name', 'alias_name', 'email', 'avatar'];
        $paginate = Pagination::paginate($request, new Author, $available_key);

        return AuthorsResource::collection($paginate['data'])->additional(['meta' => ['total' => $paginate['count']]]);
    }

    /**
     * Add new author
     *
     * @bodyParam author_name string required Name of the author. Example: Joe Jambul
     * @bodyParam alias_name string Alias name of the author. Example: Joe Jambul
     * @bodyParam email string Email of the author. Example: joe.jambul@azuddin.com
     * @bodyParam avatar image Image of the author.
     * 
     * @response {
     * "message": "Author has been created"
     * }
     */
    public function store(Request $request)
    {
        $validated = $request->validate([
            'author_name' => 'required|unique:authors',
            'alias_name' => 'sometimes',
            'email' => 'sometimes|email',
            'avatar' => 'sometimes|image'
        ]);

        $avatar = isset($validated['avatar']) ? $validated['avatar'] : null;
        $validated['avatar'] = "";

        $author = new Author;
        $author->fill($validated);
        $author->save();

        if (!empty($avatar)) {
            $author->avatar = Upload::uploadAuthorAvatar($author, $avatar);
            $author->save();
        }

        Cache::put('category:' . $author->id, $author);

        return response(['message' => 'Author has been created']);
    }

    /**
     * Get author details
     *
     * @queryParam author required Id of the author. Example: 1
     * 
     * @response {
     * "data": {
            "id": 1,
            "author_name": "CESB",
            "alias_name": null,
            "email": null,
            "avatar": null,
            "created_at": "22-03-19"
        }
     * }
     */
    public function show(Author $author)
    {
        if ($author == null) return response(['error' => 'Author not found'], 400);

        return (new AuthorResource($author));
    }

    /**
     * Update author detail
     *
     * @queryParam author required Id of author. Example: 1
     * @bodyParam author_name string required Name of the author. Example: Stan Lee
     * @bodyParam alias_name string Alias name of the author. Example: Stan Lee
     * @bodyParam email string Email of the author. Example: stan.lee@azuddin.com
     * @bodyParam avatar image Avatar of the author.
     * 
     * @response {
     * "message": "Author has been updated"
     * }
     */
    public function update(Request $request, Author $author)
    {
        if ($author == null) return response(['error' => 'Author not found'], 400);

        $validated = $request->validate([
            'author_name' => 'required|unique:authors',
            'alias_name' => 'sometimes',
            'email' => 'sometimes|email',
            'avatar' => 'sometimes|image'
        ]);

        $validated['avatar'] = isset($validated['avatar']) ? Upload::uploadAuthorAvatar($author, $validated['avatar']) : $author->avatar;

        $author->fill($validated);
        $author->save();

        Cache::put('category:' . $author->id, $author);

        return response(['message' => 'Author has been updated']);
    }

    /**
     * Remove author
     *
     * @queryParam author required Id of author. Example: 1
     * 
     * @response {
     * "message": "Author has been deleted"
     * }
     */
    public function destroy(Author $author)
    {
        if ($author == null) return response(['error' => 'Author not found'], 400);

        $author->delete();

        return response(['message' => 'Author has been deleted']);
    }
}
