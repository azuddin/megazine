<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Traits\Pagination;
use App\Http\Resources\BundlesResource;
use App\Bundle;
use App\Http\Resources\BundleResource;
use App\Product;
use App\Traits\Upload;
use App\BundleImage;

/**
 * @group (Admin) Bundle
 */
class BundleController extends Controller
{

    /**
     * Get bundle list
     *
     * @queryParam pageSize List page size. Example: 10
     * @queryParam page Page number. Example: 0
     * @queryParam sorted Array of sorted item. e.g: sort descending by bundle_name [{"bundle_name":"true"}]. Example: sorted[0][id]=bundle_name&sorted[0][desc]=false
     * @queryParam filtered Array of filtered item. e.g: filter record by bundle_name [{"bundle_name":"Bundle Raya"}]. Example: filtered[0][id]=bundle_name&filtered[0][value]=Bundle Raya
     * 
     * @response {
     * "data": [
            {
                "id": 1,
                "bundle_name": "Molestiae corporis qui dolorem id.",
                "bundle_desc": "Voluptates quis ut rerum officiis. Molestias vitae qui deleniti porro nisi.\n\nNulla impedit suscipit harum et. Blanditiis corrupti modi qui dolores consequatur. Quos dolorem sequi architecto ut mollitia. Laborum voluptatum perspiciatis sit quibusdam eaque aut praesentium.",
                "images": [],
                "bundle_items_count": 3,
                "price": "21.14"
            }
        ],
        "meta": {
            "total": 1
        }
     * }
     */
    public function index(Request $request)
    {
        $available_key = ['id', 'bundle_name', 'bundle_desc', 'price'];
        $paginate = Pagination::paginate($request, new Bundle, $available_key);

        return BundlesResource::collection($paginate['data'])->additional(['meta' => ['total' => $paginate['count']]]);
    }

    /**
     * Add new bundle
     *
     * @bodyParam bundle_name string required Name of the bundle. Example: bundle A
     * @bodyParam bundle_desc string required Description of the bundle. Example: lorem ipsum
     * @bodyParam price double required Price of the bundle. Example: 10.23
     * @bodyParam bundle_images array Array of bundle image.
     * 
     * @response {
     * "message": "Bundle has been created"
     * }
     */
    public function store(Request $request)
    {
        $validated = $request->validate([
            'bundle_name' => 'required',
            'bundle_desc' => 'required',
            'price' => 'required|numeric',
            'bundle_images' => 'sometimes|array',
            'bundle_images.*' => 'sometimes|image'
        ]);

        $bundle = new Bundle;
        $bundle->bundle_name = $validated['bundle_name'];
        $bundle->bundle_desc = $validated['bundle_desc'];
        $bundle->price = $validated['price'];
        $bundle->save();

        if (count($validated['bundle_images']) > 0) {
            foreach ($validated['bundle_images'] as $image) {
                $storeUrl = Upload::uploadBundleImage($bundle->id, $image);
                BundleImage::create([
                    'bundle_id' => $bundle->id,
                    'url' => $storeUrl
                ]);
            }
        }

        return response(['message' => 'Bundle has been created']);
    }

    /**
     * Get bundle details
     *
     * @queryParam bundle required Id of the bundle. Example: 1
     * 
     * @response {
     * "data": {
            "id": 1,
            "bundle_name": "Molestiae corporis qui dolorem id.",
            "bundle_desc": "Voluptates quis ut rerum officiis. Molestias vitae qui deleniti porro nisi.\n\nNulla impedit suscipit harum et. Blanditiis corrupti modi qui dolores consequatur. Quos dolorem sequi architecto ut mollitia. Laborum voluptatum perspiciatis sit quibusdam eaque aut praesentium.",
            "images": [],
            "items": [
                {
                    "id": 1,
                    "product": {
                        "id": 1,
                        "product_name": "Gila-Gila",
                        "product_desc": null,
                        "product_type": "normal",
                        "author_id": 6,
                        "category_id": 2,
                        "price": "0.00",
                        "stock": 1,
                        "hit": null,
                        "likes": "0",
                        "is_public": 1,
                        "product_images": [
                            {
                                "url": "gg/gg001/gg00101.jpg",
                                "collection_no": 0,
                                "order": 1
                            }
                        ]
                    },
                    "price": "0.00",
                    "quantity": 1
                }
            ],
            "price": "21.14"
        }
     * }
     */
    public function show(Bundle $bundle)
    {
        if ($bundle == null) return response(['error' => 'Bundle not found'], 400);

        return (new BundleResource($bundle));
    }

    /**
     * Update bundle detail
     *
     * @queryParam bundle required Id of bundle. Example: 1
     * @bodyParam bundle_name string required Name of the bundle. Example: bundle AB
     * @bodyParam bundle_desc string required Description of the bundle. Example: sit amet dolor
     * @bodyParam price double required Price of the bundle. Example: 13.23
     * @bodyParam bundle_images array Array of bundle image.
     * 
     * @response {
     * "message": "Bundle has been updated"
     * }
     */
    public function update(Request $request, Bundle $bundle)
    {
        if ($bundle == null) return response(['error' => 'Bundle not found'], 400);

        $validated = $request->validate([
            'bundle_name' => 'required',
            'bundle_desc' => 'required',
            'price' => 'required|numeric',
            'bundle_images' => 'sometimes|array',
            'bundle_images.*' => 'sometimes|image'
        ]);

        $bundle->bundle_name = $validated['bundle_name'];
        $bundle->bundle_desc = $validated['bundle_desc'];
        $bundle->price = $validated['price'];
        $bundle->save();

        if (count($validated['bundle_images']) > 0) {
            foreach ($validated['bundle_images'] as $image) {
                $storeUrl = Upload::uploadBundleImage($bundle->id, $image);
                BundleImage::create([
                    'bundle_id' => $bundle->id,
                    'url' => $storeUrl
                ]);
            }
        }

        return response(['message' => 'Bundle has been updated']);
    }

    /**
     * Remove bundle
     *
     * @queryParam bundle required Id of bundle. Example: 1
     * 
     * @response {
     * "message": "Bundle has been deleted"
     * }
     */
    public function destroy(Bundle $bundle)
    {
        if ($bundle == null) return response(['error' => 'Bundle not found'], 400);

        $bundle->delete();

        return response(['message' => 'Bundle has been deleted']);
    }

    /**
     * Remove bundle image
     *
     * @queryParam bundleImage required Id of bundle's image. Example: 1
     * 
     * @response {
     * "message": "Bundle's image has been deleted"
     * }
     */
    public function destroyBundleImage(BundleImage $bundleImage)
    {
        if ($bundleImage == null) return response(['error' => 'Bundle\'s image not found'], 400);

        $bundleImage->delete();

        return response(['message' => 'Bundle\'s image has been deleted']);
    }

    /**
     * Attach a product
     * 
     * @queryParam bundle required Id of the bundle. Example: 1
     * @bodyParam product_id integer required Id of the product. Example: 1
     * 
     * @response{
     * "message": "Product has been attached"
     * }
     */
    public function attach_product(Request $request, Bundle $bundle)
    {
        $validated = $request->validate([
            'product_id' => 'required|exists:products,id'
        ]);

        if ($bundle == null) return response(['error' => 'Bundle not found'], 400);
        $product = Product::find($validated['product_id']);

        $bundle->attachItem($product);

        return response(['message' => 'Product has been attached']);
    }

    /**
     * Detach a product
     * 
     * @queryParam bundle required Id of the bundle. Example: 1
     * @bodyParam product_id integer required Id of the product. Example: 1
     * 
     * @response{
     * "message": "Product has been detached"
     * }
     */
    public function detach_product(Request $request, Bundle $bundle)
    {
        $validated = $request->validate([
            'product_id' => 'required|exists:products,id'
        ]);

        if ($bundle == null) return response(['error' => 'Bundle not found'], 400);
        $product = Product::find($validated['product_id']);

        $bundle->detachItem($product);

        return response(['message' => 'Product has been detached']);
    }
}
