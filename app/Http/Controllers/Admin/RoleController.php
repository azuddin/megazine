<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Traits\Pagination;
use Spatie\Permission\Models\Role;
use App\Http\Resources\RoleResource;
use App\Http\Resources\RolesResource;
use Spatie\Permission\Models\Permission;
use App\Http\Resources\PermissionsResource;

/**
 * @group (Admin) Role
 */
class RoleController extends Controller
{

    /**
     * Get role list
     *
     * @queryParam pageSize List page size. Example: 10
     * @queryParam page Page number. Example: 0
     * @queryParam sorted Array of sorted item. e.g: sort descending by name [{"name":"true"}]. Example: sorted[0][id]=name&sorted[0][desc]=false
     * @queryParam filtered Array of filtered item. e.g: filter record by name [{"name":"author"}]. Example: filtered[0][id]=name&filtered[0][value]=author
     * 
     * @response {
     * "data": [
            {
                "id": 2,
                "name": "admin"
            },
            {
                "id": 3,
                "name": "finance"
            },
            {
                "id": 4,
                "name": "author"
            }
        ],
        "meta": {
            "total": 4
        }
     * }
     */
    public function index(Request $request)
    {
        $available_key = ['id', 'name'];
        $paginate = Pagination::paginate($request, new Role, $available_key);

        return RolesResource::collection($paginate['data'])->additional(['meta' => ['total' => $paginate['count']]]);
    }

    /**
     * Add new role
     *
     * @bodyParam name string required Name of the role. Example: author
     * 
     * @response {
     * "message": "Role has been created"
     * }
     */
    public function store(Request $request)
    {
        $validated = $request->validate([
            'name' => 'required|unique:roles'
        ]);

        Role::create(['name' => $validated['name']]);

        return response(['message' => 'Role has been created']);
    }

    /**
     * Get role details
     *
     * @queryParam role required Id of the role. Example: 1
     * 
     * @response {
     * "data": {
            "id": 2,
            "name": "admin",
            "created_at": "22-03-2019",
            "permission": [
                {
                    "id": 1,
                    "name": "view_admins",
                    "created_at": "22-03-2019"
                },
                {
                    "id": 2,
                    "name": "manage_admins",
                    "created_at": "22-03-2019"
                }
            ]
        }
     * }
     */
    public function show(Role $role)
    {
        if ($role == null || $role->id == 1) return response(['error' => 'Role not found'], 400);

        return new RoleResource($role);
    }

    /**
     * Update role detail
     *
     * @bodyParam name string required Name of the role. Example: Author
     * 
     * @queryParam role required Id of role. Example: 1
     * 
     * @response {
     * "message": "Role has been updated"
     * }
     */
    public function update(Request $request, Role $role)
    {
        if ($role == null || $role->id == 1) return response(['error' => 'Role not found'], 400);

        $validated = $request->validate([
            'name' => 'required|unique:roles'
        ]);

        $role->name = $validated['name'];
        $role->save();

        return response(['message' => 'Role has been updated']);
    }

    /**
     * Remove role
     *
     * @queryParam role required Id of role. Example: 1
     * 
     * @response {
     * "message": "Role has been deleted"
     * }
     */
    public function destroy(Role $role)
    {
        if ($role == null || $role->id == 1) return response(['error' => 'Role not found'], 400);

        $role->delete();

        return response(['message' => 'Role has been deleted']);
    }

    /**
     * Give permission
     * 
     * @bodyParam permission_id integer required Id of the permission. Example: 1
     * 
     * @queryParam role required Id of the role. Example: 1
     * 
     * @response {
     * "message": "Permission has been given"
     * }
     */
    public function permission_attach(Request $request, Role $role)
    {
        $validated = $request->validate([
            'permission_id' => 'required|exists:permissions,id'
        ]);

        $permission = Permission::findById($validated['permission_id']);

        if ($role->id == 1) return response(['error' => 'Role not found'], 400);
        $role->givePermissionTo($permission);

        return response(['message' => 'Permission has been given']);
    }

    /**
     * Revoke permission
     * 
     * @bodyParam permission_id integer required Id of the permission. Example: 1
     * 
     * @queryParam role required Id of the role. Example: 1
     * 
     * @response {
     * "message": "Permission has been revoked"
     * }
     */
    public function permission_detach(Request $request, Role $role)
    {
        $validated = $request->validate([
            'permission_id' => 'required|exists:permissions,id'
        ]);

        $permission = Permission::findById($validated['permission_id']);

        if ($role->id == 1) return response(['error' => 'Role not found'], 400);
        $role->revokePermissionTo($permission);

        return response(['message' => 'Permission has been revoked']);
    }


    /**
     * Add multiple permission
     * 
     * @bodyParam permission_id array required Id of the permission.
     * 
     * @queryParam role required Id of the role. Example: 1
     * 
     * @response {
     * "message": "Permission has been revoked"
     * }
     */
    public function permission_multiple(Request $request, Role $role)
    {
        $validated = $request->validate([
            'permission_id' => 'required|array',
            'permission_id.*' => 'exists:permissions,id'
        ]);

        $permissions = Permission::whereIn('id', $validated['permission_id'])->get();

        if ($role->id == 1) return response(['error' => 'Role not found'], 400);
        $role->syncPermissions([]);
        $role->syncPermissions($permissions);

        return response(['message' => 'Permissions has been set']);
    }
}
