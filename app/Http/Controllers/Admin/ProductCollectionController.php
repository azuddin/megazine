<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\AdminProductCollectionsResource;
use App\Http\Resources\AdminProductCollectionResource;
use App\Traits\Pagination;
use App\ProductCollection;
use Illuminate\Support\Facades\Cache;

/**
 * @group (Admin) Product Collection
 */
class ProductCollectionController extends Controller
{
    /**
     * Get product image collection
     *
     * @queryParam pageSize List page size. Example: 10
     * @queryParam page Page number. Example: 0
     * @queryParam sorted Array of sorted item. e.g: sort descending by collection_name [{"collection_name":"true"}]. Example: sorted[0][id]=collection_name&sorted[0][desc]=false
     * @queryParam filtered Array of filtered item. e.g: filter record by collection_name [{"collection_name":"pok@ pai 3"}]. Example: filtered[0][id]=collection_name&filtered[0][value]=pok@ pai 3
     * 
     * @response {
     * "data": [
            {
                "id": 1,
                "product_id": 1,
                "collection_name": "cover",
                "collection_slug": "cover",
                "created_at": "24-03-2019"
            },
            {
                "id": 2,
                "product_id": 1,
                "collection_name": "gila gila 1",
                "collection_slug": "gila-gila-1",
                "created_at": "24-03-2019"
            },
            {
                "id": 3,
                "product_id": 1,
                "collection_name": "gila gila 788",
                "collection_slug": "gila-gila-788",
                "created_at": "24-03-2019"
            }
        ],
        "meta": {
            "total": 3
        }
     * }
     */
    public function index(Request $request)
    {
        $available_key = ['id', 'product_id', 'collection_name', 'collection_slug', 'created_at'];
        $paginate = Pagination::paginate($request, new ProductCollection, $available_key);

        return AdminProductCollectionsResource::collection($paginate['data'])->additional(['meta' => ['total' => $paginate['count']]]);
    }

    /**
     * Add new product image collection
     *
     * @bodyParam product_id integer required Id of the product. Example: 1
     * @bodyParam collection_name string required Title of the Collection. Example: siri istimewa hari raya 789
     * @bodyParam is_hidden boolean Status of the product whether hidden or not. Example: 1
     * 
     * @response {
     * "message": "Collection has been created"
     * }
     */
    public function store(Request $request)
    {
        $validated = $request->validate([
            'product_id' => 'required|exists:products,id',
            'collection_name' => 'required|string',
            'is_hidden' => 'sometimes|in:0,1',
        ]);

        $productCollection = new ProductCollection;
        $productCollection->fill($validated);
        $productCollection->save();

        Cache::put('product:' . $validated['product_id'] . ':collection:' . $productCollection->id, $productCollection);

        return response(['message' => 'Collection has been created', 'data' => new AdminProductCollectionsResource($productCollection)]);
    }

    /**
     * Get product image collection details
     *
     * @queryParam productCollection required Id of the product image collection. Example: 1
     * 
     * @response {
     * "data": {
            "id": 1,
            "product_id": 1,
            "collection_name": "cover",
            "collection_slug": "cover",
            "created_at": "24-03-2019"
        }
     * }
     */
    public function show(ProductCollection $productCollection)
    {
        if ($productCollection == null) return response(['error' => 'Collection not found'], 400);

        return (new AdminProductCollectionResource($productCollection));
    }

    /**
     * Update product image collection detail
     *
     * @bodyParam product_id integer Id of the product. Example: 1
     * @bodyParam collection_name string Title of the Collection. Example: siri istimewa hari raya 789
     * @bodyParam is_hidden boolean Status of the product whether hidden or not. Example: 1
     * 
     * @queryParam productCollection required Id of the product image collection. Example: 1
     * 
     * @response {
     * "message": "Collection has been updated"
     * }
     */
    public function update(Request $request, ProductCollection $productCollection)
    {
        if ($productCollection == null) return response(['error' => 'Collection not found'], 400);

        $validated = $request->validate([
            'product_id' => 'sometimes|exists:products,id',
            'collection_name' => 'sometimes|string',
            'is_hidden' => 'sometimes|in:0,1',
        ]);

        $productCollection->fill($validated);
        $productCollection->save();

        Cache::put('product:' . $productCollection->product_id . ':collection:' . $productCollection->id, $productCollection);

        return response(['message' => 'Collection has been updated']);
    }

    /**
     * Remove product image collection
     *
     * @queryParam productCollection required Id of product's collection. Example: 1
     * 
     * @response {
     * "message": "Collection has been deleted"
     * }
     */
    public function destroy(ProductCollection $productCollection)
    {
        if ($productCollection == null) return response(['error' => 'Collection not found'], 400);

        $productCollection->delete();

        return response(['message' => 'Collection has been deleted']);
    }
}
