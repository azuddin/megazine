<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Traits\Pagination;
use App\Notifications\SignupActivation;
use App\Http\Resources\UsersResource;
use App\User;
use App\Http\Resources\AdminUserResource;
use App\Traits\Upload;

/**
 * @group (Admin) User
 */
class UserController extends Controller
{
    use Pagination, Upload;
    /**
     * Get user list
     *
     * @queryParam pageSize List page size. Example: 10
     * @queryParam page Page number. Example: 0
     * @queryParam sorted Array of sorted item. e.g: sort descending by user_name [{"user_name":"true"}]. Example: sorted[0][id]=user_name&sorted[0][desc]=false
     * @queryParam filtered Array of filtered item. e.g: filter record by user_name [{"user_name":"Aut dignissimos"}]. Example: filtered[0][id]=user_name&filtered[0][value]=Aut dignissimos
     * 
     * @response {
     * "data": [
            {
                "id": 1,
                "name": "Ahmad Azuddin",
                "email": "ahmad@azuddin.com",
                "phone_no": null,
                "created_at": "22-03-2019"
            }
        ],
        "meta": {
            "total": 1
        }
     * }
     */
    public function index(Request $request)
    {
        $available_key = ['id', 'name', 'email', 'phone_no', 'created_at'];
        $paginate = Pagination::paginate($request, new User, $available_key);

        return AdminUserResource::collection($paginate['data'])->additional(['meta' => ['total' => $paginate['count']]]);
    }

    /**
     * Add new user
     *
     * @bodyParam email string required Email of the user account. Example: ahmad@azuddin.com
     * @bodyParam name string required Name of the account. Example: azuddin
     * @bodyParam phone_no string Phone number of the user. Example: 0123456789
     * @bodyParam avatar image Image for avatar of the user
     * 
     * @response {
     * "data": {
            "id": 1,
            "name": "azuddin",
            "email": "ahmad@azuddin.com",
            "phone_no": "0123456789",
            "avatar": "http://megazine.test/avatar/user/12-1553341920.png",
            "created_at": "23-03-2019"
        }
     * }
     */
    public function store(Request $request)
    {
        $validated = $this->validate($request, [
            'name' => 'required',
            'email' => 'required|email|unique:users',
            'phone_no' => 'sometimes|numeric',
            'avatar' => 'sometimes|image'
        ]);

        $validated['password'] = bcrypt(str_random(8));
        $validated['password'] = bcrypt($validated['password']);
        $avatar = isset($validated['avatar']) ? $validated['avatar'] : null;
        $validated['avatar'] = "";

        $user = new User;
        $user->fill($validated);
        $user->activation_token = str_random(60);
        $user->save();

        if (!empty($avatar)) {
            $user->avatar = $this->uploadUserAvatar($user, $avatar);
            $user->save();
        } else {
            $user->avatar = env('APP_URL') . '/storage/avatar-' . rand(1, 9) . '.png';
            $user->save();
        }

        $user->notify(new SignupActivation($user));

        return (new AdminUserResource($user));
    }

    /**
     * Get user details
     *
     * @queryParam user required Id of the user. Example: 1
     * 
     * @response {
     * "data": {
            "id": 12,
            "name": "azuddin",
            "email": "norawe@azuddin.com",
            "phone_no": "0123456789",
            "avatar": "http://megazine.test/avatar/user/12-1553341920.png",
            "created_at": "23-03-2019"
        }
     * }
     */
    public function show(User $user)
    {
        if ($user == null) return response(['error' => 'User not found'], 400);

        return (new AdminUserResource($user));
    }

    /**
     * Update user detail
     *
     * @bodyParam email string required Email of the user account. Example: ahmad@azuddin.com
     * @bodyParam name string required Name of the account. Example: azuddin
     * @bodyParam phone_no string Phone number of the user. Example: 0123456789
     * @bodyParam avatar image Image for avatar of the user
     * 
     * @queryParam user required Id of user. Example: 1
     * 
     * @response {
     * "data": {
            "id": 1,
            "name": "azuddin",
            "email": "ahmad@azuddin.com",
            "phone_no": "0123456789",
            "avatar": "http://megazine.test/avatar/user/12-1553341920.png",
            "created_at": "23-03-2019"
        }
     * }
     */
    public function update(Request $request, User $user)
    {
        if ($user == null) return response(['error' => 'User not found'], 400);

        $validated = $this->validate($request, [
            'name' => 'string',
            'email' => 'email|unique:users,email,' . $user->id,
            'phone_no' => 'sometimes',
            'avatar' => 'sometimes|image'
        ]);

        $validated['avatar'] = isset($validated['avatar']) ? $this->uploadUserAvatar($user, $validated['avatar']) : $user->avatar;
        $user->fill($validated);
        $user->save();

        return new AdminUserResource($user);
    }

    /**
     * Remove user
     *
     * @queryParam user required Id of user. Example: 1
     * 
     * @response {
     * "message": "User has been deleted"
     * }
     */
    public function destroy(User $user)
    {
        if ($user == null) return response(['error' => 'User not found'], 400);

        $user->delete();

        return response(['message' => 'User has been deleted']);
    }

    public function nonAdmin()
    {
        $user = User::doesntHave('roles')->get();

        return UsersResource::collection($user);
    }
}
