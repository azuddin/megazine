<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Traits\Pagination;
use App\User;
use App\Http\Resources\AdminsResource;
use App\Http\Resources\AdminResource;
use Spatie\Permission\Models\Role;
use App\Http\Resources\UserResource;

/**
 * @group (Admin) Admin
 */
class AdminController extends Controller
{

    /**
     * Get admin list
     * 
     * @queryParam pageSize List page size. Example: 10
     * @queryParam page Page number. Example: 0
     * @queryParam sorted Array of sorted item. e.g: sort descending by user_name [{"user_name":"true"}]. Example: sorted[0][id]=user_name&sorted[0][desc]=false
     * @queryParam filtered Array of filtered item. e.g: filter record by user_name [{"user_name":"azuddin"}]. Example: filtered[0][id]=user_name&filtered[0][value]=azuddin
     * 
     * @response {
     * "data": [
            {
                "id": 1,
                "name": "Ahmad Azuddin",
                "email": "ahmad@azuddin.com",
                "phone_no": null,
                "created_at": "22-03-2019"
            }
        ],
        "meta": {
            "total": 1
        }
     * }
     */
    public function index(Request $request)
    {
        $available_key = ['id', 'email', 'phone_no', 'created_at'];
        $paginate = Pagination::paginate($request, User::role('admin'), $available_key);

        return AdminsResource::collection($paginate['data'])->additional(['meta' => ['total' => $paginate['count']]]);
    }

    /**
     * Get admin details
     *
     * @queryParam admin required Id of the admin. Example: 1
     * 
     * @response {
     * "data": {
            "id": 1,
            "name": "Ahmad Azuddin",
            "email": "ahmad@azuddin.com",
            "phone_no": null,
            "created_at": "04-04-2019",
            "roles": [
                {
                    "id": 2,
                    "name": "admin"
                }
            ]
        }
     * }
     */
    public function show(User $admin)
    {
        if (!$admin->hasRole('admin')) return response(['error' => 'Admin not found'], 400);

        return (new AdminResource($admin));
    }

    /**
     * Attach role to an admin
     * 
     * @bodyParam role_id integer required Id of the role. Example: 1
     * @bodyParam user_id integer required Id of the user. Example: 1
     * 
     * @response {
     * "message": "Role has been added"
     * }
     */
    public function roles_attach(Request $request)
    {
        $validated = $request->validate([
            'role_id' => 'required|exists:roles,id',
            'user_id' => 'required|exists:users,id'
        ]);

        $role = Role::findById($validated['role_id'], 'web');
        $user = User::find($validated['user_id']);
        $user->assignRole($role->name);

        return response(['message' => 'Role has been added']);
    }

    /**
     * Detach role from an admin
     * 
     * @bodyParam role_id integer required Id of the role. Example: 1
     * @bodyParam user_id integer required Id of the user. Example: 1
     * 
     * @response {
     * "message": "Role has been removed"
     * }
     */
    public function roles_detach(Request $request)
    {
        $validated = $request->validate([
            'role_id' => 'required|exists:roles,id',
            'user_id' => 'required|exists:users,id'
        ]);

        $role = Role::findById($validated['role_id'], 'web');
        $user = User::find($validated['user_id']);
        $user->removeRole($role->name);
        if ($role->name == 'admin') $user->syncRoles([]);

        return response(['message' => 'Role has been removed']);
    }
}
