<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Traits\Pagination;
use App\Product;
use App\Http\Resources\ProductsResource;
use App\Http\Resources\AdminProductResource;
use App\Traits\Upload;
use App\ProductImage;
use App\ProductCollection;
use Illuminate\Support\Facades\Cache;

/**
 * @group (Admin) Product
 */
class ProductController extends Controller
{

    /**
     * Get product list
     *
     * @queryParam pageSize List page size. Example: 10
     * @queryParam page Page number. Example: 0
     * @queryParam sorted Array of sorted item. e.g: sort descending by product_name [{"product_name":"true"}]. Example: sorted[0][id]=product_name&sorted[0][desc]=false
     * @queryParam filtered Array of filtered item. e.g: filter record by product_name [{"product_name":"Product A"}]. Example: filtered[0][id]=product_name&filtered[0][value]=Product A
     * 
     * @response {
     * "data": [
            {
                "id": 1,
                "product_name": "Gila-Gila",
                "product_desc": null,
                "product_type": "normal",
                "author_id": 6,
                "category_id": 2,
                "price": "0.00",
                "stock": 1,
                "hit": null,
                "likes": "0",
                "is_public": 1,
                "product_images": [
                    {
                        "url": "gg/gg001/gg00101.jpg",
                        "collection_no": 0,
                        "order": 1
                    }
                ]
            }
        ],
        "meta": {
            "total": 1
        }
     * }
     */
    public function index(Request $request)
    {
        $available_key = ['id', 'product_name', 'product_desc', 'product_type', 'author_id', 'category_id', 'price', 'stock', 'is_public', 'hit', 'likes'];
        $paginate = Pagination::paginate($request, new Product, $available_key);

        return ProductsResource::collection($paginate['data'])->additional(['meta' => ['total' => $paginate['count']]]);
    }

    /**
     * Add new product
     *
     * @bodyParam product_name string required Name of the product. Example: product A
     * @bodyParam product_desc string required Description of the product. Example: lorem ipsum
     * @bodyParam product_type string required Type of the product. Example: normal
     * @bodyParam price double required Price of the product. Example: 10.23
     * @bodyParam stock double required Stock of the product. Example: 1
     * @bodyParam author_id integer required Author of the product. Example: 1
     * @bodyParam category_id integer required Category of the product. Example: 1
     * @bodyParam product_images array Array of product image.
     * 
     * @response {
     * "message": "Product has been created"
     * }
     */
    public function store(Request $request)
    {
        $validated = $request->validate([
            'product_name' => 'required',
            'product_desc' => 'required',
            'product_type' => 'required|in:' . implode(',', Product::TYPE_ARRAY),
            'price' => 'required|numeric',
            'stock' => 'required|numeric',
            'author_id' => 'required|exists:authors,id',
            'category_id' => 'required|exists:categories,id',
            'product_images' => 'sometimes|array',
            'product_images.*.image' => 'required|image',
            'product_images.*.collection_name' => 'required',
            'product_images.*.order' => 'sometimes|numeric'
        ]);

        $product = new Product;
        $product->product_name = $validated['product_name'];
        $product->product_desc = $validated['product_desc'];
        $product->product_type = $validated['product_type'];
        $product->price = $validated['price'];
        $product->stock = $validated['stock'];
        $product->author_id = $validated['author_id'];
        $product->category_id = $validated['category_id'];
        $product->save();

        Cache::put('product:' . $product->id, $product);

        if (isset($validated['product_images']) && count($validated['product_images']) > 0) {
            foreach ($validated['product_images'] as $img) {
                //firstOrCreate collection
                $productCollection = ProductCollection::firstOrCreate([
                    'product_id' => $product->id,
                    'collection_name' => $img['collection_name']
                ]);

                //insert into that collection
                $storedUrl = Upload::uploadProductImage($product->id, $productCollection->id, $img['image']);
                $image = new ProductImage(['url' => $storedUrl, 'order' => isset($img['order']) ? $img['order'] : 1]);
                $productCollection->images()->save($image);
            }
        }

        return response(['message' => 'Product has been created']);
    }

    /**
     * Get product details
     *
     * @queryParam product required Id of the product. Example: 1
     * 
     * @response {
     * "data": {
            "id": 1,
            "product_name": "Gila-Gila",
            "product_desc": null,
            "product_type": "normal",
            "author_id": 6,
            "category_id": 2,
            "price": "0.00",
            "stock": 1,
            "hit": null,
            "likes": "0",
            "product_images": [
                {
                    "url": "gg/gg001/gg00101.jpg",
                    "collection_no": 0,
                    "order": 1
                }
            ],
            "collections": [
                {
                    "collection_no": 1
                },
                {
                    "collection_no": 788
                },
                {
                    "collection_no": 789
                },
                {
                    "collection_no": 811
                },
                {
                    "collection_no": 812
                }
            ]
        }
     * }
     */
    public function show(Product $product)
    {
        if ($product == null) return response(['error' => 'Product not found'], 400);

        return (new AdminProductResource($product));
    }

    /**
     * Update product detail
     *
     * @bodyParam product_name string required Name of the product. Example: product AB
     * @bodyParam product_desc string required Description of the product. Example: sit amet dolor
     * @bodyParam product_type string required Type of the product. Example: normal
     * @bodyParam price double required Price of the product. Example: 13.23
     * @bodyParam stock double required Stock of the product. Example: 2
     * @bodyParam author_id integer required Author of the product. Example: 1
     * @bodyParam category_id integer required Category of the product. Example: 1
     * @bodyParam product_images array Array of product image.
     * 
     * @queryParam product required Id of product. Example: 1
     * 
     * @response {
     * "message": "Product has been updated"
     * }
     */
    public function update(Request $request, Product $product)
    {
        if ($product == null) return response(['error' => 'Product not found'], 400);

        $validated = $request->validate([
            'product_name' => 'required',
            'product_desc' => 'required',
            'product_type' => 'required|in:' . implode(',', Product::TYPE_ARRAY),
            'price' => 'required|numeric',
            'stock' => 'required|numeric',
            'author_id' => 'required|exists:authors,id',
            'category_id' => 'required|exists:categories,id',
            'product_images' => 'sometimes|array',
            'product_images.*.image' => 'required|image',
            'product_images.*.collection_name' => 'required',
            'product_images.*.order' => 'sometimes|numeric'
        ]);

        $product->product_name = $validated['product_name'];
        $product->product_desc = $validated['product_desc'];
        $product->product_type = $validated['product_type'];
        $product->price = $validated['price'];
        $product->stock = $validated['stock'];
        $product->author_id = $validated['author_id'];
        $product->category_id = $validated['category_id'];
        $product->save();

        Cache::put('product:' . $product->id, $product);

        if (isset($validated['product_images']) && count($validated['product_images']) > 0) {
            foreach ($validated['product_images'] as $img) {
                //firstOrCreate collection
                $productCollection = ProductCollection::firstOrCreate([
                    'product_id' => $product->id,
                    'collection_name' => $img['collection_name']
                ]);

                //insert into that collection
                $storedUrl = Upload::uploadProductImage($product->id, $productCollection->id, $img['image']);
                $image = new ProductImage(['url' => $storedUrl, 'order' => isset($img['order']) ? $img['order'] : 1]);
                $productCollection->images()->save($image);
            }
        }

        return response(['message' => 'Product has been updated']);
    }

    /**
     * Remove product
     *
     * @queryParam product required Id of product. Example: 1
     * 
     * @response {
     * "message": "Product has been deleted"
     * }
     */
    public function destroy(Product $product)
    {
        if ($product == null) return response(['error' => 'Product not found'], 400);

        $product->delete();

        return response(['message' => 'Product has been deleted']);
    }

    /**
     * Remove product image
     *
     * @queryParam productImage required Id of product's image. Example: 1
     * 
     * @response {
     * "message": "Product's image has been deleted"
     * }
     */
    public function destroyProductImage(ProductImage $productImage)
    {
        if ($productImage == null) return response(['error' => 'Product\'s image not found'], 400);

        $productImage->delete();

        return response(['message' => 'Product\'s image has been deleted']);
    }
}
