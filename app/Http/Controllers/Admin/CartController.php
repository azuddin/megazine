<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Traits\Pagination;
use App\Cart;
use App\Http\Resources\AdminCartResource;

/**
 * @group (Admin) Cart
 */
class CartController extends Controller
{

    /**
     * Get cart list
     *
     * @queryParam pageSize List page size. Example: 10
     * @queryParam page Page number. Example: 0
     * @queryParam sorted Array of sorted item. e.g: sort descending by cart_id [{"cart_id":"true"}]. Example: sorted[0][id]=cart_id&sorted[0][desc]=false
     * @queryParam filtered Array of filtered item. e.g: filter record by cart_id [{"cart_id":"Aut dignissimos"}]. Example: filtered[0][id]=cart_id&filtered[0][value]=Aut dignissimos
     * 
     * @response {
     * "data": [
            {
                "id": 1,
                "user_id": 2,
                "product_id": null,
                "bundle_id": 2,
                "price": "79.16",
                "quantity": 1,
                "created_at": "23-03-2019"
            }
        ],
        "meta": {
            "total": 1
        }
     * }
     */
    public function index(Request $request)
    {
        $available_key = ['id', 'user_id', 'product_id', 'bundle_id', 'price', 'quantity', 'created_at'];
        $paginate = Pagination::paginate($request, new Cart, $available_key);

        return AdminCartResource::collection($paginate['data'])->additional(['meta' => ['total' => $paginate['count']]]);
    }

    /**
     * Get cart details
     *
     * @queryParam cart required Id of the cart. Example: 1
     * 
     * @response {
     * "data": {
            "id": 1,
            "user_id": 2,
            "product_id": null,
            "bundle_id": 2,
            "price": "79.16",
            "quantity": 1,
            "created_at": "23-03-2019"
        }
     * }
     */
    public function show(Cart $cart)
    {
        if ($cart == null) return response(['error' => 'Cart not found'], 400);

        return (new AdminCartResource($cart));
    }
}
