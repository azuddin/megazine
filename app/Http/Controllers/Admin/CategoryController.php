<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Traits\Pagination;
use App\Category;
use App\Http\Resources\CategoriesResource;
use App\Http\Resources\CategoryResource;
use Illuminate\Support\Facades\Cache;

/**
 * @group (Admin) Category
 */
class CategoryController extends Controller
{

    /**
     * Get category list
     *
     * @queryParam pageSize List page size. Example: 10
     * @queryParam page Page number. Example: 0
     * @queryParam sorted Array of sorted item. e.g: sort descending by category_name [{"category_name":"true"}]. Example: sorted[0][id]=category_name&sorted[0][desc]=false
     * @queryParam filtered Array of filtered item. e.g: filter record by category_name [{"category_name":"Magazine"}]. Example: filtered[0][id]=category_name&filtered[0][value]=Magazine
     * 
     * @response {
     * "data": [
            {
                "id": 1,
                "category_name": "Komik"
            },
            {
                "id": 2,
                "category_name": "Majalah"
            },
            {
                "id": 3,
                "category_name": "Koleksi"
            }
        ],
        "meta": {
            "total": 5
        }
     * }
     */
    public function index(Request $request)
    {
        $available_key = ['id', 'category_name'];
        $paginate = Pagination::paginate($request, new Category, $available_key);

        return CategoriesResource::collection($paginate['data'])->additional(['meta' => ['total' => $paginate['count']]]);
    }

    /**
     * Add new category
     *
     * @bodyParam category_name string required Name of the category. Example: Magazine
     * 
     * @response {
     * "message": "Category has been created"
     * }
     */
    public function store(Request $request)
    {
        $validated = $request->validate([
            'category_name' => 'required|unique:categories'
        ]);

        $category = new Category;
        $category->fill($validated);
        $category->save();

        Cache::put('category:' . $category->id, $category);

        return response(['message' => 'Category has been created']);
    }

    /**
     * Get category details
     *
     * @queryParam category required Id of the category. Example: 1
     * 
     * @response {
     * "data": {
            "category_id": 1,
            "category_name": "Komik"
        }
     * }
     */
    public function show(Category $category)
    {
        if ($category == null) return response(['error' => 'Category not found'], 400);

        return (new CategoryResource($category));
    }

    /**
     * Update category detail
     *
     * @queryParam category required Id of category. Example: 1
     * @bodyParam category_name string required Name of the category. Example: Comic
     * 
     * @response {
     * "message": "Category has been updated"
     * }
     */
    public function update(Request $request, Category $category)
    {
        if ($category == null) return response(['error' => 'Category not found'], 400);

        $validated = $request->validate([
            'category_name' => 'required|unique:categories',
        ]);

        $category->fill($validated);
        $category->save();

        Cache::put('category:' . $category->id, $category);

        return response(['message' => 'Category has been updated']);
    }

    /**
     * Remove category
     *
     * @queryParam category required Id of category. Example: 1
     * 
     * @response {
     * "message": "Category has been deleted"
     * }
     */
    public function destroy(Category $category)
    {
        if ($category == null) return response(['error' => 'Category not found'], 400);

        $category->delete();

        return response(['message' => 'Category has been deleted']);
    }
}
