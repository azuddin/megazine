<?php

namespace App\Http\Controllers;

use App\Cart;
use App\Product;
use App\Bundle;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Traits\Pagination;
use App\Http\Resources\CartResource;

/**
 * @group Cart
 */
class CartController extends Controller
{

    /**
     * Get cart list
     *
     * Example url: /api/cart?pageSize=2&page=0&sorted[0][id]=price&sorted[0][desc]=false&filtered[0][id]=price&filtered[0][value]=78.48
     * 
     * @queryParam pageSize List page size. Example: 10
     * @queryParam page Page number. Example: 1
     * @queryParam sorted Array sorted item. e.g: sort descending by price [{"price":"true"}]. Example: sorted[0][id]=price&sorted[0][desc]=false
     * @queryParam filtered Array filtered item. e.g: filter record by price [{"price":"78.48"}]. Example: filtered[0][id]=price&filtered[0][value]=78.48
     * 
     * @response {
     * "data": [
            {
                "id": 1,
                "name": "Molestias velit et consectetur.",
                "product_id": null,
                "bundle_id": 2,
                "price": "79.16",
                "quantity": 1
            }
        ],
        "meta": {
            "total": 1
        }
     * }
     */
    public function index(Request $request)
    {
        $available_key = ['id', 'name', 'product_id', 'bundle_id', 'price', 'quantity'];
        $paginate = Pagination::paginate($request, new Cart, $available_key);

        return CartResource::collection($paginate['data'])->additional(['meta' => ['total' => $paginate['count']]]);
    }

    /**
     * Add an item into cart
     *
     * @bodyParam product_id integer required Id of the product. *Optional if bundle_id is exist. Example: 1
     * @bodyParam bundle_id integer Id of the bundle. Example: 2
     * @bodyParam quantity integer required Quantity of the item. Example: 1
     * 
     * @response {
     * "message": "Item has been added to cart"
     * }
     */
    public function create(Request $request)
    {
        $validated = $this->validate($request, [
            'product_id' => 'required_without:bundle_id|exists:products,id',
            'bundle_id' => 'sometimes|exists:bundles,id',
            'quantity' => 'required|numeric|gt:0',
        ]);

        $product = isset($validated['bundle_id']) ? null : Product::find($validated['product_id']);
        $bundle = isset($validated['bundle_id']) ? Bundle::find($validated['bundle_id']) : null;
        $user = Auth::user();

        if (!$user) return response(['error' => 'User not found']);

        $cart = Cart::firstOrNew([
            'user_id' => Auth::user()->id,
            'product_id' => $product == null ? null : $product->id,
            'bundle_id' => $bundle == null ? null : $bundle->id,
        ]);

        $cart->price = $bundle !== null ? $bundle->price : $product->price;
        $cart->quantity = $validated['quantity'];
        $cart->save();

        return response(['message' => 'Item has been added to cart']);
    }


    /**
     * Delete an item from cart
     *
     * @queryParam cart required Id of the cart item. Example: 1
     * 
     * @response {
     * "message": "Item has been deleted from cart"
     * }
     */
    public function destroy(Cart $cart)
    {
        if ($cart == null) return response(['error' => 'Item is invalid']);

        $cart->delete();

        return response(['message' => 'Item has been deleted from cart']);
    }
}
