<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Traits\Pagination;
use App\Bundle;
use App\Http\Resources\BundleResource;
use App\Http\Resources\BundlesResource;

/**
 * @group Bundle
 */
class BundleController extends Controller
{

    /**
     * Get bundle list
     *
     * Example url: /api/bundle?pageSize=2&page=0&sorted[0][id]=bundle_name&sorted[0][desc]=false&filtered[0][id]=bundle_name&filtered[0][value]=et
     * 
     * @queryParam pageSize List page size. Example: 10
     * @queryParam page Page number. Example: 1
     * @queryParam sorted Array of sorted item. e.g: sort descending by bundle_name [{"bundle_name":"true"}]. Example: sorted[0][id]=bundle_name&sorted[0][desc]=false
     * @queryParam filtered Array of filtered item. e.g: filter record by bundle_name [{"bundle_name":"Bundle Raya"}]. Example: filtered[0][id]=bundle_name&filtered[0][value]=Bundle Raya
     * 
     * @response {
     * "data": [
            {
                "id": 1,
                "bundle_name": "Molestiae corporis qui dolorem id.",
                "bundle_desc": "Voluptates quis ut rerum officiis. Molestias vitae qui deleniti porro nisi.\n\nNulla impedit suscipit harum et. Blanditiis corrupti modi qui dolores consequatur. Quos dolorem sequi architecto ut mollitia. Laborum voluptatum perspiciatis sit quibusdam eaque aut praesentium.",
                "images": [],
                "bundle_items_count": 3,
                "price": "21.14"
            }
        ],
        "meta": {
            "total": 1
        }
     * }
     */
    public function index(Request $request)
    {
        $available_key = ['id', 'bundle_name', 'bundle_desc', 'price'];
        $paginate = Pagination::paginate($request, new Bundle, $available_key);

        return BundlesResource::collection($paginate['data'])->additional(['meta' => ['total' => $paginate['count']]]);
    }

    /**
     * Get bundle details
     *
     * @queryParam bundle required Id of the bundle. Example: 1
     * 
     * @response {
     * "data": {
            "id": 1,
            "bundle_name": "Molestiae corporis qui dolorem id.",
            "bundle_desc": "Voluptates quis ut rerum officiis. Molestias vitae qui deleniti porro nisi.\n\nNulla impedit suscipit harum et. Blanditiis corrupti modi qui dolores consequatur. Quos dolorem sequi architecto ut mollitia. Laborum voluptatum perspiciatis sit quibusdam eaque aut praesentium.",
            "images": [],
            "items": [
                {
                    "id": 1,
                    "product": {
                        "id": 1,
                        "product_name": "Gila-Gila",
                        "product_desc": null,
                        "product_type": "normal",
                        "author_id": 6,
                        "category_id": 2,
                        "price": "0.00",
                        "stock": 1,
                        "hit": null,
                        "likes": "0",
                        "is_public": 1,
                        "product_images": [
                            {
                                "url": "gg/gg001/gg00101.jpg",
                                "collection_no": 0,
                                "order": 1
                            }
                        ]
                    },
                    "price": "0.00",
                    "quantity": 1
                }
            ],
            "price": "21.14"
        }
     * }
     */
    public function show(Bundle $bundle)
    {
        if ($bundle == null) return response(['error' => 'Bundle not found'], 400);

        return new BundleResource($bundle);
    }
}
