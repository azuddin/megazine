<?php

namespace App\Traits;

use App\Order;

trait Billplz
{
    public static function create_bill(Order $order, $callback_url, $redirect_url = '')
    {
        $data = [
            "collection_id" => env('BILLPLZ_COLLECTION_ID'),
            "email" => $order->user->email,
            "mobile" => '',
            "name" => $order->user->name,
            "amount" => $order->total * 100, //in cents
            "callback_url" => $callback_url,
            "redirect_url" => $redirect_url,
            "description" => $order->order_ref
        ];
        $process = curl_init(env('BILLPLZ_URL') . '/bills');
        $headers = array();
        $headers[] = 'Content-Type: application/json';
        curl_setopt($process, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($process, CURLOPT_USERPWD, env('BILLPLZ_SECRET') . ":");
        curl_setopt($process, CURLOPT_TIMEOUT, 30);
        curl_setopt($process, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($process, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($process, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($process, CURLOPT_POSTFIELDS, json_encode($data));

        $return = curl_exec($process);
        if (curl_errno($process)) {
            return ['error' => curl_error($process)];
        }
        curl_close($process);

        /**
         * sample response:
            "id": "vryiy04z"
            "collection_id": "ikhmwj0y"
            "paid": false
            "state": "due"
            "amount": 56
            "paid_amount": 0
            "due_at": "2019-3-4"
            "email": "ahmad@azuddin.com"
            "mobile": null
            "name": "AHMAD AZUDDIN"
            "url": "https://billplz-staging.herokuapp.com/bills/vryiy04z"
            "reference_1_label": "Reference 1"
            "reference_1": null
            "reference_2_label": "Reference 2"
            "reference_2": null
            "redirect_url": null
            "callback_url": "http://megazine.test/api/billplz-webhook"
            "description": "REF5386253829"
            "paid_at": null
         */
        return json_decode($return);
    }

    public static function get_bill($id)
    {
        $process = curl_init(env('BILLPLZ_URL') . '/bills/' . $id);
        $headers = array();
        $headers[] = 'Content-Type: application/json';
        curl_setopt($process, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($process, CURLOPT_USERPWD, env('BILLPLZ_SECRET') . ":");
        curl_setopt($process, CURLOPT_TIMEOUT, 30);
        curl_setopt($process, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($process, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($process, CURLOPT_SSL_VERIFYPEER, 0);

        $return = curl_exec($process);
        if (curl_errno($process)) {
            return ['error' => curl_error($process)];
        }
        curl_close($process);

        /**
         * sample response:
            "id": "677vvqip"
            "collection_id": "ikhmwj0y"
            "paid": false
            "state": "due"
            "amount": 56
            "paid_amount": 0
            "due_at": "2019-3-4"
            "email": "ahmad@azuddin.com"
            "mobile": null
            "name": "AHMAD AZUDDIN"
            "url": "https://billplz-staging.herokuapp.com/bills/677vvqip"
            "reference_1_label": "Reference 1"
            "reference_1": null
            "reference_2_label": "Reference 2"
            "reference_2": null
            "redirect_url": null
            "callback_url": "http://megazine.test/api/billplz-webhook"
            "description": "REF2417924125"
            "paid_at": null
         */
        return json_decode($return);
    }

    public static function validate_xsignature($requestArray, $is_redirect = false)
    {
        $x_signature = $requestArray['x_signature'];

        //#1 Extract all key-value pair parameters except x_signature,
        ksort($requestArray);
        unset($requestArray['x_signature']);

        //#2 Construct source string from each key-value pair parameters, 
        $mappedArr = array_map(function ($v, $k) use ($is_redirect) {
            return ($is_redirect ? 'billplz' : '') . $k . $v;
        }, $requestArray, array_keys($requestArray));

        //#3 Sort in ascending order, case-insensitive.
        asort($mappedArr);

        //#4 Combine sorted source strings with "|" (pipe) character in between
        $string = implode('|', $mappedArr);

        //#5 Compute x_signature by signing the final source in #4 using HMAC_SHA256 and the sample XSignature Key
        $computed_hash = hash_hmac('sha256', $string, env('BILLPLZ_XSIGNATURE'));

        //#6 Compare the computed x_signature with the x_signature passed in the request.
        if (strcmp($x_signature, $computed_hash) === 0) {
            return true;
        } else {
            return false;
        }
    }

    public static function deleteBill($id)
    {
        $process = curl_init(env('BILLPLZ_URL') . '/bills/' . $id);
        $headers = array();
        $headers[] = 'Content-Type: application/json';
        curl_setopt($process, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($process, CURLOPT_USERPWD, env('BILLPLZ_SECRET') . ":");
        curl_setopt($process, CURLOPT_TIMEOUT, 30);
        curl_setopt($process, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($process, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($process, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($process, CURLOPT_CUSTOMREQUEST, 'DELETE');


        $return = curl_exec($process);
        if (curl_errno($process)) {
            return ['error' => curl_error($process)];
        }
        curl_close($process);

        /**
         * sample response:
            {}
         */
        return json_decode($return);
    }
}
