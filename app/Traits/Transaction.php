<?php

namespace App\Traits;

use App\User;
use App\Order;
use App\OrderItem;
use App\Cart;
use App\Bundle;
use App\BundleItem;
use App\Product;
use App\Notifications\OrderPaid;

trait Transaction
{
    public static function create_order(User $user)
    {
        $total = 0;
        $latestOrder = Order::orderBy('created_at', 'DESC')->first();
        $ref_num = 'REF' . str_pad(($latestOrder == null ? 1 : $latestOrder->id + 1), 10, random_int(0, 100000), STR_PAD_LEFT);

        //take all from cart
        $cart_items = Cart::where('user_id', $user->id)->get();
        if (count($cart_items) == 0) return ['message' => 'Cart is empty'];

        //create order
        $order = Order::create([
            'user_id' => $user->id,
            'order_ref' => $ref_num
        ]);

        //insert order_item
        foreach ($cart_items as $ci) {

            if ($ci->bundle_id !== null) {
                $bundle = Bundle::find($ci->bundle_id);
                $bundle_item = BundleItem::where('bundle_id', $bundle->id)->get();

                if (count($bundle_item) == 0) return ['message' => 'Bundle is empty. Bundle id:' . $bundle->id];

                foreach ($bundle_item as $bi) {
                    OrderItem::create([
                        'order_id' => $order->id,
                        'product_id' => $bi->product_id,
                        'bundle_id' => $bi->bundle_id,
                        'price' => $bi->price,
                        'quantity' => ($bi->quantity * $ci->quantity),
                        'product_type' => $bi->product->product_type,
                    ]);
                }
            } else {
                $product = Product::find($ci->product_id);

                OrderItem::create([
                    'order_id' => $order->id,
                    'product_id' => $ci->product_id,
                    'bundle_id' => $ci->bundle_id,
                    'price' => $ci->price,
                    'quantity' => $ci->quantity,
                    'product_type' => $product->product_type,
                ]);
            }

            $total += ($ci->price * $ci->quantity);
        }

        $order->total = round($total, 2);
        $order->save();

        //empty cart
        Cart::where('user_id', $user->id)->delete();

        return $order;
    }

    public static function handle_paid(Order $order)
    {
        if ($order == null) {
            return false;
        }

        //deduct stock 
        $order_items = OrderItem::where('order_id', $order->id)->get();
        foreach ($order_items as $item) {
            $product = Product::find($item->product_id);

            if ($item->product_type == Product::TYPE_SUBS) {
                $subs = Subscription::firstOrNew([
                    'user_id' => $order->user->id,
                    'product_id' => $product->id
                ]);
                $subs->expired_at = (Carbon::now()->timestamp > strtotime($subs->expired_at)) ? Carbon::now()->addMonths($item->quantity)->format('Y-m-d H:i:s') : Carbon::parse($subs->expired_at)->addMonths($item->quantity)->format('Y-m-d H:i:s');
                $subs->save();
            } else {
                $product->stock -= $item->quantity;
                $product->save();
            }
        }

        $order->order_status = 'paid';
        $order->save();

        $order->user->notify(new OrderPaid($order));

        return true;
    }

    public static function handle_fail(Order $order)
    {
        if ($order == null) {
            return false;
        }

        $order->order_status = 'fail';
        $order->save();

        return true;
    }

    public static function handle_cancel(Order $order)
    {
        if ($order == null) {
            return false;
        }

        $order->order_status = 'cancel';
        $order->save();

        return true;
    }

    public static function handle_refund(Order $order)
    {
        if ($order == null) {
            return false;
        }

        //deduct stock 
        $order_items = OrderItem::where('order_id', $order->id);
        foreach ($order_items as $item) {
            $product = Product::find($item->product_id);
            $product->stock += $item->quantity;
            $product->save();

            if ($product->product_type == Product::TYPE_SUBS) {
                $subs = Subscription::firstOrNew([
                    'user_id' => $order->user->id,
                    'product_id' => $product->id
                ]);
                $subs->expired_at = Carbon::parse($subs->expired_at)->subMonths($item->quantity)->format('Y-m-d H:i:s');
                $subs->save();
            }
        }

        $order->order_status = 'refund';
        $order->save();

        return true;
    }
}
