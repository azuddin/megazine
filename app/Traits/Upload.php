<?php

namespace App\Traits;

use Intervention\Image\ImageManagerStatic as Image;
use App\ProductImage;
use Carbon\Carbon;
use App\User;
use Illuminate\Support\Facades\Storage;
use App\Author;
use App\BundleImage;


trait Upload
{
    public static function uploadUserAvatar(User $user, $avatar)
    {
        $name = $user->id . '-' . Carbon::now()->timestamp . '.' . $avatar->getClientOriginalExtension();
        $img = Image::make($avatar->getRealPath());
        $img->resize(80, 80, function ($constraint) {
            $constraint->aspectRatio();
        });
        $img->stream();

        Storage::put('public/avatar/user/' . $name, $img);

        return env('APP_URL') . '/avatar/user/' . $name;
    }

    public static function uploadAuthorAvatar(Author $author, $avatar)
    {
        $name = $author->id . '-' . Carbon::now()->timestamp . '.' . $avatar->getClientOriginalExtension();
        $img = Image::make($avatar->getRealPath());
        $img->resize(100, 100, function ($constraint) {
            $constraint->aspectRatio();
        });
        $img->stream();

        Storage::put('public/avatar/author/' . $name, $img);

        return env('APP_URL') . '/avatar/author/' . $name;
    }

    public static function uploadProductImage($product_id, $collection_id, $image)
    {
        $name = microtime(true) . '.' . $image->getClientOriginalExtension();
        $img = Image::make($image->getRealPath());
        $img->stream();

        Storage::put('public/product/id-' . $product_id . '/collection-' . $collection_id . '/' . $name, $img);

        return env('APP_URL') . '/product/id-' . $product_id . '/collection-' . $collection_id . '/' . $name;
    }

    public static function uploadBundleImage($bundle_id, $image)
    {
        $name = microtime(true) . '.' . $image->getClientOriginalExtension();
        $img = Image::make($image->getRealPath());
        $img->stream();

        Storage::put('public/bundle/id-' . $bundle_id . '/' .  $name, $img);

        return env('APP_URL') . '/bundle/id-' . $bundle_id . '/' .  $name;
    }
}
