<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\SoftDeletes;

class Product extends Model
{
    use SoftDeletes;

    const TYPE_NORMAL = 'normal';
    const TYPE_SUBS = 'subs';

    /**
     * array of type. if another type is added, this array and 
     * enum value of column product_type in db also need to updated
     **/
    const TYPE_ARRAY = ['subs', 'normal'];

    protected $fillable = [
        'product_name',
        'product_desc',
        'product_type',
        'price',
        'stock',
        'is_active',
        'is_public',
        'author_id',
        'category_id',
        'hit',
        'likes'
    ];

    public function images()
    {
        return $this->hasManyThrough('App\ProductImage', 'App\ProductCollection');
    }

    public function productCollections()
    {
        return $this->hasMany('App\ProductCollection')->where('is_hidden', 0);
    }

    public function allProductCollections()
    {
        return $this->hasMany('App\ProductCollection');
    }

    public function subscriber()
    {
        return $this->belongsToMany('App\User', 'subscriptions')->as('subscription');
    }

    public function is_subscribe()
    {
        return $this->hasMany('App\Subscription')->where('user_id', Auth::guest() ? 0 : Auth::user()->id)->first();
    }

    public function author()
    {
        return $this->belongsTo('App\Author');
    }

    public function category()
    {
        return $this->belongsTo('App\Category');
    }

    public function collections()
    {
        $collections = ProductImage::select('collection_no')
            ->where('product_id', $this->id)
            ->where('collection_no', '<>', 0)
            ->groupBy('collection_no')
            ->get()->toArray();

        return $collections;
    }
}
