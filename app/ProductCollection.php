<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class ProductCollection extends Model
{
    protected $fillable = ['product_id', 'collection_name', 'is_hidden'];
    protected $guard = ['collection_slug'];

    public function setCollectionNameAttribute($value)
    {
        $this->attributes['collection_name'] = $value;
        $this->attributes['collection_slug'] = str_slug($value);
    }

    public function images()
    {
        return $this->hasMany('App\ProductImage')->orderBy('order', 'ASC')->orderBy('id', 'ASC');
    }

    public function product()
    {
        return $this->belongsTo('App\Product');
    }
}
