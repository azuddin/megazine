<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Order extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'order_ref',
        'user_id',
        'payment_method',
        'payment_ref',
        'payment_url',
        'order_status',
        'total'
    ];

    public function getRouteKeyName()
    {
        if (request()->is('api/admin/*')) return 'id';

        return 'order_ref';
    }

    public function items()
    {
        return $this->hasMany('App\OrderItem');
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
