<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class BundleItem extends Model
{
    use SoftDeletes;

    protected $fillable = ['bundle_id', 'product_id', 'price', 'quantity'];

    public function product()
    {
        return $this->belongsTo('App\Product');
    }
}
