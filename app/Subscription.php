<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\SoftDeletes;

class Subscription extends Model
{
    use SoftDeletes;

    protected $fillable = ['user_id', 'product_id', 'expired_at'];

    public function is_expired()
    {
        return Carbon::now()->timestamp > strtotime($this->expired_at) ? true : false;
    }
}
