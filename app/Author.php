<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Author extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'author_name', 'alias_name', 'email', 'avatar'
    ];

    public function books()
    {
        return $this->hasMany('App\Product');
    }
}
