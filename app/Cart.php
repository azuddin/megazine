<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Cart extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'user_id',
        'product_id',
        'bundle_id',
        'price',
        'quantity',
    ];

    public function bundle()
    {
        return $this->belongsTo('App\Bundle');
    }

    public function product()
    {
        return $this->belongsTo('App\Product');
    }
}
