<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Image;
use Response;
use Illuminate\Database\Eloquent\SoftDeletes;

class ProductImage extends Model
{
    use SoftDeletes;

    protected $fillable = ['product_collection_id', 'url'];

    //if not subs, blur the image
    public function getUrlAttribute($value)
    {
        $product = $this->productCollection->product;
        $is_subscribe = $product->is_subscribe();
        if ($this->collection_no !== 0 && $product->product_type == Product::TYPE_SUBS) {
            switch (true) {
                case $is_subscribe == null:
                    return 'not subscribe';
                    break;
                case $is_subscribe->is_expired():
                    return 'subscription has expired';
                    break;
                default:
                    return 'not show';
            }
        }
        return $value;
    }

    public function productCollection()
    {
        return $this->belongsTo('App\ProductCollection');
    }
}
