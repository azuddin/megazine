<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class OrderItem extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'order_id',
        'product_id',
        'bundle_id',
        'price',
        'quantity',
        'product_type'
    ];

    public function product()
    {
        return $this->hasOne('App\Product');
    }

    public function bundle()
    {
        return $this->hasOne('App\Bundle');
    }
}
