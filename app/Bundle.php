<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Product;
use Illuminate\Database\Eloquent\SoftDeletes;

class Bundle extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'bundle_name',
        'bundle_desc',
        'price',
        'is_active'
    ];

    public function items()
    {
        return $this->hasMany('App\BundleItem');
    }

    public function images()
    {
        return $this->hasMany('App\BundleImage');
    }

    public function attachItem(Product $product)
    {
        return BundleItem::create(['bundle_id' => $this->id, 'product_id' => $product->id, 'price' => $product->price]);
    }

    public function detachItem(Product $product)
    {
        return BundleItem::where(['bundle_id' => $this->id, 'product_id' => $product->id])->delete();
    }
}
