<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['as' => 'api.'], function () {
    Route::get('version', function () {
        $commitHash = trim(exec('git log --pretty="%h" -n1 HEAD'));
        $commitDate = new \DateTime(trim(exec('git log -n1 --pretty=%ci HEAD')));
        $commitDate->setTimezone(new \DateTimeZone('UTC'));
        $response = ['author' => env('APP_NAME') . ' ©', 'version' => sprintf('v%s-git.%s (%s)', app()::VERSION, $commitHash, $commitDate->format('Y-m-d H:i:s'))];
        return response($response, 200);
    })->name('version');

    Route::post('login', 'UserController@login')->name('login');
    Route::post('signup', 'UserController@signup')->name('signup');
    Route::post('resend-verification', 'UserController@resend')->name('resend_verification');

    Route::post('billplz-webhook', 'OrderController@billplzWebhook')->name('billplz_webhook');

    Route::get('bundle', 'BundleController@index')->name('bundle_list');
    Route::get('bundle/{bundle}', 'BundleController@show')->name('bundle_show');

    Route::get('product', 'ProductController@index')->name('product_list');
    Route::get('product/{product_id}', 'ProductController@show')->name('product_show');
    Route::get('product/{product_id}/collection/{collection_id}', 'CollectionController@index')->name('product_collection');

    Route::get('author', 'AuthorController@index')->name('author_list');
    Route::get('author/{author}', 'AuthorController@show')->name('author_show');

    Route::get('category', 'CategoryController@index')->name('category_list');
    Route::get('category/{category}', 'CategoryController@show')->name('category_show');

    Route::group(['middleware' => ['auth:api', 'verified']], function () {
        Route::get('profile', 'UserController@profile')->name('profile');
        Route::post('profile', 'UserController@updateProfile')->name('update_profile');

        Route::get('order', 'OrderController@index')->name('orders');
        Route::get('order/{order}', 'OrderController@show')->name('order');
        Route::post('order', 'OrderController@create')->name('create_order');

        Route::get('cart', 'CartController@index')->name('cart');
        Route::post('cart', 'CartController@create')->name('create_cart');
        Route::delete('cart/{cart}', 'CartController@destroy')->name('delete_cart');
    });
});

Route::group(['middleware' => ['auth:api', 'role:admin'], 'as' => 'admin.', 'prefix' => 'admin/'], function () {

    Route::get('users/non-admin', 'Admin\UserController@nonAdmin', ['middleware' => ['permission:manage_roles']])->name('user.non_admin');

    Route::group(['middleware' => ['permission:view_users']], function () {
        Route::get('users', 'Admin\UserController@index')->name('user.list');
        Route::get('users/{user}', 'Admin\UserController@show')->name('user.details');
    });

    Route::group(['middleware' => ['permission:manage_users']], function () {
        Route::post('users/add', 'Admin\UserController@store')->name('user.add');
        Route::put('users/update/{user}', 'Admin\UserController@update')->name('user.update');
        Route::delete('users/delete/{user}', 'Admin\UserController@destroy')->name('user.delete');
    });

    Route::group(['middleware' => ['permission:view_roles']], function () {
        Route::get('roles', 'Admin\RoleController@index')->name('role.list');
        Route::get('roles/{role}', 'Admin\RoleController@show')->name('role.details');
    });

    Route::group(['middleware' => ['permission:manage_roles']], function () {
        Route::post('roles/add', 'Admin\RoleController@store')->name('role.add');
        Route::put('roles/update/{role}', 'Admin\RoleController@update')->name('role.update');
        Route::delete('roles/delete/{role}', 'Admin\RoleController@destroy')->name('role.delete');

        Route::post('roles/{role}/permissions-attach', 'Admin\RoleController@permission_attach')->name('role.permission_attach');
        Route::post('roles/{role}/permissions-detach', 'Admin\RoleController@permission_detach')->name('role.permission_detach');

        Route::post('roles/{role}/permissions-multiple', 'Admin\RoleController@permission_multiple')->name('role.permission_multiple');
    });

    Route::group(['middleware' => ['permission:view_permissions']], function () {
        Route::get('permissions', 'Admin\PermissionController@index')->name('permission.list');
        // Route::get('permissions/{permission}', 'Admin\PermissionController@show')->name('permission.details');
    });

    Route::group(['middleware' => ['permission:manage_permissions']], function () {
        // Route::post('permissions/add', 'Admin\PermissionController@store')->name('permission.add');
        // Route::put('permissions/update/{permission}', 'Admin\PermissionController@update')->name('permission.update');
        // Route::delete('permissions/delete/{permission}', 'Admin\PermissionController@destroy')->name('permission.delete');
    });

    Route::group(['middleware' => ['permission:view_products']], function () {
        Route::get('products', 'Admin\ProductController@index')->name('product.list');
        Route::get('products/{product}', 'Admin\ProductController@show')->name('product.details');
    });

    Route::group(['middleware' => ['permission:manage_products']], function () {
        Route::post('products/add', 'Admin\ProductController@store')->name('product.add');
        Route::put('products/update/{product}', 'Admin\ProductController@update')->name('product.update');
        Route::delete('products/delete/{product}', 'Admin\ProductController@destroy')->name('product.delete');
        Route::delete('products/delete-image/{productImage}', 'Admin\ProductController@destroyProductImage')->name('product.delete_image');
    });

    Route::group(['middleware' => ['permission:view_products']], function () {
        Route::get('products-collection', 'Admin\ProductCollectionController@index')->name('product_collection.list');
        Route::get('products-collection/{productCollection}', 'Admin\ProductCollectionController@show')->name('product_collection.details');
    });

    Route::group(['middleware' => ['permission:manage_products']], function () {
        Route::post('products-collection/add', 'Admin\ProductCollectionController@store')->name('product_collection.add');
        Route::put('products-collection/update/{productCollection}', 'Admin\ProductCollectionController@update')->name('product_collection.update');
        Route::delete('products-collection/delete/{productCollection}', 'Admin\ProductCollectionController@destroy')->name('product_collection.delete');
    });

    Route::group(['middleware' => ['permission:view_authors']], function () {
        Route::get('authors', 'Admin\AuthorController@index')->name('author.list');
        Route::get('authors/{author}', 'Admin\AuthorController@show')->name('author.details');
    });

    Route::group(['middleware' => ['permission:manage_authors']], function () {
        Route::post('authors/add', 'Admin\AuthorController@store')->name('author.add');
        Route::put('authors/update/{author}', 'Admin\AuthorController@update')->name('author.update');
        Route::delete('authors/delete/{author}', 'Admin\AuthorController@destroy')->name('author.delete');
    });

    Route::group(['middleware' => ['permission:view_categories']], function () {
        Route::get('categories', 'Admin\CategoryController@index')->name('category.list');
        Route::get('categories/{category}', 'Admin\CategoryController@show')->name('category.details');
    });

    Route::group(['middleware' => ['permission:manage_categories']], function () {
        Route::post('categories/add', 'Admin\CategoryController@store')->name('category.add');
        Route::put('categories/update/{category}', 'Admin\CategoryController@update')->name('category.update');
        Route::delete('categories/delete/{category}', 'Admin\CategoryController@destroy')->name('category.delete');
    });

    Route::group(['middleware' => ['permission:view_bundles']], function () {
        Route::get('bundles', 'Admin\BundleController@index')->name('bundle.list');
        Route::get('bundles/{bundle}', 'Admin\BundleController@show')->name('bundle.details');
    });

    Route::group(['middleware' => ['permission:manage_bundles']], function () {
        Route::post('bundles/add', 'Admin\BundleController@store')->name('bundle.add');
        Route::put('bundles/update/{bundle}', 'Admin\BundleController@update')->name('bundle.update');
        Route::delete('bundles/delete/{bundle}', 'Admin\BundleController@destroy')->name('bundle.delete');
        Route::delete('bundles/delete-image/{bundleImage}', 'Admin\BundleController@destroyBundleImage')->name('bundle.delete_image');
        Route::post('bundles/attach-product/{bundle}', 'Admin\BundleController@attach_product')->name('bundle.attach_product');
        Route::post('bundles/detach-product/{bundle}', 'Admin\BundleController@detach_product')->name('bundle.detach_product');
    });

    Route::group(['middleware' => ['permission:view_carts']], function () {
        Route::get('carts', 'Admin\CartController@index')->name('cart.list');
        Route::get('carts/{cart}', 'Admin\CartController@show')->name('cart.details');
    });

    Route::group(['middleware' => ['permission:view_orders']], function () {
        Route::get('orders', 'Admin\OrderController@index')->name('order.list');
        Route::get('orders/{order}', 'Admin\OrderController@show')->name('order.details');
    });

    Route::group(['middleware' => ['permission:view_admins']], function () {
        Route::get('/', 'Admin\AdminController@index')->name('list');
        Route::get('/{admin}', 'Admin\AdminController@show')->name('details');
    });

    Route::group(['middleware' => ['permission:manage_admins']], function () {
        Route::post('/roles-attach', 'Admin\AdminController@roles_attach')->name('roles_attach');
        Route::post('/roles-detach', 'Admin\AdminController@roles_detach')->name('roles_detach');
    });
});
