<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

// Auth::routes(['verify' => true]);

// Route::get('/home', 'HomeController@index')->name('home');

Route::group(['as' => 'user.'], function () {
    Route::get('verify/{activation_token}', 'UserController@verify')->name('verify');
    Route::get('verification-status', function () {
        return view('verification')->with(['message' => session('message')]);
    })->name('verification-status');
});

Route::group(['as' => 'order.'], function () {
    Route::get('order/payment-redirect', 'OrderController@redirect')->name('payment-redirect');
    Route::get('order/payment-status', function () {
        return view('payment')->with(['title' => session('title'), 'status' => session('status'), 'order_ref' => session('order_ref')]);
    })->name('payment-status');
    Route::get('order/{order}', 'OrderController@show')->name('order-detail');
});

Route::group(['as' => 'deploy.'], function () {
    Route::post('deploy/gitlab', 'DeployController@gitlab')->name('gitlab');
});
