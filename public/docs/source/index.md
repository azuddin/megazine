---
title: API Reference

language_tabs:
- bash
- javascript

includes:

search: true

toc_footers:
- <a href='http://github.com/mpociot/documentarian'>Documentation Powered by Documentarian</a>
---
<!-- START_INFO -->
# Info

Welcome to the generated API reference.
[Get Postman Collection](http://megazine.test/docs/collection.json)

<!-- END_INFO -->

#(Admin) Admin
<!-- START_99a3c4dbe1d3c6031b4d1ac151ce6a20 -->
## Get admin list

> Example request:

```bash
curl -X GET -G "http://megazine.test/api/admin" \
    -H "Authorization: Bearer {token}" \
    -H "Accept: application/json"
```

```javascript
const url = new URL("http://megazine.test/api/admin");

    let params = {
            "pageSize": "10",
            "page": "0",
            "sorted": "sorted[0][id]=user_name&amp;sorted[0][desc]=false",
            "filtered": "filtered[0][id]=user_name&amp;filtered[0][value]=azuddin",
        };
    Object.keys(params).forEach(key => url.searchParams.append(key, params[key]));

let headers = {
    "Authorization": "Bearer {token}",
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

> Example response (200):

```json
{
    "data": [
        {
            "id": 1,
            "name": "Ahmad Azuddin",
            "email": "ahmad@azuddin.com",
            "phone_no": null,
            "created_at": "22-03-2019"
        }
    ],
    "meta": {
        "total": 1
    }
}
```

### HTTP Request
`GET api/admin`

#### Query Parameters

Parameter | Status | Description
--------- | ------- | ------- | -----------
    pageSize |  optional  | List page size.
    page |  optional  | Page number.
    sorted |  optional  | Array of sorted item. e.g: sort descending by user_name [{"user_name":"true"}].
    filtered |  optional  | Array of filtered item. e.g: filter record by user_name [{"user_name":"azuddin"}].

<!-- END_99a3c4dbe1d3c6031b4d1ac151ce6a20 -->

<!-- START_78d527762967c4e0148c18955586ca0b -->
## Get admin details

> Example request:

```bash
curl -X GET -G "http://megazine.test/api/admin/{admin}" \
    -H "Authorization: Bearer {token}" \
    -H "Accept: application/json"
```

```javascript
const url = new URL("http://megazine.test/api/admin/{admin}");

    let params = {
            "user_id": "1",
        };
    Object.keys(params).forEach(key => url.searchParams.append(key, params[key]));

let headers = {
    "Authorization": "Bearer {token}",
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

> Example response (200):

```json
{
    "data": {
        "id": 1,
        "name": "Ahmad Azuddin",
        "email": "ahmad@azuddin.com",
        "phone_no": null,
        "roles": [
            {
                "id": 1,
                "name": "superAdmin"
            },
            {
                "id": 2,
                "name": "admin"
            }
        ]
    }
}
```

### HTTP Request
`GET api/admin/{admin}`

#### Query Parameters

Parameter | Status | Description
--------- | ------- | ------- | -----------
    user_id |  required  | Id of the admin.

<!-- END_78d527762967c4e0148c18955586ca0b -->

<!-- START_3c57a106fcb6ea48fa3855c06e6c6e09 -->
## Attach role to an admin

> Example request:

```bash
curl -X POST "http://megazine.test/api/admin/roles-attach" \
    -H "Authorization: Bearer {token}" \
    -H "Accept: application/json" \
    -H "Content-Type: application/json" \
    -d '{"role_id":1,"user_id":1}'

```

```javascript
const url = new URL("http://megazine.test/api/admin/roles-attach");

let headers = {
    "Authorization": "Bearer {token}",
    "Accept": "application/json",
    "Content-Type": "application/json",
}

let body = {
    "role_id": 1,
    "user_id": 1
}

fetch(url, {
    method: "POST",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```

> Example response (200):

```json
{
    "message": "Role has been added"
}
```

### HTTP Request
`POST api/admin/roles-attach`

#### Body Parameters

Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    role_id | integer |  required  | Id of the role.
    user_id | integer |  required  | Id of the user.

<!-- END_3c57a106fcb6ea48fa3855c06e6c6e09 -->

<!-- START_63bcbbc6112e7a562245f9892eacf34e -->
## Detach role from an admin

> Example request:

```bash
curl -X POST "http://megazine.test/api/admin/roles-detach" \
    -H "Authorization: Bearer {token}" \
    -H "Accept: application/json" \
    -H "Content-Type: application/json" \
    -d '{"role_id":1,"user_id":1}'

```

```javascript
const url = new URL("http://megazine.test/api/admin/roles-detach");

let headers = {
    "Authorization": "Bearer {token}",
    "Accept": "application/json",
    "Content-Type": "application/json",
}

let body = {
    "role_id": 1,
    "user_id": 1
}

fetch(url, {
    method: "POST",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```

> Example response (200):

```json
{
    "message": "Role has been removed"
}
```

### HTTP Request
`POST api/admin/roles-detach`

#### Body Parameters

Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    role_id | integer |  required  | Id of the role.
    user_id | integer |  required  | Id of the user.

<!-- END_63bcbbc6112e7a562245f9892eacf34e -->

#(Admin) Author
<!-- START_fa8a533c76ea42b5dd6cf579397a3929 -->
## Get author list

> Example request:

```bash
curl -X GET -G "http://megazine.test/api/admin/authors" \
    -H "Authorization: Bearer {token}" \
    -H "Accept: application/json"
```

```javascript
const url = new URL("http://megazine.test/api/admin/authors");

    let params = {
            "pageSize": "10",
            "page": "0",
            "sorted": "sorted[0][id]=author_name&amp;sorted[0][desc]=false",
            "filtered": "filtered[0][id]=author_name&amp;filtered[0][value]=Joe Jambul",
        };
    Object.keys(params).forEach(key => url.searchParams.append(key, params[key]));

let headers = {
    "Authorization": "Bearer {token}",
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

> Example response (200):

```json
{
    "data": [
        {
            "id": 1,
            "author_name": "CESB",
            "alias_name": null,
            "email": null,
            "avatar": null
        },
        {
            "id": 2,
            "author_name": "epie",
            "alias_name": null,
            "email": null,
            "avatar": null
        },
        {
            "id": 3,
            "author_name": "Halia",
            "alias_name": null,
            "email": null,
            "avatar": null
        }
    ],
    "meta": {
        "total": 3
    }
}
```

### HTTP Request
`GET api/admin/authors`

#### Query Parameters

Parameter | Status | Description
--------- | ------- | ------- | -----------
    pageSize |  optional  | List page size.
    page |  optional  | Page number.
    sorted |  optional  | Array of sorted item. e.g: sort descending by author_name [{"author_name":"true"}].
    filtered |  optional  | Array of filtered item. e.g: filter record by author_name [{"author_name":"Joe Jambul"}].

<!-- END_fa8a533c76ea42b5dd6cf579397a3929 -->

<!-- START_98552da8b52e7d4f975da682b07d4cbd -->
## Get author details

> Example request:

```bash
curl -X GET -G "http://megazine.test/api/admin/authors/{author}" \
    -H "Authorization: Bearer {token}" \
    -H "Accept: application/json"
```

```javascript
const url = new URL("http://megazine.test/api/admin/authors/{author}");

    let params = {
            "author_id": "1",
        };
    Object.keys(params).forEach(key => url.searchParams.append(key, params[key]));

let headers = {
    "Authorization": "Bearer {token}",
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

> Example response (200):

```json
{
    "data": {
        "id": 1,
        "author_name": "CESB",
        "alias_name": null,
        "email": null,
        "avatar": null,
        "created_at": "22-03-19"
    }
}
```

### HTTP Request
`GET api/admin/authors/{author}`

#### Query Parameters

Parameter | Status | Description
--------- | ------- | ------- | -----------
    author_id |  required  | Id of the author.

<!-- END_98552da8b52e7d4f975da682b07d4cbd -->

<!-- START_34c3ed71ded2e6534624b5b1ad4a5ce3 -->
## Add new author

> Example request:

```bash
curl -X POST "http://megazine.test/api/admin/authors/add" \
    -H "Authorization: Bearer {token}" \
    -H "Accept: application/json" \
    -H "Content-Type: application/json" \
    -d '{"author_name":"Joe Jambul","alias_name":"Joe Jambul","email":"joe.jambul@azuddin.com","avatar":"NULQvzfiZ0JVKPJW"}'

```

```javascript
const url = new URL("http://megazine.test/api/admin/authors/add");

let headers = {
    "Authorization": "Bearer {token}",
    "Accept": "application/json",
    "Content-Type": "application/json",
}

let body = {
    "author_name": "Joe Jambul",
    "alias_name": "Joe Jambul",
    "email": "joe.jambul@azuddin.com",
    "avatar": "NULQvzfiZ0JVKPJW"
}

fetch(url, {
    method: "POST",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```

> Example response (200):

```json
{
    "message": "Author has been created"
}
```

### HTTP Request
`POST api/admin/authors/add`

#### Body Parameters

Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    author_name | string |  required  | Name of the author.
    alias_name | string |  optional  | Alias name of the author.
    email | string |  optional  | Email of the author.
    avatar | image |  optional  | Image of the author.

<!-- END_34c3ed71ded2e6534624b5b1ad4a5ce3 -->

<!-- START_6a456daf18304b8da6bc62148c35cc72 -->
## Update author detail

> Example request:

```bash
curl -X PUT "http://megazine.test/api/admin/authors/update/{author}" \
    -H "Authorization: Bearer {token}" \
    -H "Accept: application/json" \
    -H "Content-Type: application/json" \
    -d '{"author_name":"Stan Lee","alias_name":"Stan Lee","email":"stan.lee@azuddin.com","avatar":"7wmMUuep8IMlGaPR"}'

```

```javascript
const url = new URL("http://megazine.test/api/admin/authors/update/{author}");

    let params = {
            "author_id": "1",
        };
    Object.keys(params).forEach(key => url.searchParams.append(key, params[key]));

let headers = {
    "Authorization": "Bearer {token}",
    "Accept": "application/json",
    "Content-Type": "application/json",
}

let body = {
    "author_name": "Stan Lee",
    "alias_name": "Stan Lee",
    "email": "stan.lee@azuddin.com",
    "avatar": "7wmMUuep8IMlGaPR"
}

fetch(url, {
    method: "PUT",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```

> Example response (200):

```json
{
    "message": "Author has been updated"
}
```

### HTTP Request
`PUT api/admin/authors/update/{author}`

#### Body Parameters

Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    author_name | string |  required  | Name of the author.
    alias_name | string |  optional  | Alias name of the author.
    email | string |  optional  | Email of the author.
    avatar | image |  optional  | Avatar of the author.
#### Query Parameters

Parameter | Status | Description
--------- | ------- | ------- | -----------
    author_id |  required  | Id of author.

<!-- END_6a456daf18304b8da6bc62148c35cc72 -->

<!-- START_ab7e464d1d648bc350ab1605003943af -->
## Remove author

> Example request:

```bash
curl -X DELETE "http://megazine.test/api/admin/authors/delete/{author}" \
    -H "Authorization: Bearer {token}" \
    -H "Accept: application/json"
```

```javascript
const url = new URL("http://megazine.test/api/admin/authors/delete/{author}");

    let params = {
            "author_id": "1",
        };
    Object.keys(params).forEach(key => url.searchParams.append(key, params[key]));

let headers = {
    "Authorization": "Bearer {token}",
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "DELETE",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

> Example response (200):

```json
{
    "message": "Author has been deleted"
}
```

### HTTP Request
`DELETE api/admin/authors/delete/{author}`

#### Query Parameters

Parameter | Status | Description
--------- | ------- | ------- | -----------
    author_id |  required  | Id of author.

<!-- END_ab7e464d1d648bc350ab1605003943af -->

#(Admin) Bundle
<!-- START_eec059b73f5bd477a45019e77a11a6ef -->
## Get bundle list

> Example request:

```bash
curl -X GET -G "http://megazine.test/api/admin/bundles" \
    -H "Authorization: Bearer {token}" \
    -H "Accept: application/json"
```

```javascript
const url = new URL("http://megazine.test/api/admin/bundles");

    let params = {
            "pageSize": "10",
            "page": "0",
            "sorted": "sorted[0][id]=bundle_name&amp;sorted[0][desc]=false",
            "filtered": "filtered[0][id]=bundle_name&amp;filtered[0][value]=Bundle Raya",
        };
    Object.keys(params).forEach(key => url.searchParams.append(key, params[key]));

let headers = {
    "Authorization": "Bearer {token}",
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

> Example response (200):

```json
{
    "data": [
        {
            "id": 1,
            "bundle_name": "Molestiae corporis qui dolorem id.",
            "bundle_desc": "Voluptates quis ut rerum officiis. Molestias vitae qui deleniti porro nisi.\n\nNulla impedit suscipit harum et. Blanditiis corrupti modi qui dolores consequatur. Quos dolorem sequi architecto ut mollitia. Laborum voluptatum perspiciatis sit quibusdam eaque aut praesentium.",
            "images": [],
            "bundle_items_count": 3,
            "price": "21.14"
        }
    ],
    "meta": {
        "total": 1
    }
}
```

### HTTP Request
`GET api/admin/bundles`

#### Query Parameters

Parameter | Status | Description
--------- | ------- | ------- | -----------
    pageSize |  optional  | List page size.
    page |  optional  | Page number.
    sorted |  optional  | Array of sorted item. e.g: sort descending by bundle_name [{"bundle_name":"true"}].
    filtered |  optional  | Array of filtered item. e.g: filter record by bundle_name [{"bundle_name":"Bundle Raya"}].

<!-- END_eec059b73f5bd477a45019e77a11a6ef -->

<!-- START_cbc3d47d82eac7e59714a4c1b9ba3322 -->
## Get bundle details

> Example request:

```bash
curl -X GET -G "http://megazine.test/api/admin/bundles/{bundle}" \
    -H "Authorization: Bearer {token}" \
    -H "Accept: application/json"
```

```javascript
const url = new URL("http://megazine.test/api/admin/bundles/{bundle}");

    let params = {
            "bundle_id": "1",
        };
    Object.keys(params).forEach(key => url.searchParams.append(key, params[key]));

let headers = {
    "Authorization": "Bearer {token}",
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

> Example response (200):

```json
{
    "data": {
        "id": 1,
        "bundle_name": "Molestiae corporis qui dolorem id.",
        "bundle_desc": "Voluptates quis ut rerum officiis. Molestias vitae qui deleniti porro nisi.\n\nNulla impedit suscipit harum et. Blanditiis corrupti modi qui dolores consequatur. Quos dolorem sequi architecto ut mollitia. Laborum voluptatum perspiciatis sit quibusdam eaque aut praesentium.",
        "images": [],
        "items": [
            {
                "id": 1,
                "product": {
                    "id": 1,
                    "product_name": "Gila-Gila",
                    "product_desc": null,
                    "product_type": "normal",
                    "author_id": 6,
                    "category_id": 2,
                    "price": "0.00",
                    "stock": 1,
                    "hit": null,
                    "likes": "0",
                    "is_public": 1,
                    "product_images": [
                        {
                            "url": "gg\/gg001\/gg00101.jpg",
                            "collection_no": 0,
                            "order": 1
                        }
                    ]
                },
                "price": "0.00",
                "quantity": 1
            }
        ],
        "price": "21.14"
    }
}
```

### HTTP Request
`GET api/admin/bundles/{bundle}`

#### Query Parameters

Parameter | Status | Description
--------- | ------- | ------- | -----------
    bundle_id |  required  | Id of the bundle.

<!-- END_cbc3d47d82eac7e59714a4c1b9ba3322 -->

<!-- START_3525a4676327abdbe36c224d096a3bfb -->
## Add new bundle

> Example request:

```bash
curl -X POST "http://megazine.test/api/admin/bundles/add" \
    -H "Authorization: Bearer {token}" \
    -H "Accept: application/json" \
    -H "Content-Type: application/json" \
    -d '{"bundle_name":"bundle A","bundle_desc":"lorem ipsum","price":10.23,"bundle_images":[]}'

```

```javascript
const url = new URL("http://megazine.test/api/admin/bundles/add");

let headers = {
    "Authorization": "Bearer {token}",
    "Accept": "application/json",
    "Content-Type": "application/json",
}

let body = {
    "bundle_name": "bundle A",
    "bundle_desc": "lorem ipsum",
    "price": 10.23,
    "bundle_images": []
}

fetch(url, {
    method: "POST",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```

> Example response (200):

```json
{
    "message": "Bundle has been created"
}
```

### HTTP Request
`POST api/admin/bundles/add`

#### Body Parameters

Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    bundle_name | string |  required  | Name of the bundle.
    bundle_desc | string |  required  | Description of the bundle.
    price | float |  required  | Price of the bundle.
    bundle_images | array |  optional  | Array of bundle image.

<!-- END_3525a4676327abdbe36c224d096a3bfb -->

<!-- START_4c41787a222cfdd40b685ee670b5988e -->
## Update bundle detail

> Example request:

```bash
curl -X PUT "http://megazine.test/api/admin/bundles/update/{bundle}" \
    -H "Authorization: Bearer {token}" \
    -H "Accept: application/json" \
    -H "Content-Type: application/json" \
    -d '{"bundle_name":"bundle AB","bundle_desc":"sit amet dolor","price":13.23,"bundle_images":[]}'

```

```javascript
const url = new URL("http://megazine.test/api/admin/bundles/update/{bundle}");

    let params = {
            "bundle_id": "1",
        };
    Object.keys(params).forEach(key => url.searchParams.append(key, params[key]));

let headers = {
    "Authorization": "Bearer {token}",
    "Accept": "application/json",
    "Content-Type": "application/json",
}

let body = {
    "bundle_name": "bundle AB",
    "bundle_desc": "sit amet dolor",
    "price": 13.23,
    "bundle_images": []
}

fetch(url, {
    method: "PUT",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```

> Example response (200):

```json
{
    "message": "Bundle has been updated"
}
```

### HTTP Request
`PUT api/admin/bundles/update/{bundle}`

#### Body Parameters

Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    bundle_name | string |  required  | Name of the bundle.
    bundle_desc | string |  required  | Description of the bundle.
    price | float |  required  | Price of the bundle.
    bundle_images | array |  optional  | Array of bundle image.
#### Query Parameters

Parameter | Status | Description
--------- | ------- | ------- | -----------
    bundle_id |  required  | Id of bundle.

<!-- END_4c41787a222cfdd40b685ee670b5988e -->

<!-- START_4907a7c816847a7490294a37815d4dcd -->
## Remove bundle

> Example request:

```bash
curl -X DELETE "http://megazine.test/api/admin/bundles/delete/{bundle}" \
    -H "Authorization: Bearer {token}" \
    -H "Accept: application/json"
```

```javascript
const url = new URL("http://megazine.test/api/admin/bundles/delete/{bundle}");

    let params = {
            "bundle_id": "1",
        };
    Object.keys(params).forEach(key => url.searchParams.append(key, params[key]));

let headers = {
    "Authorization": "Bearer {token}",
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "DELETE",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

> Example response (200):

```json
{
    "message": "Bundle has been deleted"
}
```

### HTTP Request
`DELETE api/admin/bundles/delete/{bundle}`

#### Query Parameters

Parameter | Status | Description
--------- | ------- | ------- | -----------
    bundle_id |  required  | Id of bundle.

<!-- END_4907a7c816847a7490294a37815d4dcd -->

<!-- START_3ba47d173e1ad1501c2930ea603ea085 -->
## Remove bundle image

> Example request:

```bash
curl -X DELETE "http://megazine.test/api/admin/bundles/delete-image/{bundleImage}" \
    -H "Authorization: Bearer {token}" \
    -H "Accept: application/json"
```

```javascript
const url = new URL("http://megazine.test/api/admin/bundles/delete-image/{bundleImage}");

    let params = {
            "bundle_image_id": "1",
        };
    Object.keys(params).forEach(key => url.searchParams.append(key, params[key]));

let headers = {
    "Authorization": "Bearer {token}",
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "DELETE",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

> Example response (200):

```json
{
    "message": "Bundle's image has been deleted"
}
```

### HTTP Request
`DELETE api/admin/bundles/delete-image/{bundleImage}`

#### Query Parameters

Parameter | Status | Description
--------- | ------- | ------- | -----------
    bundle_image_id |  required  | Id of bundle's image.

<!-- END_3ba47d173e1ad1501c2930ea603ea085 -->

<!-- START_fe39c505e6fe250c13006b813c9c410b -->
## Attach a product

> Example request:

```bash
curl -X POST "http://megazine.test/api/admin/bundles/attach-product/{bundle}" \
    -H "Authorization: Bearer {token}" \
    -H "Accept: application/json" \
    -H "Content-Type: application/json" \
    -d '{"product_id":1}'

```

```javascript
const url = new URL("http://megazine.test/api/admin/bundles/attach-product/{bundle}");

    let params = {
            "bundle_id": "1",
        };
    Object.keys(params).forEach(key => url.searchParams.append(key, params[key]));

let headers = {
    "Authorization": "Bearer {token}",
    "Accept": "application/json",
    "Content-Type": "application/json",
}

let body = {
    "product_id": 1
}

fetch(url, {
    method: "POST",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```

> Example response (200):

```json
{
    "message": "Product has been attached"
}
```

### HTTP Request
`POST api/admin/bundles/attach-product/{bundle}`

#### Body Parameters

Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    product_id | integer |  required  | Id of the product.
#### Query Parameters

Parameter | Status | Description
--------- | ------- | ------- | -----------
    bundle_id |  required  | Id of the bundle.

<!-- END_fe39c505e6fe250c13006b813c9c410b -->

<!-- START_14a494398122ea444ba9327d92ed5164 -->
## Detach a product

> Example request:

```bash
curl -X POST "http://megazine.test/api/admin/bundles/detach-product/{bundle}" \
    -H "Authorization: Bearer {token}" \
    -H "Accept: application/json" \
    -H "Content-Type: application/json" \
    -d '{"product_id":1}'

```

```javascript
const url = new URL("http://megazine.test/api/admin/bundles/detach-product/{bundle}");

    let params = {
            "bundle_id": "1",
        };
    Object.keys(params).forEach(key => url.searchParams.append(key, params[key]));

let headers = {
    "Authorization": "Bearer {token}",
    "Accept": "application/json",
    "Content-Type": "application/json",
}

let body = {
    "product_id": 1
}

fetch(url, {
    method: "POST",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```

> Example response (200):

```json
{
    "message": "Product has been detached"
}
```

### HTTP Request
`POST api/admin/bundles/detach-product/{bundle}`

#### Body Parameters

Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    product_id | integer |  required  | Id of the product.
#### Query Parameters

Parameter | Status | Description
--------- | ------- | ------- | -----------
    bundle_id |  required  | Id of the bundle.

<!-- END_14a494398122ea444ba9327d92ed5164 -->

#(Admin) Cart
<!-- START_6300dfaa9a12b51247d90b5db089bd17 -->
## Get cart list

> Example request:

```bash
curl -X GET -G "http://megazine.test/api/admin/carts" \
    -H "Authorization: Bearer {token}" \
    -H "Accept: application/json"
```

```javascript
const url = new URL("http://megazine.test/api/admin/carts");

    let params = {
            "pageSize": "10",
            "page": "0",
            "sorted": "sorted[0][id]=cart_id&amp;sorted[0][desc]=false",
            "filtered": "filtered[0][id]=cart_id&amp;filtered[0][value]=Aut dignissimos",
        };
    Object.keys(params).forEach(key => url.searchParams.append(key, params[key]));

let headers = {
    "Authorization": "Bearer {token}",
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

> Example response (200):

```json
{
    "data": [
        {
            "id": 1,
            "user_id": 2,
            "product_id": null,
            "bundle_id": 2,
            "price": "79.16",
            "quantity": 1,
            "created_at": "23-03-2019"
        }
    ],
    "meta": {
        "total": 1
    }
}
```

### HTTP Request
`GET api/admin/carts`

#### Query Parameters

Parameter | Status | Description
--------- | ------- | ------- | -----------
    pageSize |  optional  | List page size.
    page |  optional  | Page number.
    sorted |  optional  | Array of sorted item. e.g: sort descending by cart_id [{"cart_id":"true"}].
    filtered |  optional  | Array of filtered item. e.g: filter record by cart_id [{"cart_id":"Aut dignissimos"}].

<!-- END_6300dfaa9a12b51247d90b5db089bd17 -->

<!-- START_6d2369a2feaa3734d8b5d700fe03229b -->
## Get cart details

> Example request:

```bash
curl -X GET -G "http://megazine.test/api/admin/carts/{cart}" \
    -H "Authorization: Bearer {token}" \
    -H "Accept: application/json"
```

```javascript
const url = new URL("http://megazine.test/api/admin/carts/{cart}");

    let params = {
            "cart_id": "1",
        };
    Object.keys(params).forEach(key => url.searchParams.append(key, params[key]));

let headers = {
    "Authorization": "Bearer {token}",
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

> Example response (200):

```json
{
    "data": {
        "id": 1,
        "user_id": 2,
        "product_id": null,
        "bundle_id": 2,
        "price": "79.16",
        "quantity": 1,
        "created_at": "23-03-2019"
    }
}
```

### HTTP Request
`GET api/admin/carts/{cart}`

#### Query Parameters

Parameter | Status | Description
--------- | ------- | ------- | -----------
    cart_id |  required  | Id of the cart.

<!-- END_6d2369a2feaa3734d8b5d700fe03229b -->

#(Admin) Category
<!-- START_aba6e7326a8a1d276c1adacebe2a4093 -->
## Get category list

> Example request:

```bash
curl -X GET -G "http://megazine.test/api/admin/categories" \
    -H "Authorization: Bearer {token}" \
    -H "Accept: application/json"
```

```javascript
const url = new URL("http://megazine.test/api/admin/categories");

    let params = {
            "pageSize": "10",
            "page": "0",
            "sorted": "sorted[0][id]=category_name&amp;sorted[0][desc]=false",
            "filtered": "filtered[0][id]=category_name&amp;filtered[0][value]=Magazine",
        };
    Object.keys(params).forEach(key => url.searchParams.append(key, params[key]));

let headers = {
    "Authorization": "Bearer {token}",
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

> Example response (200):

```json
{
    "data": [
        {
            "id": 1,
            "category_name": "Komik"
        },
        {
            "id": 2,
            "category_name": "Majalah"
        },
        {
            "id": 3,
            "category_name": "Koleksi"
        }
    ],
    "meta": {
        "total": 5
    }
}
```

### HTTP Request
`GET api/admin/categories`

#### Query Parameters

Parameter | Status | Description
--------- | ------- | ------- | -----------
    pageSize |  optional  | List page size.
    page |  optional  | Page number.
    sorted |  optional  | Array of sorted item. e.g: sort descending by category_name [{"category_name":"true"}].
    filtered |  optional  | Array of filtered item. e.g: filter record by category_name [{"category_name":"Magazine"}].

<!-- END_aba6e7326a8a1d276c1adacebe2a4093 -->

<!-- START_d3970d657b32f9b199272b75c7611004 -->
## Get category details

> Example request:

```bash
curl -X GET -G "http://megazine.test/api/admin/categories/{category}" \
    -H "Authorization: Bearer {token}" \
    -H "Accept: application/json"
```

```javascript
const url = new URL("http://megazine.test/api/admin/categories/{category}");

    let params = {
            "category_id": "1",
        };
    Object.keys(params).forEach(key => url.searchParams.append(key, params[key]));

let headers = {
    "Authorization": "Bearer {token}",
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

> Example response (200):

```json
{
    "data": {
        "category_id": 1,
        "category_name": "Komik"
    }
}
```

### HTTP Request
`GET api/admin/categories/{category}`

#### Query Parameters

Parameter | Status | Description
--------- | ------- | ------- | -----------
    category_id |  required  | Id of the category.

<!-- END_d3970d657b32f9b199272b75c7611004 -->

<!-- START_c69db513fb08edd61c452f04e67d8005 -->
## Add new category

> Example request:

```bash
curl -X POST "http://megazine.test/api/admin/categories/add" \
    -H "Authorization: Bearer {token}" \
    -H "Accept: application/json" \
    -H "Content-Type: application/json" \
    -d '{"category_name":"Magazine"}'

```

```javascript
const url = new URL("http://megazine.test/api/admin/categories/add");

let headers = {
    "Authorization": "Bearer {token}",
    "Accept": "application/json",
    "Content-Type": "application/json",
}

let body = {
    "category_name": "Magazine"
}

fetch(url, {
    method: "POST",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```

> Example response (200):

```json
{
    "message": "Category has been created"
}
```

### HTTP Request
`POST api/admin/categories/add`

#### Body Parameters

Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    category_name | string |  required  | Name of the category.

<!-- END_c69db513fb08edd61c452f04e67d8005 -->

<!-- START_c6245dc6b77947344472f35827934e68 -->
## Update category detail

> Example request:

```bash
curl -X PUT "http://megazine.test/api/admin/categories/update/{category}" \
    -H "Authorization: Bearer {token}" \
    -H "Accept: application/json" \
    -H "Content-Type: application/json" \
    -d '{"category_name":"Comic"}'

```

```javascript
const url = new URL("http://megazine.test/api/admin/categories/update/{category}");

    let params = {
            "category_id": "1",
        };
    Object.keys(params).forEach(key => url.searchParams.append(key, params[key]));

let headers = {
    "Authorization": "Bearer {token}",
    "Accept": "application/json",
    "Content-Type": "application/json",
}

let body = {
    "category_name": "Comic"
}

fetch(url, {
    method: "PUT",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```

> Example response (200):

```json
{
    "message": "Category has been updated"
}
```

### HTTP Request
`PUT api/admin/categories/update/{category}`

#### Body Parameters

Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    category_name | string |  required  | Name of the category.
#### Query Parameters

Parameter | Status | Description
--------- | ------- | ------- | -----------
    category_id |  required  | Id of category.

<!-- END_c6245dc6b77947344472f35827934e68 -->

<!-- START_25ce32419d0a59e7f963c6d3aa0d55fc -->
## Remove category

> Example request:

```bash
curl -X DELETE "http://megazine.test/api/admin/categories/delete/{category}" \
    -H "Authorization: Bearer {token}" \
    -H "Accept: application/json"
```

```javascript
const url = new URL("http://megazine.test/api/admin/categories/delete/{category}");

    let params = {
            "category_id": "1",
        };
    Object.keys(params).forEach(key => url.searchParams.append(key, params[key]));

let headers = {
    "Authorization": "Bearer {token}",
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "DELETE",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

> Example response (200):

```json
{
    "message": "Category has been deleted"
}
```

### HTTP Request
`DELETE api/admin/categories/delete/{category}`

#### Query Parameters

Parameter | Status | Description
--------- | ------- | ------- | -----------
    category_id |  required  | Id of category.

<!-- END_25ce32419d0a59e7f963c6d3aa0d55fc -->

#(Admin) Order
<!-- START_40f5a4504d42fc88caef48a9b453e0aa -->
## Get order list

> Example request:

```bash
curl -X GET -G "http://megazine.test/api/admin/orders" \
    -H "Authorization: Bearer {token}" \
    -H "Accept: application/json"
```

```javascript
const url = new URL("http://megazine.test/api/admin/orders");

    let params = {
            "pageSize": "10",
            "page": "0",
            "sorted": "sorted[0][id]=price&amp;sorted[0][desc]=false",
            "filtered": "filtered[0][id]=price&amp;filtered[0][value]=10.23",
        };
    Object.keys(params).forEach(key => url.searchParams.append(key, params[key]));

let headers = {
    "Authorization": "Bearer {token}",
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

> Example response (200):

```json
{
    "data": [
        {
            "id": 1,
            "user_id": 2,
            "order_ref": "REF5722257221",
            "order_status": "pending",
            "total": "0.00",
            "payment_method": null,
            "order_items_count": 0
        }
    ],
    "meta": {
        "total": 1
    }
}
```

### HTTP Request
`GET api/admin/orders`

#### Query Parameters

Parameter | Status | Description
--------- | ------- | ------- | -----------
    pageSize |  optional  | List page size.
    page |  optional  | Page number.
    sorted |  optional  | Array of sorted item. e.g: sort descending by price [{"price":"true"}].
    filtered |  optional  | Array of filtered item. e.g: filter record by price [{"price":"10.23"}].

<!-- END_40f5a4504d42fc88caef48a9b453e0aa -->

<!-- START_876f4dae88a2290b943b543b6610e96a -->
## Get order details

> Example request:

```bash
curl -X GET -G "http://megazine.test/api/admin/orders/{order}" \
    -H "Authorization: Bearer {token}" \
    -H "Accept: application/json"
```

```javascript
const url = new URL("http://megazine.test/api/admin/orders/{order}");

    let params = {
            "order_id": "1",
        };
    Object.keys(params).forEach(key => url.searchParams.append(key, params[key]));

let headers = {
    "Authorization": "Bearer {token}",
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

> Example response (200):

```json
{}
```

### HTTP Request
`GET api/admin/orders/{order}`

#### Query Parameters

Parameter | Status | Description
--------- | ------- | ------- | -----------
    order_id |  required  | Id of the order.

<!-- END_876f4dae88a2290b943b543b6610e96a -->

#(Admin) Permission
<!-- START_97490970821a6ac10cf75498147304ba -->
## Get permission list

> Example request:

```bash
curl -X GET -G "http://megazine.test/api/admin/permissions" \
    -H "Authorization: Bearer {token}" \
    -H "Accept: application/json"
```

```javascript
const url = new URL("http://megazine.test/api/admin/permissions");

    let params = {
            "pageSize": "10",
            "page": "0",
            "sorted": "sorted[0][id]=name&amp;sorted[0][desc]=false",
            "filtered": "filtered[0][id]=name&amp;filtered[0][value]=edit product",
        };
    Object.keys(params).forEach(key => url.searchParams.append(key, params[key]));

let headers = {
    "Authorization": "Bearer {token}",
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

> Example response (200):

```json
{
    "data": [
        {
            "id": 1,
            "name": "view_admins"
        },
        {
            "id": 2,
            "name": "manage_admins"
        }
    ],
    "meta": {
        "total": 2
    }
}
```

### HTTP Request
`GET api/admin/permissions`

#### Query Parameters

Parameter | Status | Description
--------- | ------- | ------- | -----------
    pageSize |  optional  | List page size.
    page |  optional  | Page number.
    sorted |  optional  | Array of sorted item. e.g: sort descending by name [{"name":"true"}].
    filtered |  optional  | Array of filtered item. e.g: filter record by name [{"name":"edit product"}].

<!-- END_97490970821a6ac10cf75498147304ba -->

#(Admin) Product
<!-- START_85bd9da640ee7e09ba5d4ef8b434628b -->
## Get product list

> Example request:

```bash
curl -X GET -G "http://megazine.test/api/admin/products" \
    -H "Authorization: Bearer {token}" \
    -H "Accept: application/json"
```

```javascript
const url = new URL("http://megazine.test/api/admin/products");

    let params = {
            "pageSize": "10",
            "page": "0",
            "sorted": "sorted[0][id]=product_name&amp;sorted[0][desc]=false",
            "filtered": "filtered[0][id]=product_name&amp;filtered[0][value]=Product A",
        };
    Object.keys(params).forEach(key => url.searchParams.append(key, params[key]));

let headers = {
    "Authorization": "Bearer {token}",
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

> Example response (200):

```json
{
    "data": [
        {
            "id": 1,
            "product_name": "Gila-Gila",
            "product_desc": null,
            "product_type": "normal",
            "author_id": 6,
            "category_id": 2,
            "price": "0.00",
            "stock": 1,
            "hit": null,
            "likes": "0",
            "is_public": 1,
            "product_images": [
                {
                    "url": "gg\/gg001\/gg00101.jpg",
                    "collection_no": 0,
                    "order": 1
                }
            ]
        }
    ],
    "meta": {
        "total": 1
    }
}
```

### HTTP Request
`GET api/admin/products`

#### Query Parameters

Parameter | Status | Description
--------- | ------- | ------- | -----------
    pageSize |  optional  | List page size.
    page |  optional  | Page number.
    sorted |  optional  | Array of sorted item. e.g: sort descending by product_name [{"product_name":"true"}].
    filtered |  optional  | Array of filtered item. e.g: filter record by product_name [{"product_name":"Product A"}].

<!-- END_85bd9da640ee7e09ba5d4ef8b434628b -->

<!-- START_9332e51c1fdec8d85b3840fcb9237a92 -->
## Get product details

> Example request:

```bash
curl -X GET -G "http://megazine.test/api/admin/products/{product}" \
    -H "Authorization: Bearer {token}" \
    -H "Accept: application/json"
```

```javascript
const url = new URL("http://megazine.test/api/admin/products/{product}");

    let params = {
            "product_id": "1",
        };
    Object.keys(params).forEach(key => url.searchParams.append(key, params[key]));

let headers = {
    "Authorization": "Bearer {token}",
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

> Example response (200):

```json
{
    "data": {
        "id": 1,
        "product_name": "Gila-Gila",
        "product_desc": null,
        "product_type": "normal",
        "author_id": 6,
        "category_id": 2,
        "price": "0.00",
        "stock": 1,
        "hit": null,
        "likes": "0",
        "product_images": [
            {
                "url": "gg\/gg001\/gg00101.jpg",
                "collection_no": 0,
                "order": 1
            }
        ],
        "collections": [
            {
                "collection_no": 1
            },
            {
                "collection_no": 788
            },
            {
                "collection_no": 789
            },
            {
                "collection_no": 811
            },
            {
                "collection_no": 812
            }
        ]
    }
}
```

### HTTP Request
`GET api/admin/products/{product}`

#### Query Parameters

Parameter | Status | Description
--------- | ------- | ------- | -----------
    product_id |  required  | Id of the product.

<!-- END_9332e51c1fdec8d85b3840fcb9237a92 -->

<!-- START_169c6c5227ff791545658649c38f1351 -->
## Add new product

> Example request:

```bash
curl -X POST "http://megazine.test/api/admin/products/add" \
    -H "Authorization: Bearer {token}" \
    -H "Accept: application/json" \
    -H "Content-Type: application/json" \
    -d '{"product_name":"product A","product_desc":"lorem ipsum","product_type":"normal","price":10.23,"stock":1,"author_id":1,"category_id":1,"product_images":[]}'

```

```javascript
const url = new URL("http://megazine.test/api/admin/products/add");

let headers = {
    "Authorization": "Bearer {token}",
    "Accept": "application/json",
    "Content-Type": "application/json",
}

let body = {
    "product_name": "product A",
    "product_desc": "lorem ipsum",
    "product_type": "normal",
    "price": 10.23,
    "stock": 1,
    "author_id": 1,
    "category_id": 1,
    "product_images": []
}

fetch(url, {
    method: "POST",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```

> Example response (200):

```json
{
    "message": "Product has been created"
}
```

### HTTP Request
`POST api/admin/products/add`

#### Body Parameters

Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    product_name | string |  required  | Name of the product.
    product_desc | string |  required  | Description of the product.
    product_type | string |  required  | Type of the product.
    price | float |  required  | Price of the product.
    stock | float |  required  | Stock of the product.
    author_id | integer |  required  | Author of the product.
    category_id | integer |  required  | Category of the product.
    product_images | array |  optional  | Array of product image.

<!-- END_169c6c5227ff791545658649c38f1351 -->

<!-- START_e09cc2c9ebd691cce2102b25f05e1d45 -->
## Update product detail

> Example request:

```bash
curl -X PUT "http://megazine.test/api/admin/products/update/{product}" \
    -H "Authorization: Bearer {token}" \
    -H "Accept: application/json" \
    -H "Content-Type: application/json" \
    -d '{"product_name":"product AB","product_desc":"sit amet dolor","product_type":"normal","price":13.23,"stock":2,"author_id":1,"category_id":1,"product_images":[]}'

```

```javascript
const url = new URL("http://megazine.test/api/admin/products/update/{product}");

    let params = {
            "author_id": "1",
        };
    Object.keys(params).forEach(key => url.searchParams.append(key, params[key]));

let headers = {
    "Authorization": "Bearer {token}",
    "Accept": "application/json",
    "Content-Type": "application/json",
}

let body = {
    "product_name": "product AB",
    "product_desc": "sit amet dolor",
    "product_type": "normal",
    "price": 13.23,
    "stock": 2,
    "author_id": 1,
    "category_id": 1,
    "product_images": []
}

fetch(url, {
    method: "PUT",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```

> Example response (200):

```json
{
    "message": "Product has been updated"
}
```

### HTTP Request
`PUT api/admin/products/update/{product}`

#### Body Parameters

Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    product_name | string |  required  | Name of the product.
    product_desc | string |  required  | Description of the product.
    product_type | string |  required  | Type of the product.
    price | float |  required  | Price of the product.
    stock | float |  required  | Stock of the product.
    author_id | integer |  required  | Author of the product.
    category_id | integer |  required  | Category of the product.
    product_images | array |  optional  | Array of product image.
#### Query Parameters

Parameter | Status | Description
--------- | ------- | ------- | -----------
    author_id |  required  | Id of product.

<!-- END_e09cc2c9ebd691cce2102b25f05e1d45 -->

<!-- START_2cd3d197dac1e3da1dcd4dbe6bfb5bf1 -->
## Remove product

> Example request:

```bash
curl -X DELETE "http://megazine.test/api/admin/products/delete/{product}" \
    -H "Authorization: Bearer {token}" \
    -H "Accept: application/json"
```

```javascript
const url = new URL("http://megazine.test/api/admin/products/delete/{product}");

    let params = {
            "product_id": "1",
        };
    Object.keys(params).forEach(key => url.searchParams.append(key, params[key]));

let headers = {
    "Authorization": "Bearer {token}",
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "DELETE",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

> Example response (200):

```json
{
    "message": "Product has been deleted"
}
```

### HTTP Request
`DELETE api/admin/products/delete/{product}`

#### Query Parameters

Parameter | Status | Description
--------- | ------- | ------- | -----------
    product_id |  required  | Id of product.

<!-- END_2cd3d197dac1e3da1dcd4dbe6bfb5bf1 -->

<!-- START_e014eba1f86a0e72a2f77360a1d47be2 -->
## Remove product image

> Example request:

```bash
curl -X DELETE "http://megazine.test/api/admin/products/delete-image/{productImage}" \
    -H "Authorization: Bearer {token}" \
    -H "Accept: application/json"
```

```javascript
const url = new URL("http://megazine.test/api/admin/products/delete-image/{productImage}");

    let params = {
            "product_image_id": "1",
        };
    Object.keys(params).forEach(key => url.searchParams.append(key, params[key]));

let headers = {
    "Authorization": "Bearer {token}",
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "DELETE",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

> Example response (200):

```json
{
    "message": "Product's image has been deleted"
}
```

### HTTP Request
`DELETE api/admin/products/delete-image/{productImage}`

#### Query Parameters

Parameter | Status | Description
--------- | ------- | ------- | -----------
    product_image_id |  required  | Id of product's image.

<!-- END_e014eba1f86a0e72a2f77360a1d47be2 -->

#(Admin) Product Collection
<!-- START_5472ad8c6505d4e8f4f4d3fa1b8fdc60 -->
## Get product image collection

> Example request:

```bash
curl -X GET -G "http://megazine.test/api/admin/products-collection" \
    -H "Authorization: Bearer {token}" \
    -H "Accept: application/json"
```

```javascript
const url = new URL("http://megazine.test/api/admin/products-collection");

    let params = {
            "pageSize": "10",
            "page": "0",
            "sorted": "sorted[0][id]=collection_name&amp;sorted[0][desc]=false",
            "filtered": "filtered[0][id]=collection_name&amp;filtered[0][value]=pok@ pai 3",
        };
    Object.keys(params).forEach(key => url.searchParams.append(key, params[key]));

let headers = {
    "Authorization": "Bearer {token}",
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

> Example response (200):

```json
{
    "data": [
        {
            "id": 1,
            "product_id": 1,
            "collection_name": "cover",
            "collection_slug": "cover",
            "created_at": "24-03-2019"
        },
        {
            "id": 2,
            "product_id": 1,
            "collection_name": "gila gila 1",
            "collection_slug": "gila-gila-1",
            "created_at": "24-03-2019"
        },
        {
            "id": 3,
            "product_id": 1,
            "collection_name": "gila gila 788",
            "collection_slug": "gila-gila-788",
            "created_at": "24-03-2019"
        }
    ],
    "meta": {
        "total": 3
    }
}
```

### HTTP Request
`GET api/admin/products-collection`

#### Query Parameters

Parameter | Status | Description
--------- | ------- | ------- | -----------
    pageSize |  optional  | List page size.
    page |  optional  | Page number.
    sorted |  optional  | Array of sorted item. e.g: sort descending by collection_name [{"collection_name":"true"}].
    filtered |  optional  | Array of filtered item. e.g: filter record by collection_name [{"collection_name":"pok@ pai 3"}].

<!-- END_5472ad8c6505d4e8f4f4d3fa1b8fdc60 -->

<!-- START_e0d1f867eda4f327a4452f16949f7881 -->
## Get product image collection details

> Example request:

```bash
curl -X GET -G "http://megazine.test/api/admin/products-collection/{productCollection}" \
    -H "Authorization: Bearer {token}" \
    -H "Accept: application/json"
```

```javascript
const url = new URL("http://megazine.test/api/admin/products-collection/{productCollection}");

    let params = {
            "product_collection_id": "1",
        };
    Object.keys(params).forEach(key => url.searchParams.append(key, params[key]));

let headers = {
    "Authorization": "Bearer {token}",
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

> Example response (200):

```json
{
    "data": {
        "id": 1,
        "product_id": 1,
        "collection_name": "cover",
        "collection_slug": "cover",
        "created_at": "24-03-2019"
    }
}
```

### HTTP Request
`GET api/admin/products-collection/{productCollection}`

#### Query Parameters

Parameter | Status | Description
--------- | ------- | ------- | -----------
    product_collection_id |  required  | Id of the product image collection.

<!-- END_e0d1f867eda4f327a4452f16949f7881 -->

<!-- START_3246e6c40a0357ff7164ea15922eb14c -->
## Add new product image collection

> Example request:

```bash
curl -X POST "http://megazine.test/api/admin/products-collection/add" \
    -H "Authorization: Bearer {token}" \
    -H "Accept: application/json" \
    -H "Content-Type: application/json" \
    -d '{"product_id":1,"collection_name":"siri istimewa hari raya 789","is_hidden":true}'

```

```javascript
const url = new URL("http://megazine.test/api/admin/products-collection/add");

let headers = {
    "Authorization": "Bearer {token}",
    "Accept": "application/json",
    "Content-Type": "application/json",
}

let body = {
    "product_id": 1,
    "collection_name": "siri istimewa hari raya 789",
    "is_hidden": true
}

fetch(url, {
    method: "POST",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```

> Example response (200):

```json
{
    "message": "Collection has been created"
}
```

### HTTP Request
`POST api/admin/products-collection/add`

#### Body Parameters

Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    product_id | integer |  required  | Id of the product.
    collection_name | string |  required  | Title of the Collection.
    is_hidden | boolean |  optional  | Status of the product whether hidden or not.

<!-- END_3246e6c40a0357ff7164ea15922eb14c -->

<!-- START_03d7c4bacf38e8ec8e3473c2ebb3d5c7 -->
## Update product image collection detail

> Example request:

```bash
curl -X PUT "http://megazine.test/api/admin/products-collection/update/{productCollection}" \
    -H "Authorization: Bearer {token}" \
    -H "Accept: application/json" \
    -H "Content-Type: application/json" \
    -d '{"product_id":1,"collection_name":"siri istimewa hari raya 789","is_hidden":true}'

```

```javascript
const url = new URL("http://megazine.test/api/admin/products-collection/update/{productCollection}");

let headers = {
    "Authorization": "Bearer {token}",
    "Accept": "application/json",
    "Content-Type": "application/json",
}

let body = {
    "product_id": 1,
    "collection_name": "siri istimewa hari raya 789",
    "is_hidden": true
}

fetch(url, {
    method: "PUT",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```

> Example response (200):

```json
{
    "message": "Collection has been updated"
}
```

### HTTP Request
`PUT api/admin/products-collection/update/{productCollection}`

#### Body Parameters

Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    product_id | integer |  required  | Id of the product.
    collection_name | string |  required  | Title of the Collection.
    is_hidden | boolean |  optional  | Status of the product whether hidden or not.

<!-- END_03d7c4bacf38e8ec8e3473c2ebb3d5c7 -->

<!-- START_2bd10a73c33d06f8e1b6afe476e96f82 -->
## Remove product image collection

> Example request:

```bash
curl -X DELETE "http://megazine.test/api/admin/products-collection/delete/{productCollection}" \
    -H "Authorization: Bearer {token}" \
    -H "Accept: application/json"
```

```javascript
const url = new URL("http://megazine.test/api/admin/products-collection/delete/{productCollection}");

    let params = {
            "product_collection_id": "1",
        };
    Object.keys(params).forEach(key => url.searchParams.append(key, params[key]));

let headers = {
    "Authorization": "Bearer {token}",
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "DELETE",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

> Example response (200):

```json
{
    "message": "Collection has been deleted"
}
```

### HTTP Request
`DELETE api/admin/products-collection/delete/{productCollection}`

#### Query Parameters

Parameter | Status | Description
--------- | ------- | ------- | -----------
    product_collection_id |  required  | Id of product's collection.

<!-- END_2bd10a73c33d06f8e1b6afe476e96f82 -->

#(Admin) Role
<!-- START_94570d2438af2b542ef2c41e7c981f14 -->
## Get role list

> Example request:

```bash
curl -X GET -G "http://megazine.test/api/admin/roles" \
    -H "Authorization: Bearer {token}" \
    -H "Accept: application/json"
```

```javascript
const url = new URL("http://megazine.test/api/admin/roles");

    let params = {
            "pageSize": "10",
            "page": "0",
            "sorted": "sorted[0][id]=name&amp;sorted[0][desc]=false",
            "filtered": "filtered[0][id]=name&amp;filtered[0][value]=author",
        };
    Object.keys(params).forEach(key => url.searchParams.append(key, params[key]));

let headers = {
    "Authorization": "Bearer {token}",
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

> Example response (200):

```json
{
    "data": [
        {
            "id": 1,
            "name": "superAdmin"
        },
        {
            "id": 2,
            "name": "admin"
        },
        {
            "id": 3,
            "name": "finance"
        },
        {
            "id": 4,
            "name": "author"
        }
    ],
    "meta": {
        "total": 4
    }
}
```

### HTTP Request
`GET api/admin/roles`

#### Query Parameters

Parameter | Status | Description
--------- | ------- | ------- | -----------
    pageSize |  optional  | List page size.
    page |  optional  | Page number.
    sorted |  optional  | Array of sorted item. e.g: sort descending by name [{"name":"true"}].
    filtered |  optional  | Array of filtered item. e.g: filter record by name [{"name":"author"}].

<!-- END_94570d2438af2b542ef2c41e7c981f14 -->

<!-- START_f5ba4005132988d95d7d1b6b23208166 -->
## Get role details

> Example request:

```bash
curl -X GET -G "http://megazine.test/api/admin/roles/{role}" \
    -H "Authorization: Bearer {token}" \
    -H "Accept: application/json"
```

```javascript
const url = new URL("http://megazine.test/api/admin/roles/{role}");

    let params = {
            "role_id": "1",
        };
    Object.keys(params).forEach(key => url.searchParams.append(key, params[key]));

let headers = {
    "Authorization": "Bearer {token}",
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

> Example response (200):

```json
{
    "data": {
        "id": 2,
        "name": "admin",
        "created_at": "22-03-2019",
        "permission": [
            {
                "id": 1,
                "name": "view_admins",
                "created_at": "22-03-2019"
            },
            {
                "id": 2,
                "name": "manage_admins",
                "created_at": "22-03-2019"
            }
        ]
    }
}
```

### HTTP Request
`GET api/admin/roles/{role}`

#### Query Parameters

Parameter | Status | Description
--------- | ------- | ------- | -----------
    role_id |  required  | Id of the role.

<!-- END_f5ba4005132988d95d7d1b6b23208166 -->

<!-- START_29b09f9dfc7bec61bd039632cc6e96f8 -->
## Add new role

> Example request:

```bash
curl -X POST "http://megazine.test/api/admin/roles/add" \
    -H "Authorization: Bearer {token}" \
    -H "Accept: application/json" \
    -H "Content-Type: application/json" \
    -d '{"name":"author"}'

```

```javascript
const url = new URL("http://megazine.test/api/admin/roles/add");

let headers = {
    "Authorization": "Bearer {token}",
    "Accept": "application/json",
    "Content-Type": "application/json",
}

let body = {
    "name": "author"
}

fetch(url, {
    method: "POST",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```

> Example response (200):

```json
{
    "message": "Role has been created"
}
```

### HTTP Request
`POST api/admin/roles/add`

#### Body Parameters

Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    name | string |  required  | Name of the role.

<!-- END_29b09f9dfc7bec61bd039632cc6e96f8 -->

<!-- START_3dc02cd52faae9cd3def397bb5943f4e -->
## Update role detail

> Example request:

```bash
curl -X PUT "http://megazine.test/api/admin/roles/update/{role}" \
    -H "Authorization: Bearer {token}" \
    -H "Accept: application/json" \
    -H "Content-Type: application/json" \
    -d '{"name":"Author"}'

```

```javascript
const url = new URL("http://megazine.test/api/admin/roles/update/{role}");

    let params = {
            "role_id": "1",
        };
    Object.keys(params).forEach(key => url.searchParams.append(key, params[key]));

let headers = {
    "Authorization": "Bearer {token}",
    "Accept": "application/json",
    "Content-Type": "application/json",
}

let body = {
    "name": "Author"
}

fetch(url, {
    method: "PUT",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```

> Example response (200):

```json
{
    "message": "Role has been updated"
}
```

### HTTP Request
`PUT api/admin/roles/update/{role}`

#### Body Parameters

Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    name | string |  required  | Name of the role.
#### Query Parameters

Parameter | Status | Description
--------- | ------- | ------- | -----------
    role_id |  required  | Id of role.

<!-- END_3dc02cd52faae9cd3def397bb5943f4e -->

<!-- START_945419b48fc563ecf173ab4dbc00aa5a -->
## Remove role

> Example request:

```bash
curl -X DELETE "http://megazine.test/api/admin/roles/delete/{role}" \
    -H "Authorization: Bearer {token}" \
    -H "Accept: application/json"
```

```javascript
const url = new URL("http://megazine.test/api/admin/roles/delete/{role}");

    let params = {
            "role_id": "1",
        };
    Object.keys(params).forEach(key => url.searchParams.append(key, params[key]));

let headers = {
    "Authorization": "Bearer {token}",
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "DELETE",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

> Example response (200):

```json
{
    "message": "Role has been deleted"
}
```

### HTTP Request
`DELETE api/admin/roles/delete/{role}`

#### Query Parameters

Parameter | Status | Description
--------- | ------- | ------- | -----------
    role_id |  required  | Id of role.

<!-- END_945419b48fc563ecf173ab4dbc00aa5a -->

<!-- START_1e52554fd4af80e38fededcb4373b3b7 -->
## Give permission

> Example request:

```bash
curl -X POST "http://megazine.test/api/admin/roles/{role}/permissions-attach" \
    -H "Authorization: Bearer {token}" \
    -H "Accept: application/json" \
    -H "Content-Type: application/json" \
    -d '{"permission_id":1}'

```

```javascript
const url = new URL("http://megazine.test/api/admin/roles/{role}/permissions-attach");

    let params = {
            "role_id": "1",
        };
    Object.keys(params).forEach(key => url.searchParams.append(key, params[key]));

let headers = {
    "Authorization": "Bearer {token}",
    "Accept": "application/json",
    "Content-Type": "application/json",
}

let body = {
    "permission_id": 1
}

fetch(url, {
    method: "POST",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```

> Example response (200):

```json
{
    "message": "Permission has been given"
}
```

### HTTP Request
`POST api/admin/roles/{role}/permissions-attach`

#### Body Parameters

Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    permission_id | integer |  required  | Id of the permission.
#### Query Parameters

Parameter | Status | Description
--------- | ------- | ------- | -----------
    role_id |  required  | Id of the role.

<!-- END_1e52554fd4af80e38fededcb4373b3b7 -->

<!-- START_eee4bb11e423588d808fd168fa0fe7d0 -->
## Revoke permission

> Example request:

```bash
curl -X POST "http://megazine.test/api/admin/roles/{role}/permissions-detach" \
    -H "Authorization: Bearer {token}" \
    -H "Accept: application/json" \
    -H "Content-Type: application/json" \
    -d '{"permission_id":1}'

```

```javascript
const url = new URL("http://megazine.test/api/admin/roles/{role}/permissions-detach");

    let params = {
            "role_id": "1",
        };
    Object.keys(params).forEach(key => url.searchParams.append(key, params[key]));

let headers = {
    "Authorization": "Bearer {token}",
    "Accept": "application/json",
    "Content-Type": "application/json",
}

let body = {
    "permission_id": 1
}

fetch(url, {
    method: "POST",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```

> Example response (200):

```json
{
    "message": "Permission has been revoked"
}
```

### HTTP Request
`POST api/admin/roles/{role}/permissions-detach`

#### Body Parameters

Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    permission_id | integer |  required  | Id of the permission.
#### Query Parameters

Parameter | Status | Description
--------- | ------- | ------- | -----------
    role_id |  required  | Id of the role.

<!-- END_eee4bb11e423588d808fd168fa0fe7d0 -->

<!-- START_8606fd9a1980b09a66bb3232b5d485bb -->
## Add multiple permission

> Example request:

```bash
curl -X POST "http://megazine.test/api/admin/roles/{role}/permissions-multiple" \
    -H "Authorization: Bearer {token}" \
    -H "Accept: application/json" \
    -H "Content-Type: application/json" \
    -d '{"permission_id":[]}'

```

```javascript
const url = new URL("http://megazine.test/api/admin/roles/{role}/permissions-multiple");

    let params = {
            "role_id": "1",
        };
    Object.keys(params).forEach(key => url.searchParams.append(key, params[key]));

let headers = {
    "Authorization": "Bearer {token}",
    "Accept": "application/json",
    "Content-Type": "application/json",
}

let body = {
    "permission_id": []
}

fetch(url, {
    method: "POST",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```

> Example response (200):

```json
{
    "message": "Permission has been revoked"
}
```

### HTTP Request
`POST api/admin/roles/{role}/permissions-multiple`

#### Body Parameters

Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    permission_id | array |  required  | Id of the permission.
#### Query Parameters

Parameter | Status | Description
--------- | ------- | ------- | -----------
    role_id |  required  | Id of the role.

<!-- END_8606fd9a1980b09a66bb3232b5d485bb -->

#(Admin) User
<!-- START_da61141e5067867e47712aa0a13baddc -->
## api/admin/users/non-admin
> Example request:

```bash
curl -X GET -G "http://megazine.test/api/admin/users/non-admin" \
    -H "Authorization: Bearer {token}" \
    -H "Accept: application/json"
```

```javascript
const url = new URL("http://megazine.test/api/admin/users/non-admin");

let headers = {
    "Authorization": "Bearer {token}",
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

> Example response (401):

```json
{
    "error": "Unauthenticated"
}
```

### HTTP Request
`GET api/admin/users/non-admin`


<!-- END_da61141e5067867e47712aa0a13baddc -->

<!-- START_1fdf5c126c9b5b722e5044c3f680bf8e -->
## Get user list

> Example request:

```bash
curl -X GET -G "http://megazine.test/api/admin/users" \
    -H "Authorization: Bearer {token}" \
    -H "Accept: application/json"
```

```javascript
const url = new URL("http://megazine.test/api/admin/users");

    let params = {
            "pageSize": "10",
            "page": "0",
            "sorted": "sorted[0][id]=user_name&amp;sorted[0][desc]=false",
            "filtered": "filtered[0][id]=user_name&amp;filtered[0][value]=Aut dignissimos",
        };
    Object.keys(params).forEach(key => url.searchParams.append(key, params[key]));

let headers = {
    "Authorization": "Bearer {token}",
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

> Example response (200):

```json
{
    "data": [
        {
            "id": 1,
            "name": "Ahmad Azuddin",
            "email": "ahmad@azuddin.com",
            "phone_no": null,
            "created_at": "22-03-2019"
        }
    ],
    "meta": {
        "total": 1
    }
}
```

### HTTP Request
`GET api/admin/users`

#### Query Parameters

Parameter | Status | Description
--------- | ------- | ------- | -----------
    pageSize |  optional  | List page size.
    page |  optional  | Page number.
    sorted |  optional  | Array of sorted item. e.g: sort descending by user_name [{"user_name":"true"}].
    filtered |  optional  | Array of filtered item. e.g: filter record by user_name [{"user_name":"Aut dignissimos"}].

<!-- END_1fdf5c126c9b5b722e5044c3f680bf8e -->

<!-- START_538c6352521b306dbf554e89cd64ac98 -->
## Get user details

> Example request:

```bash
curl -X GET -G "http://megazine.test/api/admin/users/{user}" \
    -H "Authorization: Bearer {token}" \
    -H "Accept: application/json"
```

```javascript
const url = new URL("http://megazine.test/api/admin/users/{user}");

    let params = {
            "user_id": "1",
        };
    Object.keys(params).forEach(key => url.searchParams.append(key, params[key]));

let headers = {
    "Authorization": "Bearer {token}",
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

> Example response (200):

```json
{
    "data": {
        "id": 12,
        "name": "azuddin",
        "email": "norawe@azuddin.com",
        "phone_no": "0123456789",
        "avatar": "http:\/\/megazine.test\/avatar\/user\/12-1553341920.png",
        "created_at": "23-03-2019"
    }
}
```

### HTTP Request
`GET api/admin/users/{user}`

#### Query Parameters

Parameter | Status | Description
--------- | ------- | ------- | -----------
    user_id |  required  | Id of the user.

<!-- END_538c6352521b306dbf554e89cd64ac98 -->

<!-- START_8165e4e6df70384300ee642fe11a4e79 -->
## Add new user

> Example request:

```bash
curl -X POST "http://megazine.test/api/admin/users/add" \
    -H "Authorization: Bearer {token}" \
    -H "Accept: application/json" \
    -H "Content-Type: application/json" \
    -d '{"email":"ahmad@azuddin.com","name":"azuddin","phone_no":"0123456789","avatar":"ByuJcDXYFj9PQg6a"}'

```

```javascript
const url = new URL("http://megazine.test/api/admin/users/add");

let headers = {
    "Authorization": "Bearer {token}",
    "Accept": "application/json",
    "Content-Type": "application/json",
}

let body = {
    "email": "ahmad@azuddin.com",
    "name": "azuddin",
    "phone_no": "0123456789",
    "avatar": "ByuJcDXYFj9PQg6a"
}

fetch(url, {
    method: "POST",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```

> Example response (200):

```json
{
    "data": {
        "id": 1,
        "name": "azuddin",
        "email": "ahmad@azuddin.com",
        "phone_no": "0123456789",
        "avatar": "http:\/\/megazine.test\/avatar\/user\/12-1553341920.png",
        "created_at": "23-03-2019"
    }
}
```

### HTTP Request
`POST api/admin/users/add`

#### Body Parameters

Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    email | string |  required  | Email of the user account.
    name | string |  required  | Name of the account.
    phone_no | string |  optional  | Phone number of the user.
    avatar | image |  optional  | Image for avatar of the user

<!-- END_8165e4e6df70384300ee642fe11a4e79 -->

<!-- START_8c9a07a6c429ca27868a750feb5044ee -->
## Update user detail

> Example request:

```bash
curl -X PUT "http://megazine.test/api/admin/users/update/{user}" \
    -H "Authorization: Bearer {token}" \
    -H "Accept: application/json" \
    -H "Content-Type: application/json" \
    -d '{"email":"ahmad@azuddin.com","name":"azuddin","phone_no":"0123456789","avatar":"3uoW8siqRizhYNA9"}'

```

```javascript
const url = new URL("http://megazine.test/api/admin/users/update/{user}");

    let params = {
            "user_id": "1",
        };
    Object.keys(params).forEach(key => url.searchParams.append(key, params[key]));

let headers = {
    "Authorization": "Bearer {token}",
    "Accept": "application/json",
    "Content-Type": "application/json",
}

let body = {
    "email": "ahmad@azuddin.com",
    "name": "azuddin",
    "phone_no": "0123456789",
    "avatar": "3uoW8siqRizhYNA9"
}

fetch(url, {
    method: "PUT",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```

> Example response (200):

```json
{
    "data": {
        "id": 1,
        "name": "azuddin",
        "email": "ahmad@azuddin.com",
        "phone_no": "0123456789",
        "avatar": "http:\/\/megazine.test\/avatar\/user\/12-1553341920.png",
        "created_at": "23-03-2019"
    }
}
```

### HTTP Request
`PUT api/admin/users/update/{user}`

#### Body Parameters

Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    email | string |  required  | Email of the user account.
    name | string |  required  | Name of the account.
    phone_no | string |  optional  | Phone number of the user.
    avatar | image |  optional  | Image for avatar of the user
#### Query Parameters

Parameter | Status | Description
--------- | ------- | ------- | -----------
    user_id |  required  | Id of user.

<!-- END_8c9a07a6c429ca27868a750feb5044ee -->

<!-- START_1d849c1563ee87ac7e9558af99bb9f71 -->
## Remove user

> Example request:

```bash
curl -X DELETE "http://megazine.test/api/admin/users/delete/{user}" \
    -H "Authorization: Bearer {token}" \
    -H "Accept: application/json"
```

```javascript
const url = new URL("http://megazine.test/api/admin/users/delete/{user}");

    let params = {
            "user_id": "1",
        };
    Object.keys(params).forEach(key => url.searchParams.append(key, params[key]));

let headers = {
    "Authorization": "Bearer {token}",
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "DELETE",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

> Example response (200):

```json
{
    "message": "User has been deleted"
}
```

### HTTP Request
`DELETE api/admin/users/delete/{user}`

#### Query Parameters

Parameter | Status | Description
--------- | ------- | ------- | -----------
    user_id |  required  | Id of user.

<!-- END_1d849c1563ee87ac7e9558af99bb9f71 -->

#Authentication

You authenticate to the application by providing your access token in the request header. You can get your access token from login endpoint.
Authentication to the API occurs via OAuth2. Provide your access token as the value of Authorization request header.

FUTURE: All API requests must be made over HTTPS. Calls made over plain HTTP will fail.
<!-- START_c3fa189a6c95ca36ad6ac4791a873d23 -->
## Login

> Example request:

```bash
curl -X POST "http://megazine.test/api/login" \
    -H "Accept: application/json" \
    -H "Content-Type: application/json" \
    -d '{"email":"ahmad@azuddin.com","password":"secret"}'

```

```javascript
const url = new URL("http://megazine.test/api/login");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

let body = {
    "email": "ahmad@azuddin.com",
    "password": "secret"
}

fetch(url, {
    method: "POST",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```

> Example response (200):

```json
{
    "data": {
        "id": 1,
        "name": "Ahmad Azuddin",
        "email": "ahmad@azuddin.com",
        "phone_no": null,
        "avatar": null
    },
    "token": "eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6IjM2ZDJmNGVlZWM3OGJlYjYwYTQ5NTE4MTZhMzM5MDdkZDJiNTNjNGI0NWE0YTVlZmE4MGFjNWJkZmYzMGRiMjhkNzNlYzBlNWZiNzhkZTU5In0.eyJhdWQiOiIxIiwianRpIjoiMzZkMmY0ZWVlYzc4YmViNjBhNDk1MTgxNmEzMzkwN2RkMmI1M2M0YjQ1YTRhNWVmYTgwYWM1YmRmZjMwZGIyOGQ3M2VjMGU1ZmI3OGRlNTkiLCJpYXQiOjE1NTMzNDI2MDEsIm5iZiI6MTU1MzM0MjYwMSwiZXhwIjoxNTg0OTY1MDAxLCJzdWIiOiIyIiwic2NvcGVzIjpbXX0.tzm5ewp6NEb97_JLFhZSdg3XYVn2jf4bb_Wa_PEA7fKdzbwUb2CxXWsqnwlsHhUZ3h1XZ25DxmT0nuiYngoWc7wmfZlLWZRCmuEtIi2HDASDM6EWb21gyCmj_tWl8cGvUOfk6M4CjpHQ8oZt39CH6tycduX_4dUGKXfO7cM2l_obYPDc4Yiarp1Wy3tNK5jV2pd8AKYhcPXcfGALS3aQ3YPBHEVqppkA1SROtBHEiYAmE5WNZB5viWTbOVu1QCVKxbOLuEe4lD1hW87TitUtcS5xw-aSxFM1cvszWVfysEVi5ykIoZmJVM9G7SoW5a7TldGM7WQ6iVNsko1NRhEjZFYmYpv18qafBrg26MXJwqnjvWdcszSZgyZHBAbd1ornn1f4U23rmjeX6SELh-MjyaK-LRpyimbtfWI_kOM0YJp420cW986etH-90PmiGGc4HdYiKJPii70gaWW-i3sdMj8nHMb8wwnQgHlcggQ6nwhV-0ess3T84hFiox2h8BtmfDYI0KZywMm96sgVH1bhj0ehky2zS8Uqljtot6DMt0-tENX2pPD6wm1c7KHbGhv0BOK0oNEUZsH1DeZxB4tKbH--fAtHUTu5u7KBN_AqZfAEG7j7CkzPb4P6aci5pJSGunOtGS1vi4ISoRVBrdAHxEcC1JLVmVERbpkWIkVuQOo",
    "expiry": "2019-03-24 12:03:21"
}
```

### HTTP Request
`POST api/login`

#### Body Parameters

Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    email | string |  required  | Email of the user account.
    password | string |  required  | Password of the account.

<!-- END_c3fa189a6c95ca36ad6ac4791a873d23 -->

<!-- START_90f45d502fd52fdc0b289e55ba3c2ec6 -->
## Signup

> Example request:

```bash
curl -X POST "http://megazine.test/api/signup" \
    -H "Accept: application/json" \
    -H "Content-Type: application/json" \
    -d '{"email":"ahmad@azuddin.com","name":"azuddin","password":"secret","password_confirmation":"secret","phone_no":"0123456789","avatar":"k8MeOmw636fhQS91"}'

```

```javascript
const url = new URL("http://megazine.test/api/signup");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

let body = {
    "email": "ahmad@azuddin.com",
    "name": "azuddin",
    "password": "secret",
    "password_confirmation": "secret",
    "phone_no": "0123456789",
    "avatar": "k8MeOmw636fhQS91"
}

fetch(url, {
    method: "POST",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```

> Example response (200):

```json
{
    "data": {
        "id": 1,
        "name": "azuddin",
        "email": "ahmad@azuddin.com",
        "phone_no": "0123456789",
        "avatar": "http:\/\/megazine.test\/avatar\/user\/14-1553342755.png"
    }
}
```

### HTTP Request
`POST api/signup`

#### Body Parameters

Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    email | string |  required  | Email of the user account.
    name | string |  required  | Name of the account.
    password | string |  required  | Password of the account.
    password_confirmation | string |  required  | Password Confirmation of the Password.
    phone_no | string |  optional  | Phone number of the user.
    avatar | image |  optional  | Image for avatar of the user

<!-- END_90f45d502fd52fdc0b289e55ba3c2ec6 -->

<!-- START_1f57315b1ba4052c129395d6bd541458 -->
## Resend verification email

> Example request:

```bash
curl -X POST "http://megazine.test/api/resend-verification" \
    -H "Accept: application/json" \
    -H "Content-Type: application/json" \
    -d '{"email":"ahmad@azuddin.com"}'

```

```javascript
const url = new URL("http://megazine.test/api/resend-verification");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

let body = {
    "email": "ahmad@azuddin.com"
}

fetch(url, {
    method: "POST",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```

> Example response (200):

```json
{
    "message": "Verification email was sent"
}
```

### HTTP Request
`POST api/resend-verification`

#### Body Parameters

Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    email | string |  required  | Email of the user account.

<!-- END_1f57315b1ba4052c129395d6bd541458 -->

<!-- START_3c520b0ccdbf5100b6f6994368e1b344 -->
## Get user profile

> Example request:

```bash
curl -X GET -G "http://megazine.test/api/profile" \
    -H "Authorization: Bearer {token}" \
    -H "Accept: application/json"
```

```javascript
const url = new URL("http://megazine.test/api/profile");

let headers = {
    "Authorization": "Bearer {token}",
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

> Example response (200):

```json
{
    "data": {
        "id": 1,
        "name": "azuddin",
        "email": "ahmad@azuddin.com",
        "phone_no": "0123456789",
        "avatar": "http:\/\/megazine.test\/avatar\/user\/14-1553342755.png"
    }
}
```

### HTTP Request
`GET api/profile`


<!-- END_3c520b0ccdbf5100b6f6994368e1b344 -->

<!-- START_1497ed33b433ac5263898f3caa2548a7 -->
## Update user profile

> Example request:

```bash
curl -X POST "http://megazine.test/api/profile" \
    -H "Authorization: Bearer {token}" \
    -H "Accept: application/json" \
    -H "Content-Type: application/json" \
    -d '{"email":"ahmad@azuddin.com","name":"azuddin","password":"secret","password_confirmation":"secret","phone_no":"0123456789","avatar":"wLlJ580vmOenUbX4"}'

```

```javascript
const url = new URL("http://megazine.test/api/profile");

let headers = {
    "Authorization": "Bearer {token}",
    "Accept": "application/json",
    "Content-Type": "application/json",
}

let body = {
    "email": "ahmad@azuddin.com",
    "name": "azuddin",
    "password": "secret",
    "password_confirmation": "secret",
    "phone_no": "0123456789",
    "avatar": "wLlJ580vmOenUbX4"
}

fetch(url, {
    method: "POST",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```

> Example response (200):

```json
{
    "data": {
        "id": 1,
        "name": "azuddin",
        "email": "ahmad@azuddin.com",
        "phone_no": "0123456789",
        "avatar": "http:\/\/megazine.test\/avatar\/user\/14-1553342755.png"
    }
}
```

### HTTP Request
`POST api/profile`

#### Body Parameters

Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    email | string |  optional  | Email of the user account.
    name | string |  optional  | Name of the account.
    password | string |  optional  | Password of the account.
    password_confirmation | string |  optional  | Password Confirmation of the Password.
    phone_no | string |  optional  | Phone number of the user.
    avatar | image |  optional  | Image for avatar of the user

<!-- END_1497ed33b433ac5263898f3caa2548a7 -->

#Author
<!-- START_405a8a7729f51fc1f43466304b8a733e -->
## Get author list

Example url: /api/author?pageSize=2&page=0&sorted[0][id]=author_name&sorted[0][desc]=false&filtered[0][id]=author_name&filtered[0][value]=et

> Example request:

```bash
curl -X GET -G "http://megazine.test/api/author" \
    -H "Accept: application/json"
```

```javascript
const url = new URL("http://megazine.test/api/author");

    let params = {
            "pageSize": "10",
            "page": "1",
            "sorted": "sorted[0][id]=author_name&amp;sorted[0][desc]=false",
            "filtered": "filtered[0][id]=author_name&amp;filtered[0][value]=Stan Lee",
        };
    Object.keys(params).forEach(key => url.searchParams.append(key, params[key]));

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

> Example response (200):

```json
{
    "data": [
        {
            "id": 1,
            "author_name": "CESB",
            "alias_name": null,
            "email": null,
            "avatar": null
        },
        {
            "id": 2,
            "author_name": "epie",
            "alias_name": null,
            "email": null,
            "avatar": null
        },
        {
            "id": 3,
            "author_name": "Halia",
            "alias_name": null,
            "email": null,
            "avatar": null
        }
    ],
    "meta": {
        "total": 3
    }
}
```

### HTTP Request
`GET api/author`

#### Query Parameters

Parameter | Status | Description
--------- | ------- | ------- | -----------
    pageSize |  optional  | List page size.
    page |  optional  | Page number.
    sorted |  optional  | Array of sorted item. e.g: sort descending by author_name [{"author_name":"true"}].
    filtered |  optional  | Array of filtered item. e.g: filter record by author_name [{"author_name":"Stan Lee"}].

<!-- END_405a8a7729f51fc1f43466304b8a733e -->

<!-- START_42a3d84c30811b5dbf3e40825b48a3b6 -->
## Get author details

> Example request:

```bash
curl -X GET -G "http://megazine.test/api/author/{author}" \
    -H "Accept: application/json"
```

```javascript
const url = new URL("http://megazine.test/api/author/{author}");

    let params = {
            "author_id": "1",
        };
    Object.keys(params).forEach(key => url.searchParams.append(key, params[key]));

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

> Example response (200):

```json
{
    "data": {
        "id": 1,
        "author_name": "CESB",
        "alias_name": null,
        "email": null,
        "avatar": null,
        "created_at": "22-03-2019"
    }
}
```

### HTTP Request
`GET api/author/{author}`

#### Query Parameters

Parameter | Status | Description
--------- | ------- | ------- | -----------
    author_id |  required  | Id of the author.

<!-- END_42a3d84c30811b5dbf3e40825b48a3b6 -->

#Bundle
<!-- START_02d11302e6795f5627774c0a59c7b5f4 -->
## Get bundle list

Example url: /api/bundle?pageSize=2&page=0&sorted[0][id]=bundle_name&sorted[0][desc]=false&filtered[0][id]=bundle_name&filtered[0][value]=et

> Example request:

```bash
curl -X GET -G "http://megazine.test/api/bundle" \
    -H "Accept: application/json"
```

```javascript
const url = new URL("http://megazine.test/api/bundle");

    let params = {
            "pageSize": "10",
            "page": "1",
            "sorted": "sorted[0][id]=bundle_name&amp;sorted[0][desc]=false",
            "filtered": "filtered[0][id]=bundle_name&amp;filtered[0][value]=Bundle Raya",
        };
    Object.keys(params).forEach(key => url.searchParams.append(key, params[key]));

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

> Example response (200):

```json
{
    "data": [
        {
            "id": 1,
            "bundle_name": "Molestiae corporis qui dolorem id.",
            "bundle_desc": "Voluptates quis ut rerum officiis. Molestias vitae qui deleniti porro nisi.\n\nNulla impedit suscipit harum et. Blanditiis corrupti modi qui dolores consequatur. Quos dolorem sequi architecto ut mollitia. Laborum voluptatum perspiciatis sit quibusdam eaque aut praesentium.",
            "images": [],
            "bundle_items_count": 3,
            "price": "21.14"
        }
    ],
    "meta": {
        "total": 1
    }
}
```

### HTTP Request
`GET api/bundle`

#### Query Parameters

Parameter | Status | Description
--------- | ------- | ------- | -----------
    pageSize |  optional  | List page size.
    page |  optional  | Page number.
    sorted |  optional  | Array of sorted item. e.g: sort descending by bundle_name [{"bundle_name":"true"}].
    filtered |  optional  | Array of filtered item. e.g: filter record by bundle_name [{"bundle_name":"Bundle Raya"}].

<!-- END_02d11302e6795f5627774c0a59c7b5f4 -->

<!-- START_e142a138dc21e58adfd057847e21384d -->
## Get bundle details

> Example request:

```bash
curl -X GET -G "http://megazine.test/api/bundle/{bundle}" \
    -H "Accept: application/json"
```

```javascript
const url = new URL("http://megazine.test/api/bundle/{bundle}");

    let params = {
            "bundle_id": "1",
        };
    Object.keys(params).forEach(key => url.searchParams.append(key, params[key]));

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

> Example response (200):

```json
{
    "data": {
        "id": 1,
        "bundle_name": "Molestiae corporis qui dolorem id.",
        "bundle_desc": "Voluptates quis ut rerum officiis. Molestias vitae qui deleniti porro nisi.\n\nNulla impedit suscipit harum et. Blanditiis corrupti modi qui dolores consequatur. Quos dolorem sequi architecto ut mollitia. Laborum voluptatum perspiciatis sit quibusdam eaque aut praesentium.",
        "images": [],
        "items": [
            {
                "id": 1,
                "product": {
                    "id": 1,
                    "product_name": "Gila-Gila",
                    "product_desc": null,
                    "product_type": "normal",
                    "author_id": 6,
                    "category_id": 2,
                    "price": "0.00",
                    "stock": 1,
                    "hit": null,
                    "likes": "0",
                    "is_public": 1,
                    "product_images": [
                        {
                            "url": "gg\/gg001\/gg00101.jpg",
                            "collection_no": 0,
                            "order": 1
                        }
                    ]
                },
                "price": "0.00",
                "quantity": 1
            }
        ],
        "price": "21.14"
    }
}
```

### HTTP Request
`GET api/bundle/{bundle}`

#### Query Parameters

Parameter | Status | Description
--------- | ------- | ------- | -----------
    bundle_id |  required  | Id of the bundle.

<!-- END_e142a138dc21e58adfd057847e21384d -->

<!-- START_02d11302e6795f5627774c0a59c7b5f4 -->
## Get bundle list

Example url: /api/bundle?pageSize=2&page=0&sorted[0][id]=bundle_name&sorted[0][desc]=false&filtered[0][id]=bundle_name&filtered[0][value]=et

> Example request:

```bash
curl -X GET -G "http://megazine.test/api/bundle" \
    -H "Authorization: Bearer {token}" \
    -H "Accept: application/json"
```

```javascript
const url = new URL("http://megazine.test/api/bundle");

    let params = {
            "pageSize": "10",
            "page": "1",
            "sorted": "sorted[0][id]=bundle_name&amp;sorted[0][desc]=false",
            "filtered": "filtered[0][id]=bundle_name&amp;filtered[0][value]=Bundle Raya",
        };
    Object.keys(params).forEach(key => url.searchParams.append(key, params[key]));

let headers = {
    "Authorization": "Bearer {token}",
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

> Example response (200):

```json
{
    "data": [
        {
            "id": 1,
            "bundle_name": "Molestiae corporis qui dolorem id.",
            "bundle_desc": "Voluptates quis ut rerum officiis. Molestias vitae qui deleniti porro nisi.\n\nNulla impedit suscipit harum et. Blanditiis corrupti modi qui dolores consequatur. Quos dolorem sequi architecto ut mollitia. Laborum voluptatum perspiciatis sit quibusdam eaque aut praesentium.",
            "images": [],
            "bundle_items_count": 3,
            "price": "21.14"
        }
    ],
    "meta": {
        "total": 1
    }
}
```

### HTTP Request
`GET api/bundle`

#### Query Parameters

Parameter | Status | Description
--------- | ------- | ------- | -----------
    pageSize |  optional  | List page size.
    page |  optional  | Page number.
    sorted |  optional  | Array of sorted item. e.g: sort descending by bundle_name [{"bundle_name":"true"}].
    filtered |  optional  | Array of filtered item. e.g: filter record by bundle_name [{"bundle_name":"Bundle Raya"}].

<!-- END_02d11302e6795f5627774c0a59c7b5f4 -->

<!-- START_e142a138dc21e58adfd057847e21384d -->
## Get bundle details

> Example request:

```bash
curl -X GET -G "http://megazine.test/api/bundle/{bundle}" \
    -H "Authorization: Bearer {token}" \
    -H "Accept: application/json"
```

```javascript
const url = new URL("http://megazine.test/api/bundle/{bundle}");

    let params = {
            "bundle_id": "1",
        };
    Object.keys(params).forEach(key => url.searchParams.append(key, params[key]));

let headers = {
    "Authorization": "Bearer {token}",
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

> Example response (200):

```json
{
    "data": {
        "id": 1,
        "bundle_name": "Molestiae corporis qui dolorem id.",
        "bundle_desc": "Voluptates quis ut rerum officiis. Molestias vitae qui deleniti porro nisi.\n\nNulla impedit suscipit harum et. Blanditiis corrupti modi qui dolores consequatur. Quos dolorem sequi architecto ut mollitia. Laborum voluptatum perspiciatis sit quibusdam eaque aut praesentium.",
        "images": [],
        "items": [
            {
                "id": 1,
                "product": {
                    "id": 1,
                    "product_name": "Gila-Gila",
                    "product_desc": null,
                    "product_type": "normal",
                    "author_id": 6,
                    "category_id": 2,
                    "price": "0.00",
                    "stock": 1,
                    "hit": null,
                    "likes": "0",
                    "is_public": 1,
                    "product_images": [
                        {
                            "url": "gg\/gg001\/gg00101.jpg",
                            "collection_no": 0,
                            "order": 1
                        }
                    ]
                },
                "price": "0.00",
                "quantity": 1
            }
        ],
        "price": "21.14"
    }
}
```

### HTTP Request
`GET api/bundle/{bundle}`

#### Query Parameters

Parameter | Status | Description
--------- | ------- | ------- | -----------
    bundle_id |  required  | Id of the bundle.

<!-- END_e142a138dc21e58adfd057847e21384d -->

#Cart
<!-- START_6c1682f4a40c1555254df708bf6b4a71 -->
## Get cart list

Example url: /api/cart?pageSize=2&page=0&sorted[0][id]=price&sorted[0][desc]=false&filtered[0][id]=price&filtered[0][value]=78.48

> Example request:

```bash
curl -X GET -G "http://megazine.test/api/cart" \
    -H "Authorization: Bearer {token}" \
    -H "Accept: application/json"
```

```javascript
const url = new URL("http://megazine.test/api/cart");

    let params = {
            "pageSize": "10",
            "page": "1",
            "sorted": "sorted[0][id]=price&amp;sorted[0][desc]=false",
            "filtered": "filtered[0][id]=price&amp;filtered[0][value]=78.48",
        };
    Object.keys(params).forEach(key => url.searchParams.append(key, params[key]));

let headers = {
    "Authorization": "Bearer {token}",
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

> Example response (200):

```json
{
    "data": [
        {
            "id": 1,
            "name": "Molestias velit et consectetur.",
            "product_id": null,
            "bundle_id": 2,
            "price": "79.16",
            "quantity": 1
        }
    ],
    "meta": {
        "total": 1
    }
}
```

### HTTP Request
`GET api/cart`

#### Query Parameters

Parameter | Status | Description
--------- | ------- | ------- | -----------
    pageSize |  optional  | List page size.
    page |  optional  | Page number.
    sorted |  optional  | Array sorted item. e.g: sort descending by price [{"price":"true"}].
    filtered |  optional  | Array filtered item. e.g: filter record by price [{"price":"78.48"}].

<!-- END_6c1682f4a40c1555254df708bf6b4a71 -->

<!-- START_ffc9879f3ac5b6304e7047c50762e044 -->
## Add an item into cart

> Example request:

```bash
curl -X POST "http://megazine.test/api/cart" \
    -H "Authorization: Bearer {token}" \
    -H "Accept: application/json" \
    -H "Content-Type: application/json" \
    -d '{"product_id":1,"bundle_id":2,"quantity":1}'

```

```javascript
const url = new URL("http://megazine.test/api/cart");

let headers = {
    "Authorization": "Bearer {token}",
    "Accept": "application/json",
    "Content-Type": "application/json",
}

let body = {
    "product_id": 1,
    "bundle_id": 2,
    "quantity": 1
}

fetch(url, {
    method: "POST",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```

> Example response (200):

```json
{
    "message": "Item has been added to cart"
}
```

### HTTP Request
`POST api/cart`

#### Body Parameters

Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    product_id | integer |  required  | Id of the product. *Optional if bundle_id is exist.
    bundle_id | integer |  optional  | Id of the bundle.
    quantity | integer |  required  | Quantity of the item.

<!-- END_ffc9879f3ac5b6304e7047c50762e044 -->

<!-- START_b8eb27f2a7d2b31b63d5d2ce9917c9f7 -->
## Delete an item from cart

> Example request:

```bash
curl -X DELETE "http://megazine.test/api/cart/{cart}" \
    -H "Authorization: Bearer {token}" \
    -H "Accept: application/json"
```

```javascript
const url = new URL("http://megazine.test/api/cart/{cart}");

    let params = {
            "cart_id": "1",
        };
    Object.keys(params).forEach(key => url.searchParams.append(key, params[key]));

let headers = {
    "Authorization": "Bearer {token}",
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "DELETE",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

> Example response (200):

```json
{
    "message": "Item has been deleted from cart"
}
```

### HTTP Request
`DELETE api/cart/{cart}`

#### Query Parameters

Parameter | Status | Description
--------- | ------- | ------- | -----------
    cart_id |  required  | Id of the cart item.

<!-- END_b8eb27f2a7d2b31b63d5d2ce9917c9f7 -->

#Category
<!-- START_db20564ba266cd451caac114b3eac8ab -->
## Get category list

Example url: /api/category?pageSize=2&page=0&sorted[0][id]=category_name&sorted[0][desc]=false&filtered[0][id]=category_name&filtered[0][value]=et

> Example request:

```bash
curl -X GET -G "http://megazine.test/api/category" \
    -H "Accept: application/json"
```

```javascript
const url = new URL("http://megazine.test/api/category");

    let params = {
            "pageSize": "10",
            "page": "1",
            "sorted": "sorted[0][id]=category_name&amp;sorted[0][desc]=false",
            "filtered": "filtered[0][id]=category_name&amp;filtered[0][value]=Magazine",
        };
    Object.keys(params).forEach(key => url.searchParams.append(key, params[key]));

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

> Example response (200):

```json
{
    "data": [
        {
            "id": 1,
            "category_name": "Komik"
        },
        {
            "id": 2,
            "category_name": "Majalah"
        },
        {
            "id": 3,
            "category_name": "Koleksi"
        }
    ],
    "meta": {
        "total": 3
    }
}
```

### HTTP Request
`GET api/category`

#### Query Parameters

Parameter | Status | Description
--------- | ------- | ------- | -----------
    pageSize |  optional  | List page size.
    page |  optional  | Page number.
    sorted |  optional  | Array of sorted item. e.g: sort descending by category_name [{"category_name":"true"}].
    filtered |  optional  | Array of filtered item. e.g: filter record by category_name [{"category_name":"Magazine"}].

<!-- END_db20564ba266cd451caac114b3eac8ab -->

<!-- START_977e23840a7e9249b1f7136f1eadabe2 -->
## Get category details

> Example request:

```bash
curl -X GET -G "http://megazine.test/api/category/{category}" \
    -H "Accept: application/json"
```

```javascript
const url = new URL("http://megazine.test/api/category/{category}");

    let params = {
            "category_id": "1",
        };
    Object.keys(params).forEach(key => url.searchParams.append(key, params[key]));

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

> Example response (200):

```json
{
    "data": {
        "id": 1,
        "category_name": "Komik"
    }
}
```

### HTTP Request
`GET api/category/{category}`

#### Query Parameters

Parameter | Status | Description
--------- | ------- | ------- | -----------
    category_id |  required  | Id of the category.

<!-- END_977e23840a7e9249b1f7136f1eadabe2 -->

#Order
<!-- START_d2e080af51835880674d3e2496ed6e62 -->
## Get order list

Example url: /api/order?pageSize=2&page=0&sorted[0][id]=order_ref&sorted[0][desc]=false&filtered[0][id]=order_ref&filtered[0][value]=REF8976189761

> Example request:

```bash
curl -X GET -G "http://megazine.test/api/order" \
    -H "Authorization: Bearer {token}" \
    -H "Accept: application/json"
```

```javascript
const url = new URL("http://megazine.test/api/order");

    let params = {
            "pageSize": "10",
            "page": "1",
            "sorted": "sorted[0][id]=order_ref&amp;sorted[0][desc]=false",
            "filtered": "filtered[0][id]=order_ref&amp;filtered[0][value]=REF8976189761",
        };
    Object.keys(params).forEach(key => url.searchParams.append(key, params[key]));

let headers = {
    "Authorization": "Bearer {token}",
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

> Example response (200):

```json
{
    "data": [
        {
            "order_ref": "REF5722257221",
            "total": "0.00",
            "order_status": "pending",
            "order_items_count": 0
        }
    ],
    "meta": {
        "total": 1
    }
}
```

### HTTP Request
`GET api/order`

#### Query Parameters

Parameter | Status | Description
--------- | ------- | ------- | -----------
    pageSize |  optional  | List page size.
    page |  optional  | Page number.
    sorted |  optional  | Array sorted item. e.g: sort descending by order_ref [{"order_ref":"true"}].
    filtered |  optional  | Array filtered item. e.g: filter record by order_ref [{"order_ref":"REF8976189761"}].

<!-- END_d2e080af51835880674d3e2496ed6e62 -->

<!-- START_9c4ec790d3f07a332b085b8efc187b58 -->
## Get order details

> Example request:

```bash
curl -X GET -G "http://megazine.test/api/order/{order}" \
    -H "Authorization: Bearer {token}" \
    -H "Accept: application/json"
```

```javascript
const url = new URL("http://megazine.test/api/order/{order}");

    let params = {
            "order": "REF8976189761",
        };
    Object.keys(params).forEach(key => url.searchParams.append(key, params[key]));

let headers = {
    "Authorization": "Bearer {token}",
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

> Example response (200):

```json
{
    "data": {
        "order_ref": "REF8976189761",
        "total": 177.16,
        "order_status": "paid",
        "order_items": [
            {
                "product_id": 1,
                "bundle_id": null,
                "bought_price": 49.34,
                "quantity": 2
            },
            {
                "product_id": 11,
                "bundle_id": 1,
                "bought_price": 25.79,
                "quantity": 1
            }
        ]
    }
}
```

### HTTP Request
`GET api/order/{order}`

#### Query Parameters

Parameter | Status | Description
--------- | ------- | ------- | -----------
    order |  required  | Reference id of the order.

<!-- END_9c4ec790d3f07a332b085b8efc187b58 -->

<!-- START_cd95d3e90339c282e0b608349e80a381 -->
## Create an order

> Example request:

```bash
curl -X POST "http://megazine.test/api/order" \
    -H "Authorization: Bearer {token}" \
    -H "Accept: application/json"
```

```javascript
const url = new URL("http://megazine.test/api/order");

let headers = {
    "Authorization": "Bearer {token}",
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "POST",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

> Example response (200):

```json
{
    "message": "Order has been created",
    "order_ref": "REF7287672873",
    "redirect_url": "https:\/\/billplz-staging.herokuapp.com\/bills\/0gme8dub"
}
```

### HTTP Request
`POST api/order`


<!-- END_cd95d3e90339c282e0b608349e80a381 -->

#Product
<!-- START_dc538d69a8586a7a3c36d4393cee42e6 -->
## Get product list

Example url: /api/product?pageSize=2&page=0&sorted[0][id]=product_name&sorted[0][desc]=false&filtered[0][id]=product_name&filtered[0][value]=Aut dignissimos

> Example request:

```bash
curl -X GET -G "http://megazine.test/api/product" \
    -H "Accept: application/json"
```

```javascript
const url = new URL("http://megazine.test/api/product");

    let params = {
            "pageSize": "10",
            "page": "0",
            "sorted": "sorted[0][id]=product_name&amp;sorted[0][desc]=false",
            "filtered": "filtered[0][id]=product_name&amp;filtered[0][value]=Product A",
        };
    Object.keys(params).forEach(key => url.searchParams.append(key, params[key]));

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

> Example response (200):

```json
{
    "data": [
        {
            "id": 1,
            "product_name": "Gila-Gila",
            "product_desc": null,
            "product_type": "normal",
            "author_id": 6,
            "category_id": 2,
            "price": "0.00",
            "stock": 1,
            "hit": 7,
            "likes": "0",
            "is_public": 1,
            "cover_images": [
                {
                    "url": "gg\/gg001\/gg00101.jpg",
                    "collection_slug": "cover",
                    "order": 1
                }
            ]
        }
    ],
    "meta": {
        "total": 1
    }
}
```

### HTTP Request
`GET api/product`

#### Query Parameters

Parameter | Status | Description
--------- | ------- | ------- | -----------
    pageSize |  optional  | List page size.
    page |  optional  | Page number.
    sorted |  optional  | Array of sorted item. e.g: sort descending by product_name [{"product_name":"true"}].
    filtered |  optional  | Array of filtered item. e.g: filter record by product_name [{"product_name":"Product A"}].

<!-- END_dc538d69a8586a7a3c36d4393cee42e6 -->

<!-- START_1fcbf5d495e6ada99ea017e9ae32b380 -->
## Get product details

> Example request:

```bash
curl -X GET -G "http://megazine.test/api/product/{product}" \
    -H "Accept: application/json"
```

```javascript
const url = new URL("http://megazine.test/api/product/{product}");

    let params = {
            "product_id": "1",
        };
    Object.keys(params).forEach(key => url.searchParams.append(key, params[key]));

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

> Example response (200):

```json
{
    "data": {
        "id": 1,
        "product_name": "Gila-Gila",
        "product_desc": null,
        "product_type": "normal",
        "author_id": 6,
        "category_id": 2,
        "price": "0.00",
        "stock": 1,
        "hit": 5,
        "likes": "0",
        "cover_images": [
            {
                "url": "gg\/gg001\/gg00101.jpg",
                "order": 1
            }
        ],
        "collections": [
            {
                "id": 2,
                "collection_name": "gila gila 1",
                "collection_image_first": {
                    "url": "gg\/gg001\/gg00150.jpg",
                    "order": 1
                },
                "collection_image_count": 50,
                "hit": 4
            },
            {
                "id": 3,
                "collection_name": "gila gila 788",
                "collection_image_first": {
                    "url": "gg\/gg788\/gg78849.jpg",
                    "order": 1
                },
                "collection_image_count": 95,
                "hit": null
            },
            {
                "id": 4,
                "collection_name": "siri istimewa hari raya 789",
                "collection_image_first": {
                    "url": "gg\/gg789\/gg78947.jpg",
                    "order": 1
                },
                "collection_image_count": 92,
                "hit": null
            },
            {
                "id": 5,
                "collection_name": "gila gila 811",
                "collection_image_first": {
                    "url": "gg\/gg811\/gg81145.jpg",
                    "order": 1
                },
                "collection_image_count": 87,
                "hit": null
            },
            {
                "id": 6,
                "collection_name": "gila gila 812",
                "collection_image_first": {
                    "url": "gg\/gg812\/gg81245.jpg",
                    "order": 1
                },
                "collection_image_count": 88,
                "hit": null
            }
        ]
    }
}
```

### HTTP Request
`GET api/product/{product}`

#### Query Parameters

Parameter | Status | Description
--------- | ------- | ------- | -----------
    product_id |  required  | Id of the product.

<!-- END_1fcbf5d495e6ada99ea017e9ae32b380 -->

<!-- START_83450be85ba428b3306220a00393692e -->
## Get product collections

> Example request:

```bash
curl -X GET -G "http://megazine.test/api/product/{product}/collection/{collection_no}" \
    -H "Accept: application/json"
```

```javascript
const url = new URL("http://megazine.test/api/product/{product}/collection/{collection_no}");

    let params = {
            "product_id": "2",
            "collection": "3",
        };
    Object.keys(params).forEach(key => url.searchParams.append(key, params[key]));

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

> Example response (200):

```json
{
    "data": {
        "id": 1,
        "product_id": 1,
        "collection_name": "cover",
        "collection_image_first": {
            "url": "gg\/gg001\/gg00101.jpg",
            "order": 1
        },
        "hit": 31,
        "images": [
            {
                "url": "gg\/gg001\/gg00101.jpg",
                "order": 1
            }
        ]
    }
}
```

### HTTP Request
`GET api/product/{product}/collection/{collection_no}`

#### Query Parameters

Parameter | Status | Description
--------- | ------- | ------- | -----------
    product_id |  required  | Id of the product.
    collection |  required  | Collection number of the image.

<!-- END_83450be85ba428b3306220a00393692e -->


