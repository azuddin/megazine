#!/bin/sh
whoami
# activate maintenance mode
php artisan down
# update source code
git fetch
git checkout -f develop
git pull
# update PHP dependencies
composer install
# --no-interaction Do not ask any interactive question
# --no-dev  Disables installation of require-dev packages.
# --prefer-dist  Forces installation from package dist even for dev versions.
# update database
#if [ -f .env ]; then
php artisan migrate --force
php artisan config:clear
php artisan apidoc:generate
#fi
# --force  Required to run when in production.
# stop maintenance mode
if [ ! -d "public/storage" ]
then
php artisan storage:link
fi

if [ ! -d ".htaccess" ]
then
cp .htaccess.example .htaccess
fi
chmod +x deploy.staging.sh deploy.production.sh
php artisan up

php artisan queue:restart