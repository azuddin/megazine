@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">
                    @if($status)
                        {{"Order has been paid!"}}
                    @else
                        {{"Order is pending."}}
                    @endif
                </div>

                <div class="card-body">
                    <p>Please check your email for the status of your order @if(isset($order_ref)){{ '(' . $order_ref . ')' }} @endif .</p>
                    <p>Thank you!</p>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
