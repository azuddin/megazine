@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Account Confirmation</div>

                <div class="card-body" style="text-align:center">
                    @if(isset($message) && strlen($message)>0)
                    {{ $message }}
                    @else
                    <p>Your account has been confirmed!</p>
                    <p>You can continue using our application.</p>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
