@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Your Order {{ $order->order_ref }}</div>

                <div class="card-body">
                    <table>
                        <tr>
                            <th>Order Ref</th>
                            <td>{{ $order->order_ref }}</td>
                        </tr>
                        <tr>
                            <th>Total</th>
                            <td>RM {{ $order->total }}</td>
                        </tr>
                        <tr>
                            <th>Status</th>
                            <td>{{ strtoupper($order->order_status) }}</td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
