<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductCollectionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_collections', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('product_id');
            $table->string('collection_name')->default('cover');
            $table->string('collection_slug');
            $table->tinyInteger('is_hidden')->default(0);
            $table->bigInteger('hit')->nullable();
            $table->timestamps();
        });

        Schema::table('product_images', function (Blueprint $table) {
            $table->dropForeign('product_images_product_id_foreign');
            $table->dropColumn('product_id');
            $table->dropColumn('collection_no');
            $table->dropColumn('is_hidden');
            $table->unsignedBigInteger('product_collection_id')->after('id');
            $table->foreign('product_collection_id')->references('id')->on('product_collections');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('product_images', function (Blueprint $table) {
            $table->dropForeign('product_images_product_collection_id_foreign');
            $table->dropColumn('product_collection_id');
            $table->unsignedBigInteger('product_id')->after('id');
            $table->foreign('product_id')->references('id')->on('products');
            $table->integer('collection_no')->nullable();
            $table->tinyInteger('is_hidden')->default(0);
        });

        Schema::dropIfExists('product_collections');
    }
}
