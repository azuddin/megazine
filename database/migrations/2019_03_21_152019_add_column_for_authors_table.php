<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnForAuthorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('authors', function (Blueprint $table) {
            $table->string('alias_name')->nullable()->after('author_name');
            $table->string('email')->nullable()->after('alias_name');
            $table->string('avatar')->nullable()->after('email');
        });

        Schema::table('products', function (Blueprint $table) {
            $table->string('likes')->default(0)->after('hit');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('authors', function (Blueprint $table) {
            $table->dropColumn('alias_name');
            $table->dropColumn('email');
            $table->dropColumn('avatar');
        });

        Schema::table('products', function (Blueprint $table) {
            $table->dropColumn('likes');
        });
    }
}
