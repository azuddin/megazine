<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Product;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('product_name');
            $table->longText('product_desc')->nullable();
            $table->enum('product_type', Product::TYPE_ARRAY)->default(Product::TYPE_NORMAL);
            $table->decimal('price', 10, 2)->default(0);
            $table->integer('stock')->default(1);
            $table->tinyInteger('is_active')->default(1);
            $table->tinyInteger('is_public')->default(1);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
