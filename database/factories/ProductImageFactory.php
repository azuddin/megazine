<?php

use Faker\Generator as Faker;
use App\ProductImage;

$factory->define(ProductImage::class, function (Faker $faker) {
    return [
        'url' => env('APP_URL') . "/one-piece/issue-934/one-piece-11929705.jpg",
        'order' => 1,
    ];
});
