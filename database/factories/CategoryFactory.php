<?php

use Faker\Generator as Faker;
use App\Category;

$factory->define(Category::class, function (Faker $faker) {
    return [
        'category_name' => $faker->randomElement(['Majalah', 'Komik', 'Koleksi'])
    ];
});
