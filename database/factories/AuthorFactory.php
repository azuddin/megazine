<?php

use Faker\Generator as Faker;
use App\Author;

$factory->define(Author::class, function (Faker $faker) {
    return [
        'author_name' => $faker->name(),
        'avatar' => env('APP_URL') . '/storage/avatar-' . rand(1, 9) . '.png'
    ];
});
