<?php

use App\Order;
use Faker\Generator as Faker;

$factory->define(Order::class, function (Faker $faker) {

    $user = App\User::first();

    if ($user == null) {
        $user = App\User::create([
            'name' => 'azuddin', 
            'email'=>'azuddin@azuddin.com', 
            'password'=>bcrypt('secret')
        ]);
    }

    return [
        'order_ref' => 'MY0000'.$faker->randomNumber(4),
        'user_id' => $user->id,
        'payment_method' => $faker->randomElement(['credit card', 'online transfer', 'billplz']),
        'payment_ref' => 'W_79pJDk'
    ];
});
