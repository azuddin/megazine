<?php

use App\Bundle;
use Faker\Generator as Faker;

$factory->define(Bundle::class, function (Faker $faker) {
    return [
        'bundle_name' => $faker->sentence(4),
        'bundle_desc' => $faker->paragraphs(2,true),
        'price' => $faker->randomFloat(2,1,99),
        'is_active' => $faker->numberBetween(0,1), // password
    ];
});