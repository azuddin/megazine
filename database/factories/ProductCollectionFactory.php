<?php

use Faker\Generator as Faker;
use App\ProductCollection;

$factory->define(ProductCollection::class, function (Faker $faker) {
    return [
        'product_id' => 1,
        'collection_name' => $faker->sentence(),
    ];
});
