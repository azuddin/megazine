<?php

use App\Product;
use Faker\Generator as Faker;

$factory->define(Product::class, function (Faker $faker) {
    return [
        'product_name' => $faker->sentence(4),
        'product_desc' => $faker->paragraphs(2,true),
        'product_type' => $faker->randomElement(['subs', 'normal']),
        'price' => $faker->randomFloat(2,1,99),
        'stock' => 100,
        'is_active' => 1,
        'is_public' => 1
    ];
});
