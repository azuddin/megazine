<?php

use Illuminate\Database\Seeder;
use App\Product;
use Illuminate\Support\Facades\Storage;
use App\Author;
use App\Category;
use App\ProductImage;
use App\ProductCollection;

class ProductsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = json_decode(file_get_contents(base_path() . '/database/data.json'));

        foreach ($data as $item) {
            $author = Author::firstOrNew([
                'author_name' => $item->author_name
            ]);
            $author->avatar = $item->author_avatar;
            $author->save();

            $category = Category::firstOrCreate([
                'category_name' => $item->category
            ]);

            $product = Product::firstOrCreate([
                'product_name' => $item->product_name,
                'author_id' => $author->id,
                'category_id' => $category->id,
            ]);

            $productCollection = ProductCollection::firstOrCreate([
                'product_id' => $product->id,
                'collection_name' => $item->collection_no,
            ]);

            factory(App\ProductImage::class)->create([
                'product_collection_id' => $productCollection->id,
                'url' => $item->url,
            ]);
        }
    }
}
