<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class RolePermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //remove all role and permission
        app()->make(\Spatie\Permission\PermissionRegistrar::class)->forgetCachedPermissions();

        try {
            Permission::create(['name' => 'view_admins']);
            Permission::create(['name' => 'manage_admins']);
            Permission::create(['name' => 'view_users']);
            Permission::create(['name' => 'manage_users']);
            Permission::create(['name' => 'view_roles']);
            Permission::create(['name' => 'manage_roles']);
            Permission::create(['name' => 'view_permissions']);
            Permission::create(['name' => 'manage_permissions']);
            Permission::create(['name' => 'view_products']);
            Permission::create(['name' => 'manage_products']);
            Permission::create(['name' => 'view_authors']);
            Permission::create(['name' => 'manage_authors']);
            Permission::create(['name' => 'view_categories']);
            Permission::create(['name' => 'manage_categories']);
            Permission::create(['name' => 'view_bundles']);
            Permission::create(['name' => 'manage_bundles']);
            Permission::create(['name' => 'view_carts']);
            Permission::create(['name' => 'view_orders']);


            $superAdmin = Role::create(['name' => 'superAdmin']);
            $admin = Role::create(['name' => 'admin']);
            $finance = Role::create(['name' => 'finance']);
            $author = Role::create(['name' => 'author']);

            $superAdmin->givePermissionTo([
                'view_admins', 'manage_admins',
                'view_users', 'manage_users',
                'view_roles', 'manage_roles',
                'view_permissions', 'manage_permissions',
                'view_products', 'manage_products',
                'view_authors', 'manage_authors',
                'view_categories', 'manage_categories',
                'view_bundles', 'manage_bundles',
                'view_carts',
                'view_orders'
            ]);
            $admin->givePermissionTo([
                'view_admins',
                'view_users', 'manage_users',
                'view_roles',
                'view_permissions',
                'view_products', 'manage_products',
                'view_authors', 'manage_authors',
                'view_categories', 'manage_categories',
                'view_bundles', 'manage_bundles',
                'view_carts',
                'view_orders'
            ]);
            $finance->givePermissionTo([
                'view_products', 'manage_products',
                'view_bundles', 'manage_bundles',
                'view_carts',
                'view_orders'
            ]);
            $author->givePermissionTo([
                'view_categories',
                'view_products', 'manage_products',
            ]);
        } catch (Exception $e) {
            $this->command->error($e->getMessage());
        }

        $user = App\User::first();
        if ($user !== null) {
            if (!$user->hasRole('superAdmin')) $user->assignRole('superAdmin');
            if (!$user->hasRole('admin')) $user->assignRole('admin');
        }
    }
}
