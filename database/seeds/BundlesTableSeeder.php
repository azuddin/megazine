<?php

use Illuminate\Database\Seeder;

class BundlesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $offset = 0;

        factory(App\Bundle::class, 3)->create()->each(function ($bundle) use (&$offset) {
            $products = App\Product::skip($offset)->take(5)->get();
            foreach ($products as $product)
            {
                $bundle->attachItem($product);
            }
            $offset += 5;
        });
    }
}
