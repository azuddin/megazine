<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        App\User::create([
            'name' => 'SUPERADMIN',
            'email' => 'admin@azuddin.com',
            'password' => bcrypt('secret'),
            'activation_token' => '',
            'email_verified_at' => now(),
            'remember_token' => Str::random(10),
            'avatar' => env('APP_URL') . '/storage/avatar-' . rand(1, 9) . '.png'
        ]);

        App\User::create([
            'name' => 'Ahmad Azuddin',
            'email' => 'ahmad@azuddin.com',
            'password' => bcrypt('secret'),
            'activation_token' => '',
            'email_verified_at' => now(),
            'remember_token' => Str::random(10),
            'avatar' => env('APP_URL') . '/storage/avatar-' . rand(1, 9) . '.png'
        ]);

        // factory(App\Author::class, 5)->create();

        // factory(App\Category::class)->create(['category_name' => 'Komik']);
        // factory(App\Category::class)->create(['category_name' => 'Majalah']);
        // factory(App\Category::class)->create(['category_name' => 'Koleksi']);
        // factory(App\Category::class)->create(['category_name' => 'Merchandise']);

        $this->call(ProductsTableSeeder::class);
        $this->call(BundlesTableSeeder::class);
        $this->call(RolePermissionSeeder::class);

        Artisan::call('passport:install');
    }
}
